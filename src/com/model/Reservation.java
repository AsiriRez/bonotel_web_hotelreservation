package com.model;

import java.util.ArrayList;

public class Reservation {
	
	private String bookingNo;
	private String bookingDate;
	private String toName;
	private String toOrderNo;
	private String notes;
	private String hotelname;
	private String bookingType;
	private String channel;
	private ArrayList<String> lastName;
	private ArrayList<String> firstName;
	private String modifiedBy;
	private String arrivalDate;
	private String departureDate;
	private ArrayList<String> roomType;
	private ArrayList<String> bedType;
	private ArrayList<String> ratePlan;
	private String rooms;
	private String nights;
	private String totalCost;
	private String bookingValue;
	private String bonoModifyFee;
	private String hotelModifyFee;
	
	private String pageTotal;
	private String grandTotal;
	private boolean isReservationReportLoaded = true;
	
	
	
	public boolean isReservationReportLoaded() {
		return isReservationReportLoaded;
	}
	public void setReservationReportLoaded(boolean isReservationReportLoaded) {
		this.isReservationReportLoaded = isReservationReportLoaded;
	}
	public ArrayList<String> getLastName() {
		return lastName;
	}
	public void setLastName(ArrayList<String> lastName) {
		this.lastName = lastName;
	}
	public ArrayList<String> getFirstName() {
		return firstName;
	}
	public void setFirstName(ArrayList<String> firstName) {
		this.firstName = firstName;
	}
	public ArrayList<String> getRoomType() {
		return roomType;
	}
	public void setRoomType(ArrayList<String> roomType) {
		this.roomType = roomType;
	}
	public ArrayList<String> getBedType() {
		return bedType;
	}
	public void setBedType(ArrayList<String> bedType) {
		this.bedType = bedType;
	}
	public ArrayList<String> getRatePlan() {
		return ratePlan;
	}
	public void setRatePlan(ArrayList<String> ratePlan) {
		this.ratePlan = ratePlan;
	}
	public String getBookingNo() {
		return bookingNo;
	}
	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getToName() {
		return toName;
	}
	public void setToName(String toName) {
		this.toName = toName;
	}
	public String getToOrderNo() {
		return toOrderNo;
	}
	public void setToOrderNo(String toOrderNo) {
		this.toOrderNo = toOrderNo;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getHotelname() {
		return hotelname;
	}
	public void setHotelname(String hotelname) {
		this.hotelname = hotelname;
	}
	public String getBookingType() {
		return bookingType;
	}
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	
	public String getRooms() {
		return rooms;
	}
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	public String getNights() {
		return nights;
	}
	public void setNights(String nights) {
		this.nights = nights;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getBookingValue() {
		return bookingValue;
	}
	public void setBookingValue(String bookingValue) {
		this.bookingValue = bookingValue;
	}
	public String getBonoModifyFee() {
		return bonoModifyFee;
	}
	public void setBonoModifyFee(String bonoModifyFee) {
		this.bonoModifyFee = bonoModifyFee;
	}
	public String getHotelModifyFee() {
		return hotelModifyFee;
	}
	public void setHotelModifyFee(String hotelModifyFee) {
		this.hotelModifyFee = hotelModifyFee;
	}
	public String getPageTotal() {
		return pageTotal;
	}
	public void setPageTotal(String pageTotal) {
		this.pageTotal = pageTotal;
	}
	public String getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}
	
	
	

}
