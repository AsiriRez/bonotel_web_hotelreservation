package com.reader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import org.openqa.selenium.WebDriver;

import com.controller.PG_Properties;
import com.model.*;

public class WebReportsReader {
	
	private WebDriver driver;
	private HotelDetails hotelDetails;
	private SearchInfo searchInfo;
	private Reservation reservationdetails;
	private ConfirmationReport conDetails;
	private CancellationDetails cancelDetails;
	private Modification modDetails;
	private TO_Details toDetails;
	private StringBuffer PrintWriter;
	private int ScenarioCount = 0;  
	private int testCaseCount, testCaseCountReser, testCountCustomer,testCountHotel,testCountPO;
	private int testCaseCancel, testCaseModify;
	private int adultCountRes = 0;
	private int childCountRes = 0; 
	private String childCount, aduCount;
	private List<String> roomList;
	private List<String> occupancyList;
	private int roomC = 0;
	private int excelRoomCount;
	private List<String> adultList;	
	private List<String> childList;
	private ArrayList<ArrayList<String>> dailyRatesResultsPage;	
	private ArrayList<String> calcultdTotalRateList;
	private ArrayList<String> calcultdTotalRoomRates;
	private List<String> oneRoomTotal;	
	private List<String> dailyRatesArray;
	private List<String> dailyCostRatesArray;
	private List<String> dailyNetRatesArray;
	private String calcultdTotalRateT;
	private String calcultdTotalCostHotel;
	private String totalCostforReservation;
	
	public WebReportsReader(HotelDetails hotelDetails, Reservation reservationdetails, ConfirmationReport conDetails, CancellationDetails canDetails, Modification moddifyDetails, SearchInfo searchInfo, TO_Details toDetails ,WebDriver driver){
		
		this.hotelDetails=hotelDetails;
		this.reservationdetails=reservationdetails;
		this.searchInfo=searchInfo;
		this.driver=driver;
		this.conDetails=conDetails;
		this.cancelDetails=canDetails;
		this.toDetails=toDetails;
		this.modDetails=moddifyDetails;
	}

	public void getHotelReservationReport(WebDriver webDriver){
		
		PrintWriter = new StringBuffer();
		ScenarioCount ++;
		roomList = new ArrayList<String>();
		occupancyList = new ArrayList<String>();
		adultList = new ArrayList<String>();
		childList = new ArrayList<String>();
		dailyRatesResultsPage = new ArrayList<ArrayList<String>>();
		oneRoomTotal = new ArrayList<String>();
		dailyRatesArray = new ArrayList<String>();
		dailyNetRatesArray = new ArrayList<String>();
		dailyCostRatesArray = new ArrayList<String>();
		calcultdTotalRateList  = new ArrayList<String>();
		calcultdTotalRoomRates  = new ArrayList<String>();
		
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		String currentDateforMatch = dateFormatcurrentDate.format(cal.getTime()); 
		
		try {
			
			excelRoomCount = Integer.parseInt(searchInfo.getRooms());
			
			if (searchInfo.getAdults().contains(";")) {
				String adu = searchInfo.getAdults();
				String[] partsAdults = adu.split(";");
				for (String valueAdu : partsAdults) {
					adultList.add(valueAdu);
				}
			} else {
				String adu = searchInfo.getAdults();
				adultList.add(adu);
			}
			
			if (searchInfo.getChildren().contains(";")) {
				String chi = searchInfo.getChildren();
				String[] partschilds = chi.split(";");
				for (String valuechi : partschilds) {
					childList.add(valuechi);
				}
			} else {
				String chi = searchInfo.getChildren();
				childList.add(chi);
			}
			
			PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
			PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Bonotel Hotel Reservation Report - Date ["+currentDateforMatch+"] / Reservation No : "+hotelDetails.getConfirmation_ReservationNO()+"</p></div>");
			PrintWriter.append("<body>");		
			PrintWriter.append("<br><br>");
			PrintWriter.append("<div style=\"border:1px solid;border-radius:3px 3px 3px 3px;background-color:#8D8D86; width:50%;\">");
			PrintWriter.append("<p class='InfoSup'style='font-weight: bold;'>"+ScenarioCount+")<br></p>");
			PrintWriter.append("<p class='InfoSub'>Current Hotel Reservation Criteria</p>");			
			PrintWriter.append("<p class='InfoSub'>Tour Operator Name :- "+searchInfo.getTourOperatorName()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Tour Operator Type :- "+searchInfo.getTO_type()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Country :- "+searchInfo.getCountry()+"</p>");
			PrintWriter.append("<p class='InfoSub'>City :- "+searchInfo.getCity()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date From :- "+searchInfo.getDateFrom()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Date To :- "+searchInfo.getDateTo()+"</p>");
			PrintWriter.append("<p class='InfoSub'>No of Rooms :- "+searchInfo.getRooms()+"</p>");
			PrintWriter.append("<p class='InfoSub'>Nights :- "+searchInfo.getNights()+"</p>");			
			
			for (int i = 0; i < excelRoomCount; i++) {
				PrintWriter.append("<p class='InfoSub'>Occupancy :- [Room "+(i+1)+"] - No of Adults : "+adultList.get(i)+" , No of Children : "+childList.get(i)+"</p>");
			}
			PrintWriter.append("<p class='InfoSub'>Portal URL :- "+PG_Properties.getProperty("SearchLink")+"</p>");
					
			PrintWriter.append("</div>");
			PrintWriter.append("<br>");
			
			testCaseCount = 1;
			PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
			
			PrintWriter.append("<tr><td class='fontiiii'>Booking Engine</td><tr>"); 
			
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking Engine - Hotel Nights</td>");
			PrintWriter.append("<td>15</td>");
			
			try {			
				if(15 == hotelDetails.getCount_nights()) {
					
					PrintWriter.append("<td>"+hotelDetails.getCount_nights()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
									
				} else {
					PrintWriter.append("<td>"+hotelDetails.getCount_nights()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
				}
					
			} catch (Exception e) {				
				PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCount++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking Engine - Hotel Rooms</td>");
			PrintWriter.append("<td>10</td>");
			
			try {			
				if(10 == hotelDetails.getCount_rooms()) {
					
					PrintWriter.append("<td>"+hotelDetails.getCount_rooms()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
									
				} else {
					PrintWriter.append("<td>"+hotelDetails.getCount_rooms()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
				}
					
			} catch (Exception e) {				
				PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCount++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking Engine - Adults Count</td>");
			PrintWriter.append("<td>4</td>");
			
			try {			
				if(4 == hotelDetails.getCount_adults()) {
					
					PrintWriter.append("<td>"+hotelDetails.getCount_adults()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
									
				} else {
					PrintWriter.append("<td>"+hotelDetails.getCount_adults()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
				}
					
			} catch (Exception e) {				
				PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			testCaseCount++;
			PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Booking Engine - Children Count</td>");
			PrintWriter.append("<td>3</td>");
			
			try {			
				if(3 == hotelDetails.getCount_children()) {
					
					PrintWriter.append("<td>"+hotelDetails.getCount_children()+"</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
									
				} else {
					PrintWriter.append("<td>"+hotelDetails.getCount_children()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
				}
					
			} catch (Exception e) {				
				PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
			}
			
			
			
			PrintWriter.append("<tr><td class='fontiiii'>Results Page</td><tr>"); 
			
			////
			
			if(hotelDetails.isResultsAvailable() == true){
				
				testCaseCount++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel CheckIn Date</td>");
				PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
				
				try {			
					if(searchInfo.getDateFrom().replaceAll("-", " ").equals(hotelDetails.getResults_CheckInDate())) {
						
						PrintWriter.append("<td>"+hotelDetails.getResults_CheckInDate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getResults_CheckInDate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel CheckOut Date</td>");
				PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
				
				try {			
					if(searchInfo.getDateTo().replaceAll("-", " ").equals(hotelDetails.getResults_CheckOutDate())) {
						
						PrintWriter.append("<td>"+hotelDetails.getResults_CheckOutDate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getResults_CheckOutDate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel No of Nights</td>");
				PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
				
				try {			
					if(searchInfo.getNights().equals(hotelDetails.getResults_Nights())) {
						
						PrintWriter.append("<td>"+hotelDetails.getResults_Nights()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getResults_Nights()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel No of Rooms</td>");
				PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
				
				try {			
					if(searchInfo.getRooms().equals(hotelDetails.getResults_Rooms())) {
						
						PrintWriter.append("<td>"+hotelDetails.getResults_Rooms()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getResults_Rooms()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				//////
				
				if (searchInfo.getAdults().contains(";")) {
					String adu = searchInfo.getAdults();
					String[] partsAdults = adu.split(";");
					for (String valueAdu : partsAdults) {
						adultCountRes = adultCountRes + Integer.parseInt(valueAdu);					
					}
					
					aduCount = Integer.toString(adultCountRes);
						
				} else {
					aduCount = searchInfo.getAdults();
					
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel Adults count</td>");
				PrintWriter.append("<td>"+aduCount+"</td>");
				
				try {			
					if(aduCount.equals(hotelDetails.getResults_Adults())) {
						
						PrintWriter.append("<td>"+hotelDetails.getResults_Adults()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getResults_Adults()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				if (searchInfo.getChildren().contains(";")) {
					String chi = searchInfo.getChildren();
					String[] partschilds = chi.split(";");
					for (String valuechi : partschilds) {
						childCountRes = childCountRes + Integer.parseInt(valuechi);					
					}
					
					childCount = Integer.toString(childCountRes);
					
				} else {
					childCount = searchInfo.getChildren();
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel Children count</td>");
				PrintWriter.append("<td>"+childCount+"</td>");
				
				try {			
					if(childCount.equals(hotelDetails.getResults_children())) {
						
						PrintWriter.append("<td>"+hotelDetails.getResults_children()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getResults_children()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				/////
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel Name</td>");
				PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
				
				try {			
					if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getResults_hotelName())) {
						
						PrintWriter.append("<td>"+hotelDetails.getResults_hotelName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getResults_hotelName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				////
			
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel Address</td>");
				PrintWriter.append("<td>"+searchInfo.getExcelHotelAddress()+"</td>");
				
				try {			
					if(searchInfo.getExcelHotelAddress().replaceAll("#", "").replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getResults_hotelAddress().replaceAll(",", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getResults_hotelAddress()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getResults_hotelAddress()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel Description</td>");
				PrintWriter.append("<td>"+searchInfo.getHotelDescription()+"</td>");
				
				try {			
					if(searchInfo.getHotelDescription().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getHoteldescription().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getHoteldescription()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getHoteldescription()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				////
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Cancellation policy link</td>");
				PrintWriter.append("<td>Cancellation policy link should be available</td>");
				
				try {			
					if(hotelDetails.isCancellationLinkAvailable() == true) {
						
						PrintWriter.append("<td>Available</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>Not Available</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Restrictions/Policies link</td>");
				PrintWriter.append("<td>Restrictions/Policies link should be available</td>");
				
				try {			
					if(hotelDetails.isRestrictionLinkAvailable() == true) {
						
						PrintWriter.append("<td>Available</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>Not Available</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel Information link</td>");
				PrintWriter.append("<td>Hotel Information link should be available</td>");
				
				try {			
					if(hotelDetails.isHotelInfoLinkAvailable() == true) {
						
						PrintWriter.append("<td>Available</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>Not Available</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Specials link</td>");
				PrintWriter.append("<td>Specials link should be available</td>");
				
				try {			
					if(hotelDetails.isSpecialsLinkAvailable() == true) {
						
						PrintWriter.append("<td>Available</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>Not Available</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
						
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel Name - Hotel Information Link</td>");
				PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
				
				try {			
					if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getNewWindow_HotelName().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getNewWindow_HotelName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getNewWindow_HotelName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				String excelAdd_HotelInfo =" "+searchInfo.getExcelHotelAddress().replaceAll("#", ",")+" "+searchInfo.getCity()+" ";
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel Address - Hotel Information Link</td>");
				PrintWriter.append("<td>"+excelAdd_HotelInfo+"</td>");
				
				try {			
					if(hotelDetails.getNewWindow_Address().replaceAll(" ", "").contains(excelAdd_HotelInfo.replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getNewWindow_Address()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getNewWindow_Address()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}

				
				////
				
				if (searchInfo.getExcelRoomType().contains("#")) {
					String roomTypes = searchInfo.getExcelRoomType();
					String[] partsEA = roomTypes.split("#");
					for (String value : partsEA) {
						roomList.add(value);
					}
				} else {
					String roomTypes = searchInfo.getExcelRoomType();
					roomList.add(roomTypes);
				}
				
				if (searchInfo.getExcelOccupancyratePlan().contains("#")) {
					String ratePlansOccupancy = searchInfo.getExcelOccupancyratePlan();
					String[] partsEAROP = ratePlansOccupancy.split("#");
					for (String valueROP : partsEAROP) {
						occupancyList.add(valueROP);
					}
				} else {
					String ratePlansOccupancy = searchInfo.getExcelOccupancyratePlan();
					occupancyList.add(ratePlansOccupancy);
				}
				
				
				ArrayList<RoomTypes> roomListDetails = hotelDetails.getRoomTypes();
				ArrayList<String> totalRateT = new ArrayList<String>();
				DecimalFormat df = new DecimalFormat("#.00"); 
				
				for (RoomTypes roomTypes : roomListDetails) {
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel Room type match - Room "+(roomC+1)+"</td>");
					PrintWriter.append("<td>"+roomList.get(roomC)+"</td>");
					
					try {			
						if(roomList.get(roomC).replaceAll(" ", "").equalsIgnoreCase(roomTypes.getResults_RoomType().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+roomTypes.getResults_RoomType()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+roomTypes.getResults_RoomType()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				
					String roomOccupancyRatePlan = roomTypes.getResults_Occupancy() + " / " + roomTypes.getResults_RatePlan();
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Hotel Occupancy/Rate plan match - Room "+(roomC+1)+"</td>");
					PrintWriter.append("<td>"+occupancyList.get(roomC)+"</td>");
					
					try {			
						if(occupancyList.get(roomC).replaceAll(" ", "").equalsIgnoreCase(roomOccupancyRatePlan.replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+roomOccupancyRatePlan+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+roomOccupancyRatePlan+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					totalRateT.add(hotelDetails.getDailyRatesList().get(roomC));
					String calcultdDayRate = "$" + hotelDetails.getDailyRatesList().get(roomC);
					String resDayRate = roomTypes.getResults_DailyRates().get(roomC);
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Daily Rate Calculations - Room "+(roomC+1)+"</td>");
					PrintWriter.append("<td>"+calcultdDayRate+"</td>");
					
					try {			
						if(calcultdDayRate.equals(resDayRate)) {
							
							PrintWriter.append("<td>"+resDayRate+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+resDayRate+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					double listx = Double.parseDouble(hotelDetails.getDailyRatesList().get(roomC))* Integer.parseInt(searchInfo.getNights());
					
					String calcultdTotalRate = "$" + df.format(listx);
					String resTotalRate = roomTypes.getResults_listTotal();
					calcultdTotalRateList.add(calcultdTotalRate);
					calcultdTotalRoomRates.add(df.format(listx));
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Total - Room "+(roomC+1)+"</td>");
					PrintWriter.append("<td>"+calcultdTotalRate+"</td>");
					
					try {			
						if(calcultdTotalRate.equals(resTotalRate)) {
							
							PrintWriter.append("<td>"+resTotalRate+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+resTotalRate+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					dailyRatesResultsPage.add(roomTypes.getResults_DailyRates()); //[  [$440.0, $440.0, $440.0]  , [$330.0, $330.0, $330.0]  ]
					oneRoomTotal.add(roomTypes.getResults_listTotal());  //[  $1320.00, $990.00  ]
					roomC ++;
						
				}
				
				
				double tValue = 0;
				for (int i = 0; i < Integer.parseInt(searchInfo.getRooms()); i++) {
					
					tValue = tValue + (Double.parseDouble(totalRateT.get(i)) * Integer.parseInt(searchInfo.getNights()));
				}
				
				calcultdTotalRateT = "$" + df.format(tValue);
				totalCostforReservation = df.format(tValue);
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Page - Room Total Rate</td>");
				PrintWriter.append("<td>"+calcultdTotalRateT+"</td>");
				
				try {			
					if(calcultdTotalRateT.equals(hotelDetails.getResults_TotalRate())) {
						
						PrintWriter.append("<td>"+hotelDetails.getResults_TotalRate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getResults_TotalRate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
					
	
				
				////
				
				PrintWriter.append("<tr><td class='fontiiii'>Payments Page</td><tr>"); 
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Hotel Name [Shopping Cart]</td>");
				PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
				
				try {			
					if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getPaymentShopCart_hotelName().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentShopCart_hotelName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentShopCart_hotelName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Hotel Qty [Shopping Cart]</td>");
				PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
				
				try {			
					if(searchInfo.getRooms().equals(hotelDetails.getPaymentShopCart_qty())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentShopCart_qty()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentShopCart_qty()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Arrival Date [Shopping Cart]</td>");
				PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
				
				try {			
					if(searchInfo.getDateFrom().replaceAll("-", "").equals(hotelDetails.getPaymentShopCart_dateFrom().replaceAll(",", "").replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentShopCart_dateFrom()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentShopCart_dateFrom()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Departure Date [Shopping Cart]</td>");
				PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
				
				try {			
					if(searchInfo.getDateTo().replaceAll("-", "").equals(hotelDetails.getPaymentShopCart_dateTo().replaceAll(",", "").replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentShopCart_dateTo()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentShopCart_dateTo()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				/////////
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Check In Date [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
				
				try {			
					if(searchInfo.getDateFrom().replaceAll("-", " ").equals(hotelDetails.getPaymentTravelDetails_checkIn())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_checkIn()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_checkIn()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Check Out Date [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
				
				try {			
					if(searchInfo.getDateTo().replaceAll("-", " ").equals(hotelDetails.getPaymentTravelDetails_checkOut())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_checkOut()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_checkOut()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Hotel Name [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
				
				try {			
					if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getPaymentTravelDetails_HotelName().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_HotelName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_HotelName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				

				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Hotel Address [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getExcelHotelAddress()+"</td>");
				
				try {			
					if(searchInfo.getExcelHotelAddress().replaceAll("#", "").replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getPaymentTravelDetails_Address().replaceAll(",", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Address()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Address()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Hotel No of Nights [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
				
				try {			
					if(searchInfo.getNights().equals(hotelDetails.getPaymentTravelDetails_Nights())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Nights()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Nights()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Hotel No of Rooms [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
				
				try {			
					if(searchInfo.getRooms().equals(hotelDetails.getPaymentTravelDetails_Rooms())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Rooms()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Rooms()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Hotel Adults count [Travel Details]</td>");
				PrintWriter.append("<td>"+aduCount+"</td>");
				
				try {			
					if(aduCount.equals(hotelDetails.getPaymentTravelDetails_Adults())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Adults()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Adults()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Hotel Children count [Travel Details]</td>");
				PrintWriter.append("<td>"+childCount+"</td>");
				
				try {			
					if(childCount.equals(hotelDetails.getPaymentTravelDetails_Children())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Children()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Children()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Hotel Country [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getCountry()+"</td>");
				
				try {			
					if(searchInfo.getCountry().equalsIgnoreCase(hotelDetails.getPaymentTravelDetails_country())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_country()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_country()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Hotel City [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getCity()+"</td>");
				
				try {			
					if(searchInfo.getCity().equalsIgnoreCase(hotelDetails.getPaymentTravelDetails_city())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_city()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_city()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				for (int i = 0; i < excelRoomCount; i++) {
					
					PrintWriter.append("<tr><td class='fontyG'>Room : "+(i+1)+"</td><tr>"); 
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Room "+(i+1)+" - Adults count [Travel Details]</td>");
					PrintWriter.append("<td>"+adultList.get(i)+"</td>");
					
					try {			
						if(adultList.get(i).equals(hotelDetails.getPaymentTravelDetails_RoomAdults().get(i))) {
							
							PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_RoomAdults().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_RoomAdults().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Room "+(i+1)+" - Childs count [Travel Details]</td>");
					PrintWriter.append("<td>"+childList.get(i)+"</td>");
					
					try {			
						if(childList.get(i).equals(hotelDetails.getPaymentTravelDetails_RoomChildren().get(i))) {
							
							PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_RoomChildren().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_RoomChildren().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Room "+(i+1)+" - Room Type [Travel Details]</td>");
					PrintWriter.append("<td>"+roomList.get(i)+"</td>");
					
					try {			
						if(roomList.get(i).replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getPaymentTravelDetails_RoomType().get(i).replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_RoomType().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_RoomType().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					String paymentOccuPancyRate = hotelDetails.getPaymentTravelDetails_RoomOccupancy().get(i) + "/" + hotelDetails.getPaymentTravelDetails_RoomRatePlan().get(i);
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Room "+(i+1)+" - Occupany/Rate plan [Travel Details]</td>");
					PrintWriter.append("<td>"+occupancyList.get(i)+"</td>");
					
					try {	
						
						if(occupancyList.get(i).replaceAll(" ", "").equalsIgnoreCase(paymentOccuPancyRate.replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+paymentOccuPancyRate+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+paymentOccuPancyRate+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					String dateFromToCal = searchInfo.getDateFrom();
					String dateToCal = searchInfo.getDateTo();
					SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
					
					Date d1 = null;
					Date d2 = null;
					
					try {	
						
						d1 = format.parse(dateFromToCal);
						d2 = format.parse(dateToCal);
			 
						//in milliseconds
						long diffD = d2.getTime() - d1.getTime();
						int diffDays = (int)(diffD / (24 * 60 * 60 * 1000));
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Room "+(i+1)+" - Days count [Travel Details]</td>");
						PrintWriter.append("<td>From :- "+dateFromToCal+" - To :- "+dateToCal+" = "+(diffDays)+"</td>");
						
						if(Integer.parseInt(hotelDetails.getPaymentTravelDetails_RoomDaysCount().get(i)) == (diffDays)) {
							
							PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_RoomDaysCount().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_RoomDaysCount().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					int excelNightss = Integer.parseInt(searchInfo.getNights());
					List<String> paymentsRoomDailyRates = new ArrayList<String>();
					List<String> resultsRoomDailyRates = new ArrayList<String>();
					
					for (int j = 0; j < excelNightss; j++) {
						
						String rateDay = hotelDetails.getPaymentTravelDetails_RoomDailyRates().get(i).get(j);
						paymentsRoomDailyRates.add(rateDay);
						String rateDay2 = dailyRatesResultsPage.get(i).get(j);
						resultsRoomDailyRates.add(rateDay2);			
					}
					
					List<String> daysList = new ArrayList<String>();
					String date=searchInfo.getDateFrom();
					String datett=searchInfo.getDateTo();
					String incDate;
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
					Calendar c = Calendar.getInstance();
					c.setTime(sdf.parse(date));
					int maxDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
					daysList.add(date);
					for(int co=0; co<maxDay; co++){
						
						c.add(Calendar.DATE, 1); 
					    incDate = sdf.format(c.getTime());
						
						if(incDate.equals(datett)){
							break;
						}
						else{
							daysList.add(incDate);
						}					    
					}	
					
					String roomDailyRate = "$" + hotelDetails.getDailyRatesList().get(i);
					String roomDailyCost = "$" + hotelDetails.getDailyRatesCostList().get(i);
					String roomDailyNetRate = "$" + hotelDetails.getDailyNetRateList().get(i);
					
					String match1 = "";
					String match2 = "";
					String calculatedDailyRatesList = "";
					String costrateCal = "";
					String netRateCal = "";
					
				
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Room "+(i+1)+" - Daily Rates [Travel Details]</td>");
					
					for (int k = 0; k < excelNightss; k++) {
						
						match1 = match1 + ""+daysList.get(k)+" : "+resultsRoomDailyRates.get(k)+"" + " ";
						match2 = match2 + ""+daysList.get(k)+" : "+paymentsRoomDailyRates.get(k)+"" + " ";
						calculatedDailyRatesList = calculatedDailyRatesList + ""+daysList.get(k)+" : "+roomDailyRate+"" + " ";
						costrateCal = costrateCal + ""+daysList.get(k)+" : "+roomDailyCost+"" + " ";
						netRateCal = netRateCal + ""+daysList.get(k)+" : "+roomDailyNetRate+"" + " ";
						
					}
					
					dailyRatesArray.add(calculatedDailyRatesList);
					dailyCostRatesArray.add(costrateCal);
					dailyNetRatesArray.add(netRateCal);
					PrintWriter.append("<td>"+calculatedDailyRatesList+"</td>");
										
					try {	
						
						if(calculatedDailyRatesList.equalsIgnoreCase(match2)) {
							
							PrintWriter.append("<td>"+match2+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+match2+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Room "+(i+1)+" - Room Total Value [Travel Details]</td>");
					PrintWriter.append("<td>"+calcultdTotalRateList.get(i)+"</td>");
					
					try {	
						
						if(calcultdTotalRateList.get(i).equals(hotelDetails.getPaymentTravelDetails_Roomtotal().get(i))) {
							
							PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Roomtotal().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_Roomtotal().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
							
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Shopping Cart Total Value [Cart Details]</td>");
				PrintWriter.append("<td>"+calcultdTotalRateT+"</td>");
				
				try {	
					
					if(calcultdTotalRateT.equals(hotelDetails.getPaymentShopCart_CartTotal())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentShopCart_CartTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentShopCart_CartTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Cart Total Value [Travel Details]</td>");
				PrintWriter.append("<td>"+calcultdTotalRateT+"</td>");
				
				try {	
					
					if(calcultdTotalRateT.equals(hotelDetails.getPaymentTravelDetails_RoomBookingTotal())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_RoomBookingTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentTravelDetails_RoomBookingTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Payments Page - Booking Total Value [Confirm Bookings]</td>");
				PrintWriter.append("<td>"+calcultdTotalRateT+"</td>");
				
				try {	
					
					if(calcultdTotalRateT.equals(hotelDetails.getPaymentShoppingCartTotal())) {
						
						PrintWriter.append("<td>"+hotelDetails.getPaymentShoppingCartTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getPaymentShoppingCartTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
		
				
				PrintWriter.append("<tr><td class='fontiiii'>Confirmation Page</td><tr>"); 
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Tour Operator Details - TO Name</td>");
				PrintWriter.append("<td>"+searchInfo.getTourOperatorName()+"</td>");
				
				try {	
					
					if(searchInfo.getTourOperatorName().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getConfirmationTOD_TOName().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_TOName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_TOName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Tour Operator Details - TO Address</td>");
				PrintWriter.append("<td>"+toDetails.getTo_Address()+"</td>");
				
				try {	
					
					if(toDetails.getTo_Address().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getConfirmationTOD_Address().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_Address()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_Address()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Tour Operator Details - TO City</td>");
				PrintWriter.append("<td>"+toDetails.getTo_City()+"</td>");
				
				try {	
					
					if(toDetails.getTo_City().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getConfirmationTOD_City().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_City()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_City()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Tour Operator Details - TO Country</td>");
				PrintWriter.append("<td>"+toDetails.getTo_Country()+"</td>");
				
				try {	
					
					if(toDetails.getTo_Country().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getConfirmationTOD_Country().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_Country()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_Country()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Tour Operator Details - TO Zip</td>");
				PrintWriter.append("<td>"+toDetails.getTo_Zip()+"</td>");
				
				try {	
					
					if(toDetails.getTo_Zip().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getConfirmationTOD_Zip().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_Zip()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_Zip()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Tour Operator Details - TO Email</td>");
				PrintWriter.append("<td>"+toDetails.getTo_Email()+"</td>");
				
				try {	
					
					if(toDetails.getTo_Email().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getConfirmationTOD_Email().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_Email()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_Email()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Tour Operator Details - TO Phone</td>");
				PrintWriter.append("<td>"+toDetails.getTo_phone()+"</td>");
				
				try {	
					
					if(toDetails.getTo_phone().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getConfirmationTOD_phone().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_phone()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTOD_phone()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				////
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Hotel Name [Travel Summary]</td>");
				PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
				
				try {			
					if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getConfirmationTS_HotelName().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTS_HotelName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTS_HotelName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Hotel Qty [Travel Summary]</td>");
				PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
				
				try {			
					if(searchInfo.getRooms().equals(hotelDetails.getConfirmationTS_qty())) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTS_qty()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTS_qty()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Arrival Date [Travel Summary]</td>");
				PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
				
				try {			
					if(searchInfo.getDateFrom().replaceAll("-", "").equals(hotelDetails.getConfirmationTS_checkInDate().replaceAll(",", "").replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTS_checkInDate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTS_checkInDate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Departure Date [Travel Summary]</td>");
				PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
				
				try {			
					if(searchInfo.getDateTo().replaceAll("-", "").equals(hotelDetails.getConfirmationTS_checkoutDate().replaceAll(",", "").replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTS_checkoutDate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTS_checkoutDate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Check In Date [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
				
				try {			
					if(searchInfo.getDateFrom().replaceAll("-", " ").equals(hotelDetails.getConfirmationTD_checkInDate())) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_checkInDate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_checkInDate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Check Out Date [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
				
				try {			
					if(searchInfo.getDateTo().replaceAll("-", " ").equals(hotelDetails.getConfirmationTD_checkOutDate())) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_checkOutDate()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_checkOutDate()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Hotel Name [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
				
				try {			
					if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getConfirmationTD_HotelName().replaceAll(" ", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_HotelName()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_HotelName()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Hotel Address [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getExcelHotelAddress()+"</td>");
				
				try {			
					if(searchInfo.getExcelHotelAddress().replaceAll("#", "").replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getConfirmationTD_hotelAddress().replaceAll(",", ""))) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_hotelAddress()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_hotelAddress()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Hotel No of Nights [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
				
				try {			
					if(searchInfo.getNights().equals(hotelDetails.getConfirmationTD_nights())) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_nights()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_nights()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Hotel No of Rooms [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
				
				try {			
					if(searchInfo.getRooms().equals(hotelDetails.getConfirmationTD_rooms())) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_rooms()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_rooms()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Hotel Adults count [Travel Details]</td>");
				PrintWriter.append("<td>"+aduCount+"</td>");
				
				try {			
					if(aduCount.equals(hotelDetails.getConfirmationTD_adults())) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_adults()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_adults()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Hotel Children count [Travel Details]</td>");
				PrintWriter.append("<td>"+childCount+"</td>");
				
				try {			
					if(childCount.equals(hotelDetails.getConfirmationTD_children())) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_children()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_children()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Hotel Country [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getCountry()+"</td>");
				
				try {			
					if(searchInfo.getCountry().equalsIgnoreCase(hotelDetails.getConfirmationTD_country())) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_country()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_country()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Hotel City [Travel Details]</td>");
				PrintWriter.append("<td>"+searchInfo.getCity()+"</td>");
				
				try {			
					if(searchInfo.getCity().equalsIgnoreCase(hotelDetails.getConfirmationTD_city())) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_city()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_city()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				////
				
				String inputStr = hotelDetails.getConfirmationTD_BookingDate();
				DateFormat dateFormat = new SimpleDateFormat("E MMM dd hh:mm:ss zzz yyyy");
				Date inputDate = dateFormat.parse(inputStr);
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				String stringDateF = sdf.format(inputDate);
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Booking Date [Travel Details]</td>");
				PrintWriter.append("<td>"+hotelDetails.getReservationDate()+"</td>");
				
				try {			
					if(hotelDetails.getReservationDate().equals(stringDateF)) {
						
						PrintWriter.append("<td>"+stringDateF+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+stringDateF+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				/////
				
				for (int i = 0; i < excelRoomCount; i++) {

					PrintWriter.append("<tr><td class='fontyG'>Room : "+(i+1)+"</td><tr>"); 
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Customer First Name (Room : "+(i+1)+") [Travel Details]</td>");
					PrintWriter.append("<td>"+hotelDetails.getCustomerFirstName().get(i)+"</td>");
					
					try {			
						if(hotelDetails.getCustomerFirstName().get(i).equalsIgnoreCase(hotelDetails.getConfirmationTD_FirstName().get(i))) {
							
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_FirstName().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_FirstName().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Customer Last Name (Room : "+(i+1)+") [Travel Details]</td>");
					PrintWriter.append("<td>"+hotelDetails.getCustomerLastName().get(i)+"</td>");
					
					try {			
						if(hotelDetails.getCustomerLastName().get(i).equalsIgnoreCase(hotelDetails.getConfirmationTD_LastNsme().get(i))) {
							
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_LastNsme().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_LastNsme().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////////
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Room "+(i+1)+" - Adults count [Travel Details]</td>");
					PrintWriter.append("<td>"+adultList.get(i)+"</td>");
					
					try {			
						if(adultList.get(i).equals(hotelDetails.getConfirmationTD_Adults().get(i))) {
							
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_Adults().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_Adults().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Room "+(i+1)+" - Childs count [Travel Details]</td>");
					PrintWriter.append("<td>"+childList.get(i)+"</td>");
					
					try {			
						if(childList.get(i).equals(hotelDetails.getConfirmationTD_Children().get(i))) {
							
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_Children().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_Children().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Room "+(i+1)+" - Room Type [Travel Details]</td>");
					PrintWriter.append("<td>"+roomList.get(i)+"</td>");
					
					try {			
						if(roomList.get(i).replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getConfirmationTD_RoomType().get(i).replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_RoomType().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_RoomType().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					String confirmOccuPancyRate = hotelDetails.getConfirmationTD_RoomBedType().get(i) + "/" + hotelDetails.getConfirmationTD_RoomRatePlan().get(i);
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Room "+(i+1)+" - Occupany/Rate plan [Travel Details]</td>");
					PrintWriter.append("<td>"+occupancyList.get(i)+"</td>");
					
					try {	
						
						if(occupancyList.get(i).replaceAll(" ", "").equalsIgnoreCase(confirmOccuPancyRate.replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+confirmOccuPancyRate+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+confirmOccuPancyRate+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					String dateFromToCal = searchInfo.getDateFrom();
					String dateToCal = searchInfo.getDateTo();
					SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
					
					Date d1 = null;
					Date d2 = null;
					
					try {	
						
						d1 = format.parse(dateFromToCal);
						d2 = format.parse(dateToCal);
			 
						//in milliseconds
						long diffD = d2.getTime() - d1.getTime();
						int diffDays = (int)(diffD / (24 * 60 * 60 * 1000));
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Room "+(i+1)+" - Days count [Travel Details]</td>");
						PrintWriter.append("<td>From :- "+dateFromToCal+" - To :- "+dateToCal+" = "+(diffDays)+"</td>");
						
						if(Integer.parseInt(hotelDetails.getConfirmationTD_DaysCount().get(i)) == (diffDays)) {
							
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_DaysCount().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_DaysCount().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					int excelNights = Integer.parseInt(searchInfo.getNights());
					List<String> confirmationRoomDailyRates = new ArrayList<String>();
					List<String> resultsRoomDailyRates = new ArrayList<String>();
					
					for (int j = 0; j < excelNights; j++) {
						
						String rateDay = hotelDetails.getConfirmationTD_DailyRates().get(i).get(j);
						confirmationRoomDailyRates.add(rateDay);
						String rateDay2 = dailyRatesResultsPage.get(i).get(j);
						resultsRoomDailyRates.add(rateDay2);			
					}
					
					List<String> daysList = new ArrayList<String>();
					String date=searchInfo.getDateFrom();
					String datett=searchInfo.getDateTo();
					String incDate;
					SimpleDateFormat sdf1= new SimpleDateFormat("dd-MMM-yyyy");
					Calendar c = Calendar.getInstance();
					c.setTime(sdf1.parse(date));
					int maxDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
					daysList.add(date);
					for(int co=0; co<maxDay; co++){
						
						c.add(Calendar.DATE, 1); 
					    incDate = sdf1.format(c.getTime());
						
						if(incDate.equals(datett)){
							break;
						}
						else{
							daysList.add(incDate);
						}					    
					}	
					
					String roomDailyRate = "$" + hotelDetails.getDailyRatesList().get(i);
					
					String match1 = "";
					String match2 = "";
					String calculatedDailyRatesList = "";
				
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Room "+(i+1)+" - Daily Rates [Travel Details]</td>");
					
					for (int k = 0; k < excelNights; k++) {
						
						match1 = match1 + ""+daysList.get(k)+" : "+resultsRoomDailyRates.get(k)+"" + " ";
						match2 = match2 + ""+daysList.get(k)+" : "+confirmationRoomDailyRates.get(k)+"" + " ";
						calculatedDailyRatesList = calculatedDailyRatesList + ""+daysList.get(k)+" : "+roomDailyRate+"" + " ";
						
						
					}
					PrintWriter.append("<td>"+calculatedDailyRatesList+"</td>");
										
					try {	
						
						if(calculatedDailyRatesList.equalsIgnoreCase(match2)) {
							
							PrintWriter.append("<td>"+match2+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+match2+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Room "+(i+1)+" - Room Total Value [Travel Details]</td>");
					PrintWriter.append("<td>"+calcultdTotalRateList.get(i)+"</td>");
					
					try {	
						
						if(calcultdTotalRateList.get(i).equals(hotelDetails.getConfirmationTD_total().get(i))) {
							
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_total().get(i)+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_total().get(i)+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
				
				}
			
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Shopping Total Value [Travel Summary]</td>");
				PrintWriter.append("<td>"+calcultdTotalRateT+"</td>");
				
				try {	
					
					if(calcultdTotalRateT.equals(hotelDetails.getConfirmationTS_Total())) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTS_Total()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTS_Total()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				testCaseCount ++;
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation Page - Booking Total</td>");
				PrintWriter.append("<td>"+calcultdTotalRateT+"</td>");
				
				try {	
					
					if(calcultdTotalRateT.equals(hotelDetails.getConfirmationTD_BookingTotal())) {
						
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_BookingTotal()+"</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
										
					} else {
						PrintWriter.append("<td>"+hotelDetails.getConfirmationTD_BookingTotal()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
					}
						
				} catch (Exception e) {				
					PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
				}
				
				
				
				PrintWriter.append("<tr><td class='fontiiii'>Voucher Details</td><tr>"); 
				
				if (hotelDetails.isVoucherLoaded() == true) {
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Booking Date</td>");
					PrintWriter.append("<td>"+hotelDetails.getReservationDate()+"</td>");
					
					try {	
						
						if(hotelDetails.getReservationDate().equalsIgnoreCase(hotelDetails.getVoucher_bookingDate())) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_bookingDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_bookingDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Reservation No</td>");
					PrintWriter.append("<td>"+hotelDetails.getConfirmation_ReservationNO()+"</td>");
					
					try {	
						
						if(hotelDetails.getConfirmation_ReservationNO().equalsIgnoreCase(hotelDetails.getVoucher_bookingNo())) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_bookingNo()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_bookingNo()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
													
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Tour Operator Name</td>");
					PrintWriter.append("<td>"+searchInfo.getTourOperatorName()+"</td>");
					
					try {	
						
						if(searchInfo.getTourOperatorName().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getVoucher_ToName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_ToName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_ToName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Hotel Name</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
					
					try {	
						
						if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getVoucher_HotelName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_HotelName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_HotelName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Hotel Address</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelAddress()+"</td>");
					
					try {	
						
						if(hotelDetails.getVoucher_HotelAddress().toLowerCase().replaceAll(" ", "").contains(searchInfo.getExcelHotelAddress().toLowerCase().replaceAll("#", ",").replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_HotelAddress()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_HotelAddress()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Hotel City</td>");
					PrintWriter.append("<td>"+searchInfo.getCity()+"</td>");
					
					try {	
						
						if(searchInfo.getCity().replaceAll(" ", "").equalsIgnoreCase(hotelDetails.getVoucher_HotelCity().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_HotelCity()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_HotelCity()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						String fullNameForCusReport = ""+hotelDetails.getCustomerTitle().get(i)+" "+hotelDetails.getCustomerFirstName().get(i)+" "+hotelDetails.getCustomerLastName().get(i)+"";
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Customer Name [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+fullNameForCusReport+"</td>");
						
						try {
							for (int j = 0; j < hotelDetails.getVoucher_customerNames().size(); j++) {
																					
								if(hotelDetails.getVoucher_customerNames().get(j).replaceAll(" ", "").equalsIgnoreCase(fullNameForCusReport.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+hotelDetails.getVoucher_customerNames().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
					}
					
					///
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Check In Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
					
					try {			
						if(searchInfo.getDateFrom().equalsIgnoreCase(hotelDetails.getVoucher_CheckInDate())) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_CheckInDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_CheckInDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Check Out Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
					
					try {			
						if(searchInfo.getDateTo().equalsIgnoreCase(hotelDetails.getVoucher_CheckOutDate())) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_CheckOutDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_CheckOutDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Hotel No of Nights</td>");
					PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
					
					try {			
						if(searchInfo.getNights().equals(hotelDetails.getVoucher_Nights())) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_Nights()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_Nights()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Hotel No of Rooms</td>");
					PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
					
					try {			
						if(searchInfo.getRooms().equals(hotelDetails.getVoucher_Rooms())) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_Rooms()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_Rooms()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Hotel Adults count</td>");
					PrintWriter.append("<td>"+aduCount+"</td>");
					
					try {			
						if(aduCount.equals(hotelDetails.getVoucher_Adults())) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_Adults()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_Adults()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCount ++;
					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Hotel Children count</td>");
					PrintWriter.append("<td>"+childCount+"</td>");
					
					try {			
						if(childCount.equals(hotelDetails.getVoucher_Children().split(" Children")[0])) {
							
							PrintWriter.append("<td>"+hotelDetails.getVoucher_Children().split(" Children")[0]+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+hotelDetails.getVoucher_Children().split(" Children")[0]+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					String[] roomTypesCustomer = new String[100];
					
					if (hotelDetails.getVoucher_RoomTypes().contains(",")) {
						
						roomTypesCustomer = hotelDetails.getVoucher_RoomTypes().split(",");
						
					} else {
						
						String nm = hotelDetails.getVoucher_RoomTypes();	
						roomTypesCustomer[0] = nm;
					}
				
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Room Type "+(i+1)+"</td>");
						PrintWriter.append("<td>"+roomList.get(i)+"</td>");
						
						try {
							for (int j = 0; j < roomTypesCustomer.length; j++) {
																					
								if(roomTypesCustomer[j].replaceAll(" ", "").contains(roomList.get(i).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+roomTypesCustomer[j]+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
					}
					
					String[] occuBedTypesCustomer = null;
					String voucherOccu = null;
					
					if (hotelDetails.getVoucher_OccupancyRatePlan().contains(",")) {
						
						occuBedTypesCustomer = hotelDetails.getVoucher_OccupancyRatePlan().split(",");
						
					} else {
						
						voucherOccu = hotelDetails.getVoucher_OccupancyRatePlan();	
						
					}
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						testCaseCount ++;
						PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Voucher Details - Occupancy/Rate Plan Type "+(i+1)+"</td>");
						PrintWriter.append("<td>"+occupancyList.get(i)+"</td>");
						
						if (excelRoomCount == 1) {
							
							try {			
								if(voucherOccu.replaceAll(" ", "").equalsIgnoreCase(occupancyList.get(i).replaceAll(" ", ""))) {
									
									PrintWriter.append("<td>"+voucherOccu+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
													
								} else {
									PrintWriter.append("<td>"+voucherOccu+"</td>");
									PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
								}
									
							} catch (Exception e) {				
								PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
							
							break;
							
						} else {
							
							try {
								for (int j = 0; j < occuBedTypesCustomer.length; j++) {
																						
									if(occuBedTypesCustomer[j].replaceAll(" ", "").equalsIgnoreCase(occupancyList.get(i).replaceAll(" ", ""))){
										
										PrintWriter.append("<td>"+occuBedTypesCustomer[j]+"</td>");
										PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
										
										break;
									}					
								}
							} catch (Exception e) {
								PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
							}
						}
					}
					
					
				} else {

					PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Confirmation page - Voucher</td>");
					PrintWriter.append("<td>Voucher should loaded</td>");
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					
					
				}
				


				PrintWriter.append("</table>");
				
				double tValuee = 0;
				for (int i = 0; i < Integer.parseInt(searchInfo.getRooms()); i++) {
					
					tValuee = tValuee + (Double.parseDouble(hotelDetails.getDailyRatesCostList().get(i)) * Integer.parseInt(searchInfo.getNights()));
				}
				
				calcultdTotalCostHotel = df.format(tValuee);
				
				
				
				//Reservation Report
				PrintWriter.append("<br><br>");
				PrintWriter.append("<p class='fontStyles'>Reservation Report Summary</p>");
				
				testCaseCountReser = 1;
				PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
				
				if (reservationdetails.isReservationReportLoaded() == true) {
					

					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Reservation No</td>");
					PrintWriter.append("<td>"+hotelDetails.getConfirmation_ReservationNO()+"</td>");
					
					try {	
						
						if(hotelDetails.getConfirmation_ReservationNO().equals(reservationdetails.getBookingNo())) {
							
							PrintWriter.append("<td>"+reservationdetails.getBookingNo()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+reservationdetails.getBookingNo()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					String bookingDateResrvationDate = reservationdetails.getBookingDate();
					DateFormat dateFormatRes = new SimpleDateFormat("MMM-dd-yyyy hh:mm:ss");
					Date inputDateRes = dateFormatRes.parse(bookingDateResrvationDate);
					SimpleDateFormat sdfR = new SimpleDateFormat("dd-MMM-yyyy");
					String stringDateReser = sdfR.format(inputDateRes );
								
					testCaseCountReser ++;
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Booking Date</td>");
					PrintWriter.append("<td>"+hotelDetails.getReservationDate()+"</td>");
					
					try {	
						
						if(hotelDetails.getReservationDate().equals(stringDateReser)) {
							
							PrintWriter.append("<td>"+stringDateReser+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+stringDateReser+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCountReser ++;
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Tour Operator Name</td>");
					PrintWriter.append("<td>"+searchInfo.getTourOperatorName()+"</td>");
					
					try {	
						
						if(searchInfo.getTourOperatorName().replaceAll(" ", "").equalsIgnoreCase(reservationdetails.getToName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+reservationdetails.getToName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+reservationdetails.getToName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCountReser ++;
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Hotel Name</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
					
					try {	
						
						if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(reservationdetails.getHotelname().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+reservationdetails.getHotelname()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+reservationdetails.getHotelname()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						String fullName = ""+hotelDetails.getCustomerLastName().get(i)+" / "+hotelDetails.getCustomerFirstName().get(i)+"";
						
						testCaseCountReser ++;
						PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Customer Name [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+fullName+"</td>");
						
						try {
							for (int j = 0; j < reservationdetails.getFirstName().size(); j++) {
																					
								if(reservationdetails.getFirstName().get(j).replaceAll(" ", "").equalsIgnoreCase(fullName.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+reservationdetails.getFirstName().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
					}
					
					//////
					
					testCaseCountReser ++;
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Hotel Arrival Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
					
					try {	
						
						if(searchInfo.getDateFrom().equalsIgnoreCase(reservationdetails.getArrivalDate())) {
							
							PrintWriter.append("<td>"+reservationdetails.getArrivalDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+reservationdetails.getArrivalDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCountReser ++;
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Hotel Departure Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
					
					try {	
						
						if(searchInfo.getDateTo().equalsIgnoreCase(reservationdetails.getDepartureDate())) {
							
							PrintWriter.append("<td>"+reservationdetails.getDepartureDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+reservationdetails.getDepartureDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					testCaseCountReser ++;
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Hotel Rooms Count</td>");
					PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
					
					try {	
						
						if(searchInfo.getRooms().equalsIgnoreCase(reservationdetails.getRooms())) {
							
							PrintWriter.append("<td>"+reservationdetails.getRooms()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+reservationdetails.getRooms()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCountReser ++;
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Hotel Nights</td>");
					PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
					
					try {	
						
						if(searchInfo.getNights().equalsIgnoreCase(reservationdetails.getNights())) {
							
							PrintWriter.append("<td>"+reservationdetails.getNights()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+reservationdetails.getNights()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					//////
					
					List<String> sourceListRt = new ArrayList<String>();
					List<String> sourceListBedRate = new ArrayList<String>();
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						String rMTypesSetName = roomList.get(i);
						sourceListRt.add(rMTypesSetName);	
						String bedRateSetName = occupancyList.get(i);
						sourceListBedRate.add(bedRateSetName);	
					}
					
					List<String> newListForRoomTypes = new ArrayList<String>(new LinkedHashSet<String>(sourceListRt));
					List<String> newListBedOccupancyTypes = new ArrayList<String>(new LinkedHashSet<String>(sourceListBedRate));
					
					for (int i = 0; i < newListForRoomTypes.size(); i++) {
						
						testCaseCountReser ++;
						PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Hotel Room Type : "+(i+1)+" </td>");
						PrintWriter.append("<td>"+newListForRoomTypes.get(i)+"</td>");
						
						try {
							
							for (int j = 0; j < reservationdetails.getRoomType().size(); j++) {
								
								if(reservationdetails.getRoomType().get(j).replaceAll(" ", "").toLowerCase().contains(newListForRoomTypes.get(i).replaceAll(" ", "").toLowerCase())) {
									
									PrintWriter.append("<td>"+reservationdetails.getRoomType().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
									
									break;							
								}						
							}
							
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}
					
					for (int i = 0; i < newListBedOccupancyTypes.size(); i++) {
						
						testCaseCountReser ++;
						PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Hotel Occupancy/ Rate Plan Type : "+(i+1)+" </td>");
						PrintWriter.append("<td>"+newListBedOccupancyTypes.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < reservationdetails.getBedType().size() ; j++) {
								
								if(newListBedOccupancyTypes.get(i).replaceAll(" ", "").equalsIgnoreCase(reservationdetails.getBedType().get(j).replaceAll(" ", ""))) {
									
									PrintWriter.append("<td>"+reservationdetails.getBedType().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
									
									break;												
								}						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
											
					}
					
					
					
					
					
					testCaseCountReser ++;
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Total Cost ($)</td>");
					PrintWriter.append("<td>"+calcultdTotalCostHotel+"</td>");
					
					try {	
						
						if(calcultdTotalCostHotel.equals(reservationdetails.getTotalCost())) {
							
							PrintWriter.append("<td>"+reservationdetails.getTotalCost()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+reservationdetails.getTotalCost()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
		
					testCaseCountReser ++;
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Total Booking Value ($)</td>");
					PrintWriter.append("<td>"+totalCostforReservation+"</td>");
					
					try {	
						
						if(totalCostforReservation.equals(reservationdetails.getBookingValue())) {
							
							PrintWriter.append("<td>"+reservationdetails.getBookingValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+reservationdetails.getBookingValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///////
					
					testCaseCountReser ++;
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Page Total ($)</td>");
					PrintWriter.append("<td>"+totalCostforReservation+"</td>");
					
					try {	
						
						if(totalCostforReservation.equals(reservationdetails.getPageTotal())) {
							
							PrintWriter.append("<td>"+reservationdetails.getPageTotal()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+reservationdetails.getPageTotal()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCountReser ++;
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report - Grand Total ($)</td>");
					PrintWriter.append("<td>"+totalCostforReservation+"</td>");
					
					try {	
						
						if(totalCostforReservation.equals(reservationdetails.getGrandTotal())) {
							
							PrintWriter.append("<td>"+reservationdetails.getGrandTotal()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+reservationdetails.getGrandTotal()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
									
					PrintWriter.append("</table>");
					
				}else{
					
					PrintWriter.append("<tr><td>"+testCaseCountReser+"</td> <td>Reservation Report Summary</td>");
					PrintWriter.append("<td>Reservation Report should be available</td>");
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						
					PrintWriter.append("</table>");	
					
				}
				
				
				
				
				
		
				//Customer Mail
				
				PrintWriter.append("<br><br>");
				PrintWriter.append("<p class='fontStyles'>Email - Customer Mail</p>");
				
				testCountCustomer = 1;
				PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
				
				if (conDetails.isCustomerMailLoaded() == true) {
					
					
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Bonotel Name</td>");
					PrintWriter.append("<td>"+PG_Properties.getProperty("Bono.Name")+"</td>");
					
					try {	
						
						if(PG_Properties.getProperty("Bono.Name").replaceAll(" ", "").equalsIgnoreCase(conDetails.getCus_BonoName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getCus_BonoName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_BonoName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Bonotel Address</td>");
					PrintWriter.append("<td>"+PG_Properties.getProperty("Bono.Address")+"</td>");
					
					try {	
						
						if(PG_Properties.getProperty("Bono.Address").replaceAll(" ", "").equalsIgnoreCase(conDetails.getCus_BonoAddress().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getCus_BonoAddress()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_BonoAddress()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Bonotel Fax/ Phone</td>");
					PrintWriter.append("<td>"+PG_Properties.getProperty("Bono.FaxNTp")+"</td>");
					
					try {	
						
						if(PG_Properties.getProperty("Bono.FaxNTp").replaceAll(" ", "").equalsIgnoreCase(conDetails.getCus_TpnFax().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getCus_TpnFax()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_TpnFax()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Booking Date</td>");
					PrintWriter.append("<td>"+hotelDetails.getReservationDate()+"</td>");
					
					try {	
						
						if(hotelDetails.getReservationDate().equalsIgnoreCase(conDetails.getCus_bookingDate())) {
							
							PrintWriter.append("<td>"+conDetails.getCus_bookingDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_bookingDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Reservation No</td>");
					PrintWriter.append("<td>"+hotelDetails.getConfirmation_ReservationNO()+"</td>");
					
					try {	
						
						if(hotelDetails.getConfirmation_ReservationNO().equalsIgnoreCase(conDetails.getCus_bookingNo())) {
							
							PrintWriter.append("<td>"+conDetails.getCus_bookingNo()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_bookingNo()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
													
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Tour Operator Name</td>");
					PrintWriter.append("<td>"+searchInfo.getTourOperatorName()+"</td>");
					
					try {	
						
						if(searchInfo.getTourOperatorName().replaceAll(" ", "").equalsIgnoreCase(conDetails.getCus_ToName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getCus_ToName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_ToName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Tour Operator Email</td>");
					PrintWriter.append("<td>"+toDetails.getTo_Email()+"</td>");
					
					try {	
						
						if(toDetails.getTo_Email().replaceAll(" ", "").equalsIgnoreCase(conDetails.getCus_ToEmail().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getCus_ToEmail()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_ToEmail()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Hotel Name</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
					
					try {	
						
						if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(conDetails.getCus_HotelName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getCus_HotelName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_HotelName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Hotel Address</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelAddress()+"</td>");
					
					try {	
						
						if(conDetails.getCus_HotelAddress().toLowerCase().replaceAll(" ", "").contains(searchInfo.getExcelHotelAddress().toLowerCase().replaceAll("#", ",").replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getCus_HotelAddress()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_HotelAddress()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Hotel City</td>");
					PrintWriter.append("<td>"+searchInfo.getCity()+"</td>");
					
					try {	
						
						if(searchInfo.getCity().replaceAll(" ", "").equalsIgnoreCase(conDetails.getCus_HotelCity().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getCus_HotelCity()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_HotelCity()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						String fullNameForCusReport = ""+hotelDetails.getCustomerTitle().get(i)+" "+hotelDetails.getCustomerFirstName().get(i)+" "+hotelDetails.getCustomerLastName().get(i)+"";
						
						testCountCustomer ++;
						PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Customer Name [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+fullNameForCusReport+"</td>");
						
						try {
							for (int j = 0; j < conDetails.getCustomerNames().size(); j++) {
																					
								if(conDetails.getCustomerNames().get(j).replaceAll(" ", "").equalsIgnoreCase(fullNameForCusReport.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+conDetails.getCustomerNames().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
					}
					
					///
					
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Check In Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
					
					try {			
						if(searchInfo.getDateFrom().equalsIgnoreCase(conDetails.getCus_CheckInDate())) {
							
							PrintWriter.append("<td>"+conDetails.getCus_CheckInDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_CheckInDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Check Out Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
					
					try {			
						if(searchInfo.getDateTo().equalsIgnoreCase(conDetails.getCus_CheckOutDate())) {
							
							PrintWriter.append("<td>"+conDetails.getCus_CheckOutDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_CheckOutDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Hotel No of Nights</td>");
					PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
					
					try {			
						if(searchInfo.getNights().equals(conDetails.getCus_Nights())) {
							
							PrintWriter.append("<td>"+conDetails.getCus_Nights()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_Nights()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Hotel No of Rooms</td>");
					PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
					
					try {			
						if(searchInfo.getRooms().equals(conDetails.getCus_Rooms())) {
							
							PrintWriter.append("<td>"+conDetails.getCus_Rooms()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_Rooms()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Hotel Adults count</td>");
					PrintWriter.append("<td>"+aduCount+"</td>");
					
					try {			
						if(aduCount.equals(conDetails.getCus_Adults())) {
							
							PrintWriter.append("<td>"+conDetails.getCus_Adults()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_Adults()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Hotel Children count</td>");
					PrintWriter.append("<td>"+childCount+"</td>");
					
					try {			
						if(childCount.equals(conDetails.getCus_Children().split(" Children")[0])) {
							
							PrintWriter.append("<td>"+conDetails.getCus_Children().split(" Children")[0]+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_Children().split(" Children")[0]+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Hotel Customer Notes</td>");
					PrintWriter.append("<td>"+hotelDetails.getPaymentsPage_CustomerNotes()+"</td>");
					
					try {			
						if(hotelDetails.getPaymentsPage_CustomerNotes().replaceAll(" ", "").equalsIgnoreCase(conDetails.getCus_CustomerNotes().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getCus_CustomerNotes()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_CustomerNotes()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					String[] roomTypesCustomer = new String[100];
					
					if (conDetails.getCus_RoomTypes().contains(",")) {
						
						roomTypesCustomer = conDetails.getCus_RoomTypes().split(",");
						
					} else {
						
						String nm = conDetails.getCus_RoomTypes();	
						roomTypesCustomer[0] = nm;
					}
				
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						testCountCustomer ++;
						PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Room Type "+(i+1)+"</td>");
						PrintWriter.append("<td>"+roomList.get(i)+"</td>");
						
						try {
							for (int j = 0; j < roomTypesCustomer.length; j++) {
																					
								if(roomTypesCustomer[j].replaceAll(" ", "").contains(roomList.get(i).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+roomTypesCustomer[j]+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
					}
					
					String[] occuBedTypesCustomer  = new String[100];
					
					if (conDetails.getCus_OccupancyRatePlan().contains(",")) {
						
						occuBedTypesCustomer = conDetails.getCus_OccupancyRatePlan().split(",");
						
					} else {
						
						String nm = conDetails.getCus_OccupancyRatePlan();	
						occuBedTypesCustomer[0] = nm;
					}
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						testCountCustomer ++;
						PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Occupancy/Rate Plan Type "+(i+1)+"</td>");
						PrintWriter.append("<td>"+occupancyList.get(i)+"</td>");
						
						try {
							for (int j = 0; j < occuBedTypesCustomer.length; j++) {
																					
								if(occuBedTypesCustomer[j].replaceAll(" ", "").equalsIgnoreCase(occupancyList.get(i).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+occuBedTypesCustomer[j]+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
					}
					
					////
					
					String[] custDailyRatesArray = conDetails.getCus_DailyRatesWithDates().split("Room");
					ArrayList<String> newListCusArray = new ArrayList<String>();
					for (int i = 0; i < excelRoomCount; i++) {
						
						String xxx = custDailyRatesArray[i+1].replace("@", " : $").replaceAll(", ", " ");
						newListCusArray.add(xxx);
						
					}
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						testCountCustomer ++;
						PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Daily Rates [Room "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+dailyRatesArray.get(i)+"</td>");
						
						try {
							
							if(newListCusArray.get(i).replaceAll(" ", "").contains(dailyRatesArray.get(i).replaceAll(" ", ""))) {
								
								PrintWriter.append("<td>"+newListCusArray.get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+newListCusArray.get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
							
							
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
				
					}
					
					
					testCountCustomer ++;
					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Customer Mail - Total Booking Charge</td>");
					PrintWriter.append("<td>"+calcultdTotalRateT+"</td>");
					
					try {	
						
						if(calcultdTotalRateT.equals(conDetails.getCus_TotalBookingCharge())) {
							
							PrintWriter.append("<td>"+conDetails.getCus_TotalBookingCharge()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getCus_TotalBookingCharge()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					
				} else {

					PrintWriter.append("<tr><td>"+testCountCustomer+"</td> <td>Email - Customer Mail</td>");
					PrintWriter.append("<td>Customer Mail should be available</td>");
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");					
				}
				
				
				PrintWriter.append("</table>");	
				
				
				
				//Hotel mail
				
				PrintWriter.append("<br><br>");
				PrintWriter.append("<p class='fontStyles'>Email - Hotel Mail</p>");
				
				testCountHotel = 1;
				PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
				
				
				if (conDetails.isHotelMailLoaded() == true) {
					
					PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Bonotel Name</td>");
					PrintWriter.append("<td>"+PG_Properties.getProperty("Bono.Name")+"</td>");
					
					try {	
						
						if(PG_Properties.getProperty("Bono.Name").replaceAll(" ", "").equalsIgnoreCase(conDetails.getHotel_BonoName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getHotel_BonoName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getHotel_BonoName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountHotel ++;
					PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Bonotel Address</td>");
					PrintWriter.append("<td>"+PG_Properties.getProperty("Bono.Address")+"</td>");
					
					try {	
						
						if(PG_Properties.getProperty("Bono.Address").replaceAll(" ", "").equalsIgnoreCase(conDetails.getHotel_BonoAddress().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getHotel_BonoAddress()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getHotel_BonoAddress()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountHotel ++;
					PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Bonotel Fax/ Phone</td>");
					PrintWriter.append("<td>"+PG_Properties.getProperty("Bono.FaxNTp")+"</td>");
					
					try {	
						
						if(PG_Properties.getProperty("Bono.FaxNTp").replaceAll(" ", "").equalsIgnoreCase(conDetails.getHotel_TpnFax().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getHotel_TpnFax()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getHotel_TpnFax()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					/////
					
				
					testCountHotel ++;
					PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Hotel Name</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
					
					try {	
						
						if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(conDetails.getHotel_HotelName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getHotel_HotelName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getHotel_HotelName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountHotel ++;
					PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Booking Date/ Time</td>");
					PrintWriter.append("<td>"+hotelDetails.getReservationDate()+"</td>");
					
					try {	
						
						if(conDetails.getHotel_BookingDate().contains(hotelDetails.getReservationDate())) {
							
							PrintWriter.append("<td>"+conDetails.getHotel_BookingDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getHotel_BookingDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCountHotel ++;
					PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Hotel City</td>");
					PrintWriter.append("<td>"+searchInfo.getCity()+"</td>");
					
					try {	
						
						if(conDetails.getHotel_city().replaceAll(" ", "").toLowerCase().contains(searchInfo.getCity().replaceAll(" ", "").toLowerCase())) {
							
							PrintWriter.append("<td>"+conDetails.getHotel_city()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getHotel_city()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					///////
					
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						PrintWriter.append("<tr><td class='fontyG'>Room : "+(i+1)+"</td><tr>"); 
						
						String fullNameForHotelReport = ""+hotelDetails.getCustomerLastName().get(i)+" "+hotelDetails.getCustomerFirstName().get(i)+" "+hotelDetails.getCustomerTitle().get(i)+"";
						
						testCountHotel ++;
						PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Customer Name [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+fullNameForHotelReport+"</td>");
						
						try {
							
							for (int j = 0; j < excelRoomCount; j++) {
								
								if(conDetails.getHotel_CustomerName().get(j).replaceAll(" ", "").equalsIgnoreCase(fullNameForHotelReport.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+conDetails.getHotel_CustomerName().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
											
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
						
						testCountHotel ++;
						PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Booking No [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+hotelDetails.getConfirmation_ReservationNO()+"</td>");
						
						try {
							
							if(hotelDetails.getConfirmation_ReservationNO().equals(conDetails.getHotel_BookingNo().get(i))) {
								
								PrintWriter.append("<td>"+conDetails.getHotel_BookingNo().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+conDetails.getHotel_BookingNo().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
							
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
						
						///
						
												
						testCountHotel ++;
						PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Room Type [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+roomList.get(i)+"</td>");
						
						try {
							
							if(conDetails.getHotel_RoomType().get(i).replaceAll(" ", "").contains(roomList.get(i).replaceAll(" ", ""))) {
								
								PrintWriter.append("<td>"+conDetails.getHotel_RoomType().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+conDetails.getHotel_RoomType().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
							
							
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
						
						testCountHotel ++;
						PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Occupancy/Rate Plan Type [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+occupancyList.get(i)+"</td>");
						
						try {
							
							if(conDetails.getHotel_OccupancyRateType().get(i).replaceAll(" ", "").equalsIgnoreCase(occupancyList.get(i).replaceAll(" ", ""))) {
								
								PrintWriter.append("<td>"+conDetails.getHotel_OccupancyRateType().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+conDetails.getHotel_OccupancyRateType().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
							
							
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
						
						testCountHotel ++;
						PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Arrival Date [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
						
						try {			
							if(searchInfo.getDateFrom().equalsIgnoreCase(conDetails.getHotel_ArrivalDate().get(i))) {
								
								PrintWriter.append("<td>"+conDetails.getHotel_ArrivalDate().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+conDetails.getHotel_ArrivalDate().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCountHotel ++;
						PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - No of Nights [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
						
						try {			
							if(searchInfo.getNights().equals(conDetails.getHotel_Nights().get(i))) {
								
								PrintWriter.append("<td>"+conDetails.getHotel_Nights().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+conDetails.getHotel_Nights().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCountHotel ++;
						PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - No of Adults [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+adultList.get(i)+"</td>");
						
						try {			
							if(adultList.get(i).equals(conDetails.getHotel_Adults().get(i))) {
								
								PrintWriter.append("<td>"+conDetails.getHotel_Adults().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+conDetails.getHotel_Adults().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCountHotel ++;
						PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - No of Children [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+childList.get(i)+"</td>");
						
						try {			
							if(childList.get(i).equals(conDetails.getHotel_Children().get(i))) {
								
								PrintWriter.append("<td>"+conDetails.getHotel_Children().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+conDetails.getHotel_Children().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
						
						String yearReservation = searchInfo.getDateFrom().split("-")[2];				
						String hss = dailyNetRatesArray.get(i);
						String gggg  = conDetails.getHotel_DailyRates().get(i).replace("@", "-"+yearReservation+" : ");
						
						testCountHotel ++;
						PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Daily Rates [Room "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+hss+"</td>");
						
						try {
							
							if(gggg.replace(" ", "").replace(",", "").contains(hss.replace(" ", ""))) {
								
								PrintWriter.append("<td>"+gggg+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+gggg+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
							
							
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
						
						double listx = Double.parseDouble(hotelDetails.getDailyRatesCostList().get(i))* Integer.parseInt(searchInfo.getNights());						
						String calcultdTotalRate = "$" + df.format(listx);
						
						testCountHotel ++;
						PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Hotel Mail - Total Room Cost [Room "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+calcultdTotalRate+"</td>");
						
						try {
							
							if(calcultdTotalRate.equals(conDetails.getHotel_TotalRoomCost().get(i))) {
								
								PrintWriter.append("<td>"+conDetails.getHotel_TotalRoomCost().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+conDetails.getHotel_TotalRoomCost().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
							
							
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
						
					}
					
//					double tValuee = 0;
//					for (int i = 0; i < Integer.parseInt(searchInfo.getRooms()); i++) {
//						
//						tValuee = tValuee + (Double.parseDouble(hotelDetails.getDailyRatesCostList().get(i)) * Integer.parseInt(searchInfo.getNights()));
//					}
//					
//					String calcultdTotalRateTHotel = "$" + df.format(tValuee);
					
					
					
					
					
					
					
					
						
					
					
				} else {
					
					PrintWriter.append("<tr><td>"+testCountHotel+"</td> <td>Email - Hotel Mail</td>");
					PrintWriter.append("<td>Hotel Mail should be available</td>");
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					
				}
				
				PrintWriter.append("</table>");	
				
				
				
				//Portal Operator Mail

				PrintWriter.append("<br><br>");
				PrintWriter.append("<p class='fontStyles'>Email - Portal Operator Mail</p>");
				
				testCountPO = 1;
				PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
				
				if (conDetails.isPortalOperatorLoaded() == true) {
					
					
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Bonotel Name</td>");
					PrintWriter.append("<td>"+PG_Properties.getProperty("Bono.Name")+"</td>");
					
					try {	
						
						if(PG_Properties.getProperty("Bono.Name").replaceAll(" ", "").equalsIgnoreCase(conDetails.getPO_BonoName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getPO_BonoName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_BonoName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Bonotel Address</td>");
					PrintWriter.append("<td>"+PG_Properties.getProperty("Bono.Address")+"</td>");
					
					try {	
						
						if(PG_Properties.getProperty("Bono.Address").replaceAll(" ", "").equalsIgnoreCase(conDetails.getPO_BonoAddress().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getPO_BonoAddress()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_BonoAddress()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Bonotel Fax/ Phone</td>");
					PrintWriter.append("<td>"+PG_Properties.getProperty("Bono.FaxNTp")+"</td>");
					
					try {	
						
						if(PG_Properties.getProperty("Bono.FaxNTp").replaceAll(" ", "").equalsIgnoreCase(conDetails.getPO_TpnFax().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getPO_TpnFax()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_TpnFax()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Booking Date</td>");
					PrintWriter.append("<td>"+hotelDetails.getReservationDate()+"</td>");
					
					try {	
						
						if(hotelDetails.getReservationDate().equalsIgnoreCase(conDetails.getPO_bookingDate())) {
							
							PrintWriter.append("<td>"+conDetails.getPO_bookingDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_bookingDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Reservation No</td>");
					PrintWriter.append("<td>"+hotelDetails.getConfirmation_ReservationNO()+"</td>");
					
					try {	
						
						if(hotelDetails.getConfirmation_ReservationNO().equalsIgnoreCase(conDetails.getPO_BookingNo())) {
							
							PrintWriter.append("<td>"+conDetails.getPO_BookingNo()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_BookingNo()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
													
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Tour Operator Name</td>");
					PrintWriter.append("<td>"+searchInfo.getTourOperatorName()+"</td>");
					
					try {	
						
						if(searchInfo.getTourOperatorName().replaceAll(" ", "").equalsIgnoreCase(conDetails.getPO_ToName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getPO_ToName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_ToName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Tour Operator Email</td>");
					PrintWriter.append("<td>"+toDetails.getTo_Email()+"</td>");
					
					try {	
						
						if(toDetails.getTo_Email().replaceAll(" ", "").equalsIgnoreCase(conDetails.getPO_ToEmail().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getPO_ToEmail()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_ToEmail()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Hotel Name</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
					
					try {	
						
						if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(conDetails.getPO_HotelName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getPO_HotelName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_HotelName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Hotel Address</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelAddress()+"</td>");
					
					try {	
						
						if(conDetails.getPO_HotelAddress().toLowerCase().replaceAll(" ", "").contains(searchInfo.getExcelHotelAddress().toLowerCase().replaceAll("#", ",").replaceAll(" ", ""))) {
									
							PrintWriter.append("<td>"+conDetails.getPO_HotelAddress()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_HotelAddress()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					////
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Hotel City</td>");
					PrintWriter.append("<td>"+searchInfo.getCity()+"</td>");
					
					try {	
						
						if(searchInfo.getCity().replaceAll(" ", "").equalsIgnoreCase(conDetails.getPO_HotelCity().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getPO_HotelCity()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_HotelCity()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						String fullNameForCusReport = ""+hotelDetails.getCustomerTitle().get(i)+" "+hotelDetails.getCustomerFirstName().get(i)+" "+hotelDetails.getCustomerLastName().get(i)+"";
						
						testCountPO ++;
						PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Customer Name [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+fullNameForCusReport+"</td>");
						
						try {
							for (int j = 0; j < conDetails.getCustomerNames().size(); j++) {
																					
								if(conDetails.getPO_customerNames().get(j).replaceAll(" ", "").equalsIgnoreCase(fullNameForCusReport.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+conDetails.getPO_customerNames().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
					}
					
					///
					
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Check In Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
					
					try {			
						if(searchInfo.getDateFrom().equalsIgnoreCase(conDetails.getPO_CheckInDate())) {
							
							PrintWriter.append("<td>"+conDetails.getPO_CheckInDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_CheckInDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Check Out Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
					
					try {			
						if(searchInfo.getDateTo().equalsIgnoreCase(conDetails.getPO_CheckOutDate())) {
							
							PrintWriter.append("<td>"+conDetails.getPO_CheckOutDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_CheckOutDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					///
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Hotel No of Nights</td>");
					PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
					
					try {			
						if(searchInfo.getNights().equals(conDetails.getPO_Nights())) {
							
							PrintWriter.append("<td>"+conDetails.getPO_Nights()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_Nights()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Hotel No of Rooms</td>");
					PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
					
					try {			
						if(searchInfo.getRooms().equals(conDetails.getPO_Rooms())) {
							
							PrintWriter.append("<td>"+conDetails.getPO_Rooms()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_Rooms()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Hotel Adults count</td>");
					PrintWriter.append("<td>"+aduCount+"</td>");
					
					try {			
						if(aduCount.equals(conDetails.getPO_Adults())) {
							
							PrintWriter.append("<td>"+conDetails.getPO_Adults()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_Adults()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Hotel Children count</td>");
					PrintWriter.append("<td>"+childCount+"</td>");
					
					try {			
						if(childCount.equals(conDetails.getPO_Children().split(" Children")[0])) {
							
							PrintWriter.append("<td>"+conDetails.getPO_Children().split(" Children")[0]+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_Children().split(" Children")[0]+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Hotel Customer Notes</td>");
					PrintWriter.append("<td>"+hotelDetails.getPaymentsPage_CustomerNotes()+"</td>");
					
					try {			
						if(hotelDetails.getPaymentsPage_CustomerNotes().replaceAll(" ", "").equalsIgnoreCase(conDetails.getPO_CustomerNotes().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+conDetails.getPO_CustomerNotes()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_CustomerNotes()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					String[] roomTypesCustomer = new String[100];
								
					if (conDetails.getPO_RoomTypes().contains(",")) {
						
						roomTypesCustomer = conDetails.getPO_RoomTypes().split(",");
						
					} else {
						
						String nm = conDetails.getPO_RoomTypes();	
						roomTypesCustomer[0] = nm;
					}
					
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						testCountPO ++;
						PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Room Type "+(i+1)+"</td>");
						PrintWriter.append("<td>"+roomList.get(i)+"</td>");
						
						try {
							for (int j = 0; j < roomTypesCustomer.length; j++) {
																					
								if(roomTypesCustomer[j].replaceAll(" ", "").contains(roomList.get(i).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+roomTypesCustomer[j]+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
					}
					
					String[] occuBedTypesCustomer = new String[100];
					
					if (conDetails.getPO_OccupancyRatePlan().contains(",")) {
						
						occuBedTypesCustomer = conDetails.getPO_OccupancyRatePlan().split(",");
						
					} else {
						
						String nm = conDetails.getPO_OccupancyRatePlan();	
						occuBedTypesCustomer[0] = nm;
					}
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						testCountPO ++;
						PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Occupancy/Rate Plan Type "+(i+1)+"</td>");
						PrintWriter.append("<td>"+occupancyList.get(i)+"</td>");
						
						try {
							for (int j = 0; j < occuBedTypesCustomer.length; j++) {
																					
								if(occuBedTypesCustomer[j].replaceAll(" ", "").equalsIgnoreCase(occupancyList.get(i).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+occuBedTypesCustomer[j]+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
					}
					
					
					String[] custDailyRatesArray = conDetails.getPO_DailyRatesWithDates().split("Room");
					ArrayList<String> newListPOArray = new ArrayList<String>();
					for (int i = 0; i < excelRoomCount; i++) {
						
						String xxx = custDailyRatesArray[i+1].replace("@", " : $").replaceAll(", ", " ");
						newListPOArray.add(xxx);
						
					}
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						testCountPO ++;
						PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Daily Rates [Room "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+dailyRatesArray.get(i)+"</td>");
						
						try {
							
							if(newListPOArray.get(i).replaceAll(" ", "").contains(dailyRatesArray.get(i).replaceAll(" ", ""))) {
								
								PrintWriter.append("<td>"+newListPOArray.get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+newListPOArray.get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
							
							
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
				
					}
					
					testCountPO ++;
					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Portal Operator Mail - Total Booking Charge</td>");
					PrintWriter.append("<td>"+calcultdTotalRateT+"</td>");
					
					try {	
						
						if(calcultdTotalRateT.equals(conDetails.getPO_TotalBookingCharge())) {
							
							PrintWriter.append("<td>"+conDetails.getPO_TotalBookingCharge()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+conDetails.getPO_TotalBookingCharge()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
						
					
				} else {

					PrintWriter.append("<tr><td>"+testCountPO+"</td> <td>Email - Portal Operator Mail</td>");
					PrintWriter.append("<td>Portal Operator Mail should be available</td>");
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						
				}
				
				
				PrintWriter.append("</table>");	
				
				
				
					
			}else{
				
				PrintWriter.append("<tr><td>"+testCaseCount+"</td> <td>Results Availability</td>");
				PrintWriter.append("<td>Results should be available</td>");
				PrintWriter.append("<td>Not Available</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					
				PrintWriter.append("</table>");	
			}
			
			
			
		
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void getCancellationReport(WebDriver webDriver){
		
		try {
			
			if (hotelDetails.isResultsAvailable() == true) {
				
				PrintWriter.append("<br><br>");
				PrintWriter.append("<p class='fontStyles'>Cancellation Summary</p>");
				
				testCaseCancel = 1;
				PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
				
				if (cancelDetails.isCancellationReportLoaded() == true) {
					
					
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Hotel Name</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
					
					try {	
						
						if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(cancelDetails.getHotelName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+cancelDetails.getHotelName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+cancelDetails.getHotelName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Hotel Country</td>");
					PrintWriter.append("<td>"+searchInfo.getCountry()+"</td>");
					
					try {			
						if(searchInfo.getCountry().equalsIgnoreCase(cancelDetails.getHotelCity().split("/")[3].replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+cancelDetails.getHotelCity().split("/")[3]+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+cancelDetails.getHotelCity().split("/")[3]+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Hotel City</td>");
					PrintWriter.append("<td>"+searchInfo.getCity()+"</td>");
					
					try {			
						if(searchInfo.getCity().equalsIgnoreCase(cancelDetails.getHotelCity().split("/")[0].replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+cancelDetails.getHotelCity().split("/")[0]+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+cancelDetails.getHotelCity().split("/")[0]+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					DateFormat dateFormat = new SimpleDateFormat("MMM-dd-yy");
					Date inputDateArr = dateFormat.parse(cancelDetails.getArrivalDate());
					Date inputDepartureD = dateFormat.parse(cancelDetails.getDepartureDate());
					Date inputBookingD = dateFormat.parse(cancelDetails.getBookingDate());
					
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
					String arrivalDate = sdf.format(inputDateArr);
					String DepartureDate = sdf.format(inputDepartureD);
					String bookingDate = sdf.format(inputBookingD);
									
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Arrival Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
					
					try {			
						if(searchInfo.getDateFrom().equalsIgnoreCase(arrivalDate)) {
							
							PrintWriter.append("<td>"+arrivalDate+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+arrivalDate+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Departure Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
					
					try {			
						if(searchInfo.getDateTo().equalsIgnoreCase(DepartureDate)) {
							
							PrintWriter.append("<td>"+DepartureDate+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+DepartureDate+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
									
					
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Hotel No of Nights</td>");
					PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
					
					try {			
						if(searchInfo.getNights().equals(cancelDetails.getNights())) {
							
							PrintWriter.append("<td>"+cancelDetails.getNights()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+cancelDetails.getNights()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Hotel No of Rooms</td>");
					PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
					
					try {			
						if(searchInfo.getRooms().equals(cancelDetails.getRooms())) {
							
							PrintWriter.append("<td>"+cancelDetails.getRooms()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+cancelDetails.getRooms()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Hotel Adults count</td>");
					PrintWriter.append("<td>"+aduCount+"</td>");
					
					try {			
						if(aduCount.equals(cancelDetails.getAdults())) {
							
							PrintWriter.append("<td>"+cancelDetails.getAdults()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+cancelDetails.getAdults()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Hotel Children count</td>");
					PrintWriter.append("<td>"+childCount+"</td>");
					
					try {			
						if(childCount.equals(cancelDetails.getChildren())) {
							
							PrintWriter.append("<td>"+cancelDetails.getChildren()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+cancelDetails.getChildren()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Booking Date</td>");
					PrintWriter.append("<td>"+hotelDetails.getReservationDate()+"</td>");
					
					try {			
						if(hotelDetails.getReservationDate().equals(bookingDate)) {
							
							PrintWriter.append("<td>"+bookingDate+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+bookingDate+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Reservation No</td>");
					PrintWriter.append("<td>"+hotelDetails.getConfirmation_ReservationNO()+"</td>");
					
					try {	
						
						if(hotelDetails.getConfirmation_ReservationNO().equals(cancelDetails.getBookingNo())) {
							
							PrintWriter.append("<td>"+cancelDetails.getBookingNo()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+cancelDetails.getBookingNo()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Booking Channel</td>");
					PrintWriter.append("<td>"+searchInfo.getBookingChannel()+"</td>");
					
					try {	
						
						if(cancelDetails.getBookingChannel().toLowerCase().contains(searchInfo.getBookingChannel().toLowerCase())) {
							
							PrintWriter.append("<td>"+cancelDetails.getBookingChannel()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+cancelDetails.getBookingChannel()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					for (int i = 0; i < excelRoomCount; i++) {
					
						PrintWriter.append("<tr><td class='fontyG'>Room : "+(i+1)+"</td><tr>"); 
						
						testCaseCancel ++;
						PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Customer Title (Room : "+(i+1)+")</td>");
						PrintWriter.append("<td>"+hotelDetails.getCustomerTitle().get(i)+"</td>");
						
						try {			
							if(hotelDetails.getCustomerTitle().get(i).equalsIgnoreCase(cancelDetails.getCusTitle().get(i))) {
								
								PrintWriter.append("<td>"+cancelDetails.getCusTitle().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+cancelDetails.getCusTitle().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel ++;
						PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Customer First Name (Room : "+(i+1)+")</td>");
						PrintWriter.append("<td>"+hotelDetails.getCustomerFirstName().get(i)+"</td>");
						
						try {			
							if(hotelDetails.getCustomerFirstName().get(i).equalsIgnoreCase(cancelDetails.getCusFirstName().get(i))) {
								
								PrintWriter.append("<td>"+cancelDetails.getCusFirstName().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+cancelDetails.getCusFirstName().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel ++;
						PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Customer Last Name (Room : "+(i+1)+")</td>");
						PrintWriter.append("<td>"+hotelDetails.getCustomerLastName().get(i)+"</td>");
						
						try {			
							if(hotelDetails.getCustomerLastName().get(i).equalsIgnoreCase(cancelDetails.getCusLastName().get(i))) {
								
								PrintWriter.append("<td>"+cancelDetails.getCusLastName().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+cancelDetails.getCusLastName().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
												
						
						testCaseCancel ++;
						PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Room "+(i+1)+" - Adults count</td>");
						PrintWriter.append("<td>"+adultList.get(i)+"</td>");
						
						try {			
							if(adultList.get(i).equals(cancelDetails.getAdultsList().get(i))) {
								
								PrintWriter.append("<td>"+cancelDetails.getAdultsList().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+cancelDetails.getAdultsList().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseCancel ++;
						PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Room "+(i+1)+" - Childs count</td>");
						PrintWriter.append("<td>"+childList.get(i)+"</td>");
						
						try {			
							if(childList.get(i).equals(cancelDetails.getChildrenList().get(i))) {
								
								PrintWriter.append("<td>"+cancelDetails.getChildrenList().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+cancelDetails.getChildrenList().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						
						testCaseCancel ++;
						PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Room "+(i+1)+" - Room Type</td>");
						PrintWriter.append("<td>"+roomList.get(i)+"</td>");
						
						try {			
							if(roomList.get(i).replaceAll(" ", "").equalsIgnoreCase(cancelDetails.getRoomTypeList().get(i).replaceAll(" ", ""))) {
								
								PrintWriter.append("<td>"+cancelDetails.getRoomTypeList().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+cancelDetails.getRoomTypeList().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
										
						testCaseCancel ++;
						PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Room "+(i+1)+" - Occupany/Rate plan</td>");
						PrintWriter.append("<td>"+occupancyList.get(i)+"</td>");
						
						try {	
								
							if(cancelDetails.getBedTypeList().get(i).replaceAll(" ", "").toLowerCase().contains(occupancyList.get(i).replaceAll("/", "(").replaceAll(" ", "").toLowerCase())) {
								
								PrintWriter.append("<td>"+cancelDetails.getBedTypeList().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+cancelDetails.getBedTypeList().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						List<String> resultsRoomDailyRates = new ArrayList<String>();
						List<String> cancellationDailyRates = new ArrayList<String>();
						
						for (int j = 0; j < Integer.parseInt(searchInfo.getNights()); j++) {
							
							String rateDay = cancelDetails.getDailyRates().get(i).get(j);
							cancellationDailyRates.add(rateDay);
							String rateDay2 = dailyRatesResultsPage.get(i).get(j).split("\\.")[0];
							resultsRoomDailyRates.add(rateDay2);			
						}
						
						String match1 = "";
						String match2 = "";
						String dailyRateRoomCancelCalculatd = hotelDetails.getDailyRatesCancelList().get(i);
					
						testCaseCancel ++;
						PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Room "+(i+1)+" - Daily Rates</td>");
						
						for (int k = 0; k < Integer.parseInt(searchInfo.getNights()); k++) {
							
							match1 = match1 + ""+dailyRateRoomCancelCalculatd+"" + " ";
							match2 = match2 + ""+cancellationDailyRates.get(k)+"" + " ";
							
						}
					
						PrintWriter.append("<td>"+match1+"</td>");
											
						try {	
							
							if(match1.equalsIgnoreCase(match2)) {
								
								PrintWriter.append("<td>"+match2+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+match2+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
							
						testCaseCancel ++;
						PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Room "+(i+1)+" - Room Total Value</td>");
						PrintWriter.append("<td>"+calcultdTotalRoomRates.get(i)+"</td>");
						
						try {	
							
							if(calcultdTotalRoomRates.get(i).contains(cancelDetails.getTotalValue().get(i))) {
								
								PrintWriter.append("<td>"+cancelDetails.getTotalValue().get(i)+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+cancelDetails.getTotalValue().get(i)+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					
					}
				
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Sub Total</td>");
					PrintWriter.append("<td>"+totalCostforReservation+"</td>");
					
					try {	
						
						if(cancelDetails.getSubTotalLast().contains(totalCostforReservation)) {
							
							PrintWriter.append("<td>"+cancelDetails.getSubTotalLast()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+cancelDetails.getSubTotalLast()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseCancel ++;
					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary - Total Payable</td>");
					PrintWriter.append("<td>"+totalCostforReservation+"</td>");
					
					try {	
						
						if(cancelDetails.getTotalPayable().contains(totalCostforReservation)) {
							
							PrintWriter.append("<td>"+cancelDetails.getTotalPayable()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+cancelDetails.getTotalPayable()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					
					PrintWriter.append("</table>");	
					
					
					
				} else {

					PrintWriter.append("<tr><td>"+testCaseCancel+"</td> <td>Cancellation Summary</td>");
					PrintWriter.append("<td>Cancellation page should be available</td>");
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						
					PrintWriter.append("</table>");	
					
				}
				
				///////////
				
				
				PrintWriter.append("<br><br>");
				PrintWriter.append("<p class='fontStyles'>Modification Details</p>");
				
				testCaseModify = 1;
				PrintWriter.append("<br><br><table><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th><th>Test Status</th></tr>");
				
				PrintWriter.append("<tr><td class='fontiiii'>Modification - Summary</td><tr>"); 
				
				if (modDetails.isModificationReportLoaded() == true) {
				
					
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Hotel Name</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
					
					try {	
						
						if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(modDetails.getSummary_hotelName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_hotelName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_hotelName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Hotel Address</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelAddress()+"</td>");
					
					try {	
						
						if(modDetails.getSummary_hotelAddress().toLowerCase().replaceAll(" ", "").contains(searchInfo.getExcelHotelAddress().toLowerCase().replaceAll("#", ",").replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_hotelAddress()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_hotelAddress()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					String cityStates = searchInfo.getStateCity().split(" - ")[1] + "/" + searchInfo.getStateCity().split(" - ")[0];
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Hotel City / State</td>");
					PrintWriter.append("<td>"+cityStates+"</td>");
					
					try {	
						
						if(modDetails.getSummary_cityStateCountry().toLowerCase().replaceAll(" ", "").contains(cityStates.toLowerCase().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_cityStateCountry()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_cityStateCountry()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					String modCheckInD = modDetails.getSummary_chechInDate();
					String modCheckOutD = modDetails.getSummary_checkOutDate();
					String modBookedDa = modDetails.getSummary_bookingDate();
					DateFormat dateFormat = new SimpleDateFormat("MMM-dd-yy");
					Date inputDateCIn = dateFormat.parse(modCheckInD);
					Date inputDateCOut = dateFormat.parse(modCheckOutD);
					Date bookedDt = dateFormat.parse(modBookedDa);
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Check In Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
					
					try {			
						if(searchInfo.getDateFrom().equalsIgnoreCase(sdf.format(inputDateCIn))) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_chechInDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_chechInDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Check Out Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
					
					try {			
						if(searchInfo.getDateTo().equalsIgnoreCase(sdf.format(inputDateCOut))) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_checkOutDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_checkOutDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Booked Date</td>");
					PrintWriter.append("<td>"+hotelDetails.getReservationDate()+"</td>");
					
					try {			
						if(hotelDetails.getReservationDate().equalsIgnoreCase(sdf.format(bookedDt))) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_bookingDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_bookingDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Hotel No of Nights</td>");
					PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
					
					try {			
						if(searchInfo.getNights().equals(modDetails.getSummary_nights())) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_nights()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_nights()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Hotel No of Rooms</td>");
					PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
					
					try {			
						if(searchInfo.getRooms().equals(modDetails.getSummary_rooms())) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_rooms()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_rooms()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Hotel Adults count</td>");
					PrintWriter.append("<td>"+aduCount+"</td>");
					
					try {			
						if(aduCount.equals(modDetails.getSummary_adults())) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_adults()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_adults()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Hotel Children count</td>");
					PrintWriter.append("<td>"+childCount+"</td>");
					
					try {			
						if(childCount.equals(modDetails.getSummary_children())) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_children()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_children()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Reservation No</td>");
					PrintWriter.append("<td>"+hotelDetails.getConfirmation_ReservationNO()+"</td>");
					
					try {	
						
						if(hotelDetails.getConfirmation_ReservationNO().equals(modDetails.getSummary_BookingNo())) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_BookingNo()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_BookingNo()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Booking Channel</td>");
					PrintWriter.append("<td>"+searchInfo.getBookingChannel()+"</td>");
					
					try {	
						
						if(modDetails.getSummary_BookingSource().toLowerCase().contains(searchInfo.getBookingChannel().toLowerCase())) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_BookingSource()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_BookingSource()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						String fullNameForCusReport = ""+hotelDetails.getCustomerTitle().get(i)+" "+hotelDetails.getCustomerLastName().get(i)+" "+hotelDetails.getCustomerFirstName().get(i)+"";
						String modificationfullNameForCusReport = ""+modDetails.getSummary_cusTitle().get(i)+" "+modDetails.getSummary_cusLname().get(i)+" "+modDetails.getSummary_cusFName().get(i)+"";
			
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Customer Name [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+fullNameForCusReport+"</td>");
						
						try {
							
							if(modificationfullNameForCusReport.replaceAll(" ", "").equalsIgnoreCase(fullNameForCusReport.replaceAll(" ", ""))) {
								
								PrintWriter.append("<td>"+modificationfullNameForCusReport+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+modificationfullNameForCusReport+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
												
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}
					
					
					
					for (int i = 0; i < excelRoomCount; i++) {

						
						PrintWriter.append("<tr><td class='fontyG'>Room : "+(i+1)+"</td><tr>"); 
					
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Room Type - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+roomList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getSummary_roomTypeList().size(); j++) {
								
								if(roomList.get(i).replaceAll(" ", "").equalsIgnoreCase(modDetails.getSummary_roomTypeList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSummary_roomTypeList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
						////////
						
						String OccuPancyRate = hotelDetails.getConfirmationTD_RoomBedType().get(i) + "(" + hotelDetails.getConfirmationTD_RoomRatePlan().get(i);
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Board Type - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+OccuPancyRate+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getSummary_OccupancyTypeList().size(); j++) {
								
								if(modDetails.getSummary_OccupancyTypeList().get(j).replaceAll(" ", "").contains(OccuPancyRate.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSummary_OccupancyTypeList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
					
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Adults count - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+adultList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getSummary_roomTypeList().size(); j++) {
								
								if(adultList.get(i).replaceAll(" ", "").equals(modDetails.getSummary_AdultsList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSummary_AdultsList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Childs count - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+childList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getSummary_roomTypeList().size(); j++) {
								
								if(childList.get(i).replaceAll(" ", "").equals(modDetails.getSummary_childrenList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSummary_childrenList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						String fullNameCustomer = hotelDetails.getCustomerFirstName().get(i) + hotelDetails.getCustomerLastName().get(i) ;
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Guest Name - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+fullNameCustomer+"</td>");
						
						try {
							for (int j = 0; j < modDetails.getSummary_roomTypeList().size(); j++) {
																					
								if(modDetails.getSummary_GuestName().get(j).replaceAll(" ", "").equalsIgnoreCase(fullNameCustomer.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSummary_GuestName().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
						///
						
						List<String> resultsRoomDailyRates = new ArrayList<String>();
						List<String> modifyDailyRates = new ArrayList<String>();
						
						for (int j = 0; j < Integer.parseInt(searchInfo.getNights()); j++) {
							
							String rateDay = modDetails.getSummary_DailyRates().get(i).get(j);
							modifyDailyRates.add(rateDay);
							String rateDay2 = dailyRatesResultsPage.get(i).get(j).split("\\.")[0];
							resultsRoomDailyRates.add(rateDay2);			
						}
						
						String match1 = "";
						String match2 = "";
						String dailyRateRoomCancelCalculatd = hotelDetails.getDailyRatesCancelList().get(i);
					
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Daily Rates - Room "+(i+1)+"</td>");
						
						for (int k = 0; k < Integer.parseInt(searchInfo.getNights()); k++) {
							
							match1 = match1 + ""+dailyRateRoomCancelCalculatd+"" + " ";
							match2 = match2 + ""+modifyDailyRates.get(k)+"" + " ";
							
						}
					
						PrintWriter.append("<td>"+match1+"</td>");
											
						try {	
							
							if(match1.equalsIgnoreCase(match2)) {
								
								PrintWriter.append("<td>"+match2+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+match2+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Room Total Value - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+calcultdTotalRoomRates.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getSummary_roomTypeList().size(); j++) {
								
								if(calcultdTotalRoomRates.get(i).replaceAll(" ", "").equalsIgnoreCase(modDetails.getSummary_Total().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSummary_Total().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					
					}
					
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Total Cost</td>");
					PrintWriter.append("<td>"+calcultdTotalCostHotel+"</td>");
					
					try {	
						
						if(modDetails.getSummary_TotalCost().contains(calcultdTotalCostHotel)) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_TotalCost()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_TotalCost()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
		
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Summary - Total Booking Value ($)</td>");
					PrintWriter.append("<td>"+totalCostforReservation+"</td>");
					
					try {	
						
						if(modDetails.getSummary_TotalBookingValue().contains(totalCostforReservation)) {
							
							PrintWriter.append("<td>"+modDetails.getSummary_TotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSummary_TotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					PrintWriter.append("<tr><td class='fontiiii'>Modification - Guest Details</td><tr>"); 
					
					
					for (int i = 0; i < excelRoomCount; i++) {
						
						String fullNameForCusReport = ""+hotelDetails.getCustomerTitle().get(i)+" "+hotelDetails.getCustomerLastName().get(i)+" "+hotelDetails.getCustomerFirstName().get(i)+"";
						String modificationfullNameForCusReport = ""+modDetails.getGD_cusTitle().get(i)+" "+modDetails.getGD_cusLname().get(i)+" "+modDetails.getGD_cusFName().get(i)+"";
			
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Guest Details - Customer Name [Room : "+(i+1)+"]</td>");
						PrintWriter.append("<td>"+fullNameForCusReport+"</td>");
						
						try {
							
							if(modificationfullNameForCusReport.replaceAll(" ", "").equalsIgnoreCase(fullNameForCusReport.replaceAll(" ", ""))) {
								
								PrintWriter.append("<td>"+modificationfullNameForCusReport+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+modificationfullNameForCusReport+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
												
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					}
					
					
					PrintWriter.append("<tr><td class='fontiiii'>Modification - Stay Period</td><tr>"); 
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Hotel Name</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
					
					try {	
						
						if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(modDetails.getSP_HotelName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+modDetails.getSP_HotelName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSP_HotelName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					String modSPCheckInD = modDetails.getSP_checkInDate();
					String modSPCheckOutD = modDetails.getSP_checkOutDate();					
					DateFormat dateFormatSP = new SimpleDateFormat("MMM-dd-yy");
					Date inputDateCInSP = dateFormatSP.parse(modSPCheckInD);
					Date inputDateCOutSP = dateFormatSP.parse(modSPCheckOutD);					
					SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM-yyyy");
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Check In Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
					
					try {			
						if(searchInfo.getDateFrom().equalsIgnoreCase(sdf1.format(inputDateCInSP))) {
							
							PrintWriter.append("<td>"+modDetails.getSP_checkInDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSP_checkInDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Check Out Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
					
					try {			
						if(searchInfo.getDateTo().equalsIgnoreCase(sdf1.format(inputDateCOutSP))) {
							
							PrintWriter.append("<td>"+modDetails.getSP_checkOutDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSP_checkOutDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Hotel No of Nights</td>");
					PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
					
					try {			
						if(searchInfo.getNights().equals(modDetails.getSP_Nights())) {
							
							PrintWriter.append("<td>"+modDetails.getSP_Nights()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSP_Nights()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Hotel No of Rooms</td>");
					PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
					
					try {			
						if(searchInfo.getRooms().equals(modDetails.getSP_Rooms())) {
							
							PrintWriter.append("<td>"+modDetails.getSP_Rooms()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSP_Rooms()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Hotel Adults count</td>");
					PrintWriter.append("<td>"+aduCount+"</td>");
					
					try {			
						if(aduCount.equals(modDetails.getSP_Adults())) {
							
							PrintWriter.append("<td>"+modDetails.getSP_Adults()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSP_Adults()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Hotel Children count</td>");
					PrintWriter.append("<td>"+childCount+"</td>");
					
					try {			
						if(childCount.equals(modDetails.getSP_Child())) {
							
							PrintWriter.append("<td>"+modDetails.getSP_Child()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSP_Child()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					
					
					for (int i = 0; i < excelRoomCount; i++) {

						
						PrintWriter.append("<tr><td class='fontyG'>Room : "+(i+1)+"</td><tr>"); 
					
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Room Type - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+roomList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getSP_roomTypeList().size(); j++) {
								
								if(roomList.get(i).replaceAll(" ", "").equalsIgnoreCase(modDetails.getSP_roomTypeList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSP_roomTypeList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
						////////
						
						String OccuPancyRate = hotelDetails.getConfirmationTD_RoomBedType().get(i) + "(" + hotelDetails.getConfirmationTD_RoomRatePlan().get(i);
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Board Type - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+OccuPancyRate+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getSP_roomTypeList().size(); j++) {
								
								if(modDetails.getSP_OccupancyTypeList().get(j).replaceAll(" ", "").contains(OccuPancyRate.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSP_OccupancyTypeList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
					
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Adults count - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+adultList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getSP_roomTypeList().size(); j++) {
								
								if(adultList.get(i).replaceAll(" ", "").equals(modDetails.getSP_AdultsList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSP_AdultsList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Childs count - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+childList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getSP_roomTypeList().size(); j++) {
								
								if(childList.get(i).replaceAll(" ", "").equals(modDetails.getSP_childrenList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSP_childrenList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						String fullNameCustomer = hotelDetails.getCustomerFirstName().get(i) + hotelDetails.getCustomerLastName().get(i) ;
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Guest Name - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+fullNameCustomer+"</td>");
						
						try {
							for (int j = 0; j < modDetails.getSP_roomTypeList().size(); j++) {
																					
								if(modDetails.getSP_GuestName().get(j).replaceAll(" ", "").equalsIgnoreCase(fullNameCustomer.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSP_GuestName().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
						///
						
						List<String> resultsRoomDailyRates = new ArrayList<String>();
						List<String> modifyDailyRates = new ArrayList<String>();
						
						for (int j = 0; j < Integer.parseInt(searchInfo.getNights()); j++) {
							
							String rateDay = modDetails.getSP_DailyRates().get(i).get(j);
							modifyDailyRates.add(rateDay);
							String rateDay2 = dailyRatesResultsPage.get(i).get(j).split("\\.")[0];
							resultsRoomDailyRates.add(rateDay2);			
						}
						
						String match1 = "";
						String match2 = "";
						String dailyRateRoomCancelCalculatd = hotelDetails.getDailyRatesCancelList().get(i);
					
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Daily Rates - Room "+(i+1)+"</td>");
						
						for (int k = 0; k < Integer.parseInt(searchInfo.getNights()); k++) {
							
							match1 = match1 + ""+dailyRateRoomCancelCalculatd+"" + " ";
							match2 = match2 + ""+modifyDailyRates.get(k)+"" + " ";
							
						}
					
						PrintWriter.append("<td>"+match1+"</td>");
											
						try {	
							
							if(match1.equalsIgnoreCase(match2)) {
								
								PrintWriter.append("<td>"+match2+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+match2+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Room Total Value - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+calcultdTotalRoomRates.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getSP_roomTypeList().size(); j++) {
								
								if(calcultdTotalRoomRates.get(i).replaceAll(" ", "").equalsIgnoreCase(modDetails.getSP_Total().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getSP_Total().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					
					}
					
					////
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Total Cost</td>");
					PrintWriter.append("<td>"+calcultdTotalCostHotel+"</td>");
					
					try {	
						
						if(modDetails.getSP_TotalCost().contains(calcultdTotalCostHotel)) {
							
							PrintWriter.append("<td>"+modDetails.getSP_TotalCost()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSP_TotalCost()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
		
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Stay Period - Total Booking Value ($)</td>");
					PrintWriter.append("<td>"+totalCostforReservation+"</td>");
					
					try {	
						
						if(modDetails.getSP_TotalBookingValue().contains(totalCostforReservation)) {
							
							PrintWriter.append("<td>"+modDetails.getSP_TotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getSP_TotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					PrintWriter.append("<tr><td class='fontiiii'>Modification - Change / Add Rooms</td><tr>"); 
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Hotel Name</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
					
					try {	
						
						if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(modDetails.getCAR_HotelName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+modDetails.getCAR_HotelName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getCAR_HotelName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					String modCARCheckInD = modDetails.getCAR_checkInDate();
					String modCARCheckOutD = modDetails.getCAR_checkOutDate();					
					DateFormat dateFormatCAR = new SimpleDateFormat("MMM-dd-yy");
					Date inputDateCInCAR = dateFormatCAR.parse(modCARCheckInD);
					Date inputDateCOutCAR = dateFormatCAR.parse(modCARCheckOutD);					
					SimpleDateFormat sdfCAR = new SimpleDateFormat("dd-MMM-yyyy");
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Check In Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
					
					try {			
						if(searchInfo.getDateFrom().equalsIgnoreCase(sdfCAR.format(inputDateCInCAR))) {
							
							PrintWriter.append("<td>"+modDetails.getCAR_checkInDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getCAR_checkInDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Check Out Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
					
					try {			
						if(searchInfo.getDateTo().equalsIgnoreCase(sdfCAR.format(inputDateCOutCAR))) {
							
							PrintWriter.append("<td>"+modDetails.getCAR_checkOutDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getCAR_checkOutDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Hotel No of Nights</td>");
					PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
					
					try {			
						if(searchInfo.getNights().equals(modDetails.getCAR_Nights())) {
							
							PrintWriter.append("<td>"+modDetails.getCAR_Nights()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getCAR_Nights()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Hotel No of Rooms</td>");
					PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
					
					try {			
						if(searchInfo.getRooms().equals(modDetails.getCAR_Rooms())) {
							
							PrintWriter.append("<td>"+modDetails.getCAR_Rooms()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getCAR_Rooms()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Hotel Adults count</td>");
					PrintWriter.append("<td>"+aduCount+"</td>");
					
					try {			
						if(aduCount.equals(modDetails.getCAR_Adults())) {
							
							PrintWriter.append("<td>"+modDetails.getCAR_Adults()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getCAR_Adults()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Hotel Children count</td>");
					PrintWriter.append("<td>"+childCount+"</td>");
					
					try {			
						if(childCount.equals(modDetails.getCAR_Child())) {
							
							PrintWriter.append("<td>"+modDetails.getCAR_Child()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getCAR_Child()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					
					
					for (int i = 0; i < excelRoomCount; i++) {

						
						PrintWriter.append("<tr><td class='fontyG'>Room : "+(i+1)+"</td><tr>"); 
					
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Room Type - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+roomList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getCAR_roomTypeList().size(); j++) {
								
								if(roomList.get(i).replaceAll(" ", "").equalsIgnoreCase(modDetails.getCAR_roomTypeList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getCAR_roomTypeList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
						////////
						
						String OccuPancyRate = hotelDetails.getConfirmationTD_RoomBedType().get(i) + "(" + hotelDetails.getConfirmationTD_RoomRatePlan().get(i);
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Board Type - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+OccuPancyRate+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getCAR_roomTypeList().size(); j++) {
								
								if(modDetails.getCAR_OccupancyTypeList().get(j).replaceAll(" ", "").contains(OccuPancyRate.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getCAR_OccupancyTypeList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
					
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Adults count - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+adultList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getCAR_roomTypeList().size(); j++) {
								
								if(adultList.get(i).replaceAll(" ", "").equals(modDetails.getCAR_AdultsList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getCAR_AdultsList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Childs count - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+childList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getCAR_roomTypeList().size(); j++) {
								
								if(childList.get(i).replaceAll(" ", "").equals(modDetails.getCAR_childrenList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getCAR_childrenList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						String fullNameCustomer = hotelDetails.getCustomerFirstName().get(i) + hotelDetails.getCustomerLastName().get(i) ;
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Guest Name - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+fullNameCustomer+"</td>");
						
						try {
							for (int j = 0; j < modDetails.getCAR_roomTypeList().size(); j++) {
																					
								if(modDetails.getCAR_GuestName().get(j).replaceAll(" ", "").equalsIgnoreCase(fullNameCustomer.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getCAR_GuestName().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
						///
						
						List<String> resultsRoomDailyRates = new ArrayList<String>();
						List<String> modifyDailyRates = new ArrayList<String>();
						
						for (int j = 0; j < Integer.parseInt(searchInfo.getNights()); j++) {
							
							String rateDay = modDetails.getCAR_DailyRates().get(i).get(j);
							modifyDailyRates.add(rateDay);
							String rateDay2 = dailyRatesResultsPage.get(i).get(j).split("\\.")[0];
							resultsRoomDailyRates.add(rateDay2);			
						}
						
						String match1 = "";
						String match2 = "";
						String dailyRateRoomCancelCalculatd = hotelDetails.getDailyRatesCancelList().get(i);
					
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Daily Rates - Room "+(i+1)+"</td>");
						
						for (int k = 0; k < Integer.parseInt(searchInfo.getNights()); k++) {
							
							match1 = match1 + ""+dailyRateRoomCancelCalculatd+"" + " ";
							match2 = match2 + ""+modifyDailyRates.get(k)+"" + " ";
							
						}
					
						PrintWriter.append("<td>"+match1+"</td>");
											
						try {	
							
							if(match1.equalsIgnoreCase(match2)) {
								
								PrintWriter.append("<td>"+match2+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+match2+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Room Total Value - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+calcultdTotalRoomRates.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getCAR_roomTypeList().size(); j++) {
								
								if(calcultdTotalRoomRates.get(i).replaceAll(" ", "").equalsIgnoreCase(modDetails.getCAR_Total().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getCAR_Total().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					
					}
					
					////
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Total Cost</td>");
					PrintWriter.append("<td>"+calcultdTotalCostHotel+"</td>");
					
					try {	
						
						if(modDetails.getCAR_TotalCost().contains(calcultdTotalCostHotel)) {
							
							PrintWriter.append("<td>"+modDetails.getCAR_TotalCost()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getCAR_TotalCost()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
		
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Change/Add Rooms - Total Booking Value ($)</td>");
					PrintWriter.append("<td>"+totalCostforReservation+"</td>");
					
					try {	
						
						if(modDetails.getCAR_TotalBookingValue().contains(totalCostforReservation)) {
							
							PrintWriter.append("<td>"+modDetails.getCAR_TotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getCAR_TotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////////
					
					PrintWriter.append("<tr><td class='fontiiii'>Modification - Remove Rooms</td><tr>"); 
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Hotel Name</td>");
					PrintWriter.append("<td>"+searchInfo.getExcelHotelName()+"</td>");
					
					try {	
						
						if(searchInfo.getExcelHotelName().replaceAll(" ", "").equalsIgnoreCase(modDetails.getRR_HotelName().replaceAll(" ", ""))) {
							
							PrintWriter.append("<td>"+modDetails.getRR_HotelName()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getRR_HotelName()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					String modRRCheckInD = modDetails.getRR_checkInDate();
					String modRRCheckOutD = modDetails.getRR_checkOutDate();					
					DateFormat dateFormatRR = new SimpleDateFormat("MMM-dd-yy");
					Date inputDateCInRR = dateFormatRR.parse(modRRCheckInD);
					Date inputDateCOutRR = dateFormatRR.parse(modRRCheckOutD);					
					SimpleDateFormat sdfRR = new SimpleDateFormat("dd-MMM-yyyy");
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Check In Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateFrom()+"</td>");
					
					try {			
						if(searchInfo.getDateFrom().equalsIgnoreCase(sdfRR.format(inputDateCInRR))) {
							
							PrintWriter.append("<td>"+modDetails.getRR_checkInDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getRR_checkInDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Check Out Date</td>");
					PrintWriter.append("<td>"+searchInfo.getDateTo()+"</td>");
					
					try {			
						if(searchInfo.getDateTo().equalsIgnoreCase(sdfRR.format(inputDateCOutRR))) {
							
							PrintWriter.append("<td>"+modDetails.getRR_checkOutDate()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getRR_checkOutDate()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Hotel No of Nights</td>");
					PrintWriter.append("<td>"+searchInfo.getNights()+"</td>");
					
					try {			
						if(searchInfo.getNights().equals(modDetails.getRR_Nights())) {
							
							PrintWriter.append("<td>"+modDetails.getRR_Nights()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getRR_Nights()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Hotel No of Rooms</td>");
					PrintWriter.append("<td>"+searchInfo.getRooms()+"</td>");
					
					try {			
						if(searchInfo.getRooms().equals(modDetails.getRR_Rooms())) {
							
							PrintWriter.append("<td>"+modDetails.getRR_Rooms()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getRR_Rooms()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Hotel Adults count</td>");
					PrintWriter.append("<td>"+aduCount+"</td>");
					
					try {			
						if(aduCount.equals(modDetails.getRR_Adults())) {
							
							PrintWriter.append("<td>"+modDetails.getRR_Adults()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getRR_Adults()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Hotel Children count</td>");
					PrintWriter.append("<td>"+childCount+"</td>");
					
					try {			
						if(childCount.equals(modDetails.getRR_Child())) {
							
							PrintWriter.append("<td>"+modDetails.getRR_Child()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getRR_Child()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					////
					
					
					
					for (int i = 0; i < excelRoomCount; i++) {

						
						PrintWriter.append("<tr><td class='fontyG'>Room : "+(i+1)+"</td><tr>"); 
					
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Room Type - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+roomList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getRR_roomTypeList().size(); j++) {
								
								if(roomList.get(i).replaceAll(" ", "").equalsIgnoreCase(modDetails.getRR_roomTypeList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getRR_roomTypeList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
					
						////////
						
						String OccuPancyRate = hotelDetails.getConfirmationTD_RoomBedType().get(i) + "(" + hotelDetails.getConfirmationTD_RoomRatePlan().get(i);
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Board Type - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+OccuPancyRate+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getRR_roomTypeList().size(); j++) {
								
								if(modDetails.getRR_OccupancyTypeList().get(j).replaceAll(" ", "").contains(OccuPancyRate.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getRR_OccupancyTypeList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
					
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Adults count - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+adultList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getRR_roomTypeList().size(); j++) {
								
								if(adultList.get(i).replaceAll(" ", "").equals(modDetails.getRR_AdultsList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getRR_AdultsList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Childs count - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+childList.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getRR_roomTypeList().size(); j++) {
								
								if(childList.get(i).replaceAll(" ", "").equals(modDetails.getRR_childrenList().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getRR_childrenList().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						String fullNameCustomer = hotelDetails.getCustomerFirstName().get(i) + hotelDetails.getCustomerLastName().get(i) ;
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Guest Name - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+fullNameCustomer+"</td>");
						
						try {
							for (int j = 0; j < modDetails.getRR_roomTypeList().size(); j++) {
																					
								if(modDetails.getRR_GuestName().get(j).replaceAll(" ", "").equalsIgnoreCase(fullNameCustomer.replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getRR_GuestName().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
						} catch (Exception e) {
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}	
						
						///
						
						List<String> resultsRoomDailyRates = new ArrayList<String>();
						List<String> modifyDailyRates = new ArrayList<String>();
						
						for (int j = 0; j < Integer.parseInt(searchInfo.getNights()); j++) {
							
							String rateDay = modDetails.getRR_DailyRates().get(i).get(j);
							modifyDailyRates.add(rateDay);
							String rateDay2 = dailyRatesResultsPage.get(i).get(j).split("\\.")[0];
							resultsRoomDailyRates.add(rateDay2);			
						}
						
						String match1 = "";
						String match2 = "";
						String dailyRateRoomCancelCalculatd = hotelDetails.getDailyRatesCancelList().get(i);
					
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Daily Rates - Room "+(i+1)+"</td>");
						
						for (int k = 0; k < Integer.parseInt(searchInfo.getNights()); k++) {
							
							match1 = match1 + ""+dailyRateRoomCancelCalculatd+"" + " ";
							match2 = match2 + ""+modifyDailyRates.get(k)+"" + " ";
							
						}
					
						PrintWriter.append("<td>"+match1+"</td>");
											
						try {	
							
							if(match1.equalsIgnoreCase(match2)) {
								
								PrintWriter.append("<td>"+match2+"</td>");
								PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
												
							} else {
								PrintWriter.append("<td>"+match2+"</td>");
								PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
						////
						
						testCaseModify ++;
						PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Room Total Value - Room "+(i+1)+"</td>");
						PrintWriter.append("<td>"+calcultdTotalRoomRates.get(i)+"</td>");
						
						try {	
							
							for (int j = 0; j < modDetails.getRR_roomTypeList().size(); j++) {
								
								if(calcultdTotalRoomRates.get(i).replaceAll(" ", "").equalsIgnoreCase(modDetails.getRR_Total().get(j).replaceAll(" ", ""))){
									
									PrintWriter.append("<td>"+modDetails.getRR_Total().get(j)+"</td>");
									PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
									
									break;
								}					
							}
								
						} catch (Exception e) {				
							PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						}
						
					
					}
					
					////
					
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Total Cost</td>");
					PrintWriter.append("<td>"+calcultdTotalCostHotel+"</td>");
					
					try {	
						
						if(modDetails.getRR_TotalCost().contains(calcultdTotalCostHotel)) {
							
							PrintWriter.append("<td>"+modDetails.getRR_TotalCost()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getRR_TotalCost()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
		
					testCaseModify ++;
					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification - Remove Rooms - Total Booking Value ($)</td>");
					PrintWriter.append("<td>"+totalCostforReservation+"</td>");
					
					try {	
						
						if(modDetails.getRR_TotalBookingValue().contains(totalCostforReservation)) {
							
							PrintWriter.append("<td>"+modDetails.getRR_TotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Passed'>PASS</td><tr>");		
											
						} else {
							PrintWriter.append("<td>"+modDetails.getRR_TotalBookingValue()+"</td>");
							PrintWriter.append("<td class='Failed'>FAIL</td><tr>");						
						}
							
					} catch (Exception e) {				
						PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					}
					
					
					
					
					
					PrintWriter.append("</table>");	
					
				}else {

					PrintWriter.append("<tr><td>"+testCaseModify+"</td> <td>Modification Details</td>");
					PrintWriter.append("<td>Modification Details should be available</td>");
					PrintWriter.append("<td>Not Available</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
						
					PrintWriter.append("</table>");	
					
				}
				
				
		
				PrintWriter.append("<br><br>");
				PrintWriter.append("<p class='fontStyles'>Response Times</p>");
				
				long timeDiff1 = hotelDetails.getTime_resultsAvailble() - hotelDetails.getTime_searchButtClick();
				long timeDiff2 = hotelDetails.getTime_addedToCart() - hotelDetails.getTime_addtoCartButtClick();
				long timeDiff3 = hotelDetails.getTime_ReserVationNoAvailable() - hotelDetails.getTime_confirmButt();
				long timeDiff4 = hotelDetails.getTime_ReserVationNoAvailable() - hotelDetails.getTime_searchButtClick();
				
				PrintWriter.append("<br><br><table><tr> <th>Description</th> <th>Time</th> </tr>");
				PrintWriter.append("<tr> <td>Initial Results Availability time - </td> <td>"+timeDiff1+"s</td> <tr>");
				PrintWriter.append("<tr> <td>Add to cart time - </td> <td>"+timeDiff2+"s</td> <tr>");
				PrintWriter.append("<tr> <td>Reservation No available time - </td> <td>"+timeDiff3+"s</td> <tr>");
				PrintWriter.append("<tr> <td>Time for full search - </td> <td>"+timeDiff4+"s</td> <tr>");
				PrintWriter.append("</table>");
				
			
			}
			
			PrintWriter.append("</body></html>");
			
			BufferedWriter bwr = new BufferedWriter(new FileWriter(new File("Report/ReservationReport_"+searchInfo.getScenarioId()+".html")));
			bwr.write(PrintWriter.toString());
			bwr.flush();
			bwr.close();
			
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
}
