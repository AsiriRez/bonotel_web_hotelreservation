package com.model;

public class Promotion {
	
	private String scenarioId;
	private String promoType;
	private String discount_Type;
	private String discount_value;
	private String discount_nights;
	
	public String getScenarioId() {
		return scenarioId;
	}
	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}
	public String getPromoType() {
		return promoType;
	}
	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}
	public String getDiscount_Type() {
		return discount_Type;
	}
	public void setDiscount_Type(String discount_Type) {
		this.discount_Type = discount_Type;
	}
	public String getDiscount_value() {
		return discount_value;
	}
	public void setDiscount_value(String discount_value) {
		this.discount_value = discount_value;
	}
	public String getDiscount_nights() {
		return discount_nights;
	}
	public void setDiscount_nights(String discount_nights) {
		this.discount_nights = discount_nights;
	}
	
	

}
