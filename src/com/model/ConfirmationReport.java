package com.model;

import java.util.ArrayList;

public class ConfirmationReport {
	
	private boolean isConfirmationReportLoaded  = true;
	private boolean isCustomerMailLoaded  = true;
	private boolean isHotelMailLoaded  = true;
	private boolean isPortalOperatorLoaded  = true;
	private String cus_BonoName;
	private String cus_BonoAddress;
	private String cus_TpnFax;
	
	private String cus_bookingDate;
	private String cus_bookingNo;
	private String cus_ToOrderNo;
	private String cus_ToName;
	private String cus_ToEmail;
	private String cus_HotelName;
	private String cus_HotelAddress;
	private String cus_HotelCity;
	private ArrayList<String> customerNames;
	private String cus_CheckInDate;
	private String cus_CheckOutDate;
	private String cus_Nights;
	private String cus_Rooms;
	private String cus_Adults;
	private String cus_Children;
	private String cus_RoomTypes;
	private String cus_OccupancyRatePlan;
	private String cus_InternalNotes;
	private String cus_Hotelnotes;
	private String cus_CustomerNotes;
	private String cus_DailyRatesWithDates;
	private String cus_ModifyFee;
	private String cus_TotalBookingCharge;
	
	private String hotel_BonoName;
	private String hotel_BonoAddress;
	private String hotel_TpnFax;
	private String hotel_HotelName;
	private String hotel_BookingDate;
	private String hotel_city;
	private String hotel_bonoNameFrom;
	private ArrayList<String> hotel_CustomerName;
	private ArrayList<String> hotel_BookingNo;
	private ArrayList<String> hotel_RoomType;
	private ArrayList<String> hotel_OccupancyRateType;
	private ArrayList<String> hotel_ArrivalDate;
	private ArrayList<String> hotel_Nights;
	private ArrayList<String> hotel_Adults;
	private ArrayList<String> hotel_Rooms;
	private ArrayList<String> hotel_Children;
	private ArrayList<String> hotel_DailyRates;
	private ArrayList<String> hotel_TotalRoomCost;
	private ArrayList<String> hotel_ModifyFee;
	
	
	private String PO_BonoName;
	private String PO_BonoAddress;
	private String PO_TpnFax;
	private String PO_bookingDate;
	private String PO_ToOrderNo;
	private String PO_BookingNo;
	private String PO_ToName;
	private String PO_ToEmail;
	private String PO_HotelName;
	private String PO_HotelAddress;
	private String PO_HotelCity;
	private ArrayList<String> PO_customerNames;
	private String PO_CheckInDate;
	private String PO_CheckOutDate;
	private String PO_Nights;
	private String PO_Rooms;
	private String PO_Adults;
	private String PO_Children;
	private String PO_RoomTypes;
	private String PO_OccupancyRatePlan;
	private String PO_InternalNotes;
	private String PO_Hotelnotes;
	private String PO_CustomerNotes;
	private String PO_DailyRatesWithDates;
	private String PO_ModifyFee;
	private String PO_TotalBookingCharge;
	
	
	
	public String getCus_ToEmail() {
		return cus_ToEmail;
	}
	public void setCus_ToEmail(String cus_ToEmail) {
		this.cus_ToEmail = cus_ToEmail;
	}
	public String getPO_ToEmail() {
		return PO_ToEmail;
	}
	public void setPO_ToEmail(String pO_ToEmail) {
		PO_ToEmail = pO_ToEmail;
	}
	public String getHotel_BonoName() {
		return hotel_BonoName;
	}
	public void setHotel_BonoName(String hotel_BonoName) {
		this.hotel_BonoName = hotel_BonoName;
	}
	public String getHotel_BonoAddress() {
		return hotel_BonoAddress;
	}
	public void setHotel_BonoAddress(String hotel_BonoAddress) {
		this.hotel_BonoAddress = hotel_BonoAddress;
	}
	public String getHotel_TpnFax() {
		return hotel_TpnFax;
	}
	public void setHotel_TpnFax(String hotel_TpnFax) {
		this.hotel_TpnFax = hotel_TpnFax;
	}
	public String getHotel_HotelName() {
		return hotel_HotelName;
	}
	public void setHotel_HotelName(String hotel_HotelName) {
		this.hotel_HotelName = hotel_HotelName;
	}
	public String getHotel_BookingDate() {
		return hotel_BookingDate;
	}
	public void setHotel_BookingDate(String hotel_BookingDate) {
		this.hotel_BookingDate = hotel_BookingDate;
	}
	public String getHotel_city() {
		return hotel_city;
	}
	public void setHotel_city(String hotel_city) {
		this.hotel_city = hotel_city;
	}
	public String getHotel_bonoNameFrom() {
		return hotel_bonoNameFrom;
	}
	public void setHotel_bonoNameFrom(String hotel_bonoNameFrom) {
		this.hotel_bonoNameFrom = hotel_bonoNameFrom;
	}
	public ArrayList<String> getHotel_CustomerName() {
		return hotel_CustomerName;
	}
	public void setHotel_CustomerName(ArrayList<String> hotel_CustomerName) {
		this.hotel_CustomerName = hotel_CustomerName;
	}
	public ArrayList<String> getHotel_BookingNo() {
		return hotel_BookingNo;
	}
	public void setHotel_BookingNo(ArrayList<String> hotel_BookingNo) {
		this.hotel_BookingNo = hotel_BookingNo;
	}
	public ArrayList<String> getHotel_RoomType() {
		return hotel_RoomType;
	}
	public void setHotel_RoomType(ArrayList<String> hotel_RoomType) {
		this.hotel_RoomType = hotel_RoomType;
	}
	public ArrayList<String> getHotel_OccupancyRateType() {
		return hotel_OccupancyRateType;
	}
	public void setHotel_OccupancyRateType(ArrayList<String> hotel_OccupancyRateType) {
		this.hotel_OccupancyRateType = hotel_OccupancyRateType;
	}
	public ArrayList<String> getHotel_ArrivalDate() {
		return hotel_ArrivalDate;
	}
	public void setHotel_ArrivalDate(ArrayList<String> hotel_ArrivalDate) {
		this.hotel_ArrivalDate = hotel_ArrivalDate;
	}
	public ArrayList<String> getHotel_Nights() {
		return hotel_Nights;
	}
	public void setHotel_Nights(ArrayList<String> hotel_Nights) {
		this.hotel_Nights = hotel_Nights;
	}
	public ArrayList<String> getHotel_Adults() {
		return hotel_Adults;
	}
	public void setHotel_Adults(ArrayList<String> hotel_Adults) {
		this.hotel_Adults = hotel_Adults;
	}
	public ArrayList<String> getHotel_Rooms() {
		return hotel_Rooms;
	}
	public void setHotel_Rooms(ArrayList<String> hotel_Rooms) {
		this.hotel_Rooms = hotel_Rooms;
	}
	public ArrayList<String> getHotel_Children() {
		return hotel_Children;
	}
	public void setHotel_Children(ArrayList<String> hotel_Children) {
		this.hotel_Children = hotel_Children;
	}
	public ArrayList<String> getHotel_DailyRates() {
		return hotel_DailyRates;
	}
	public void setHotel_DailyRates(ArrayList<String> hotel_DailyRates) {
		this.hotel_DailyRates = hotel_DailyRates;
	}
	public ArrayList<String> getHotel_TotalRoomCost() {
		return hotel_TotalRoomCost;
	}
	public void setHotel_TotalRoomCost(ArrayList<String> hotel_TotalRoomCost) {
		this.hotel_TotalRoomCost = hotel_TotalRoomCost;
	}
	public ArrayList<String> getHotel_ModifyFee() {
		return hotel_ModifyFee;
	}
	public void setHotel_ModifyFee(ArrayList<String> hotel_ModifyFee) {
		this.hotel_ModifyFee = hotel_ModifyFee;
	}
	public String getCus_bookingNo() {
		return cus_bookingNo;
	}
	public void setCus_bookingNo(String cus_bookingNo) {
		this.cus_bookingNo = cus_bookingNo;
	}
	public boolean isCustomerMailLoaded() {
		return isCustomerMailLoaded;
	}
	public void setCustomerMailLoaded(boolean isCustomerMailLoaded) {
		this.isCustomerMailLoaded = isCustomerMailLoaded;
	}
	public boolean isHotelMailLoaded() {
		return isHotelMailLoaded;
	}
	public void setHotelMailLoaded(boolean isHotelMailLoaded) {
		this.isHotelMailLoaded = isHotelMailLoaded;
	}
	public boolean isPortalOperatorLoaded() {
		return isPortalOperatorLoaded;
	}
	public void setPortalOperatorLoaded(boolean isPortalOperatorLoaded) {
		this.isPortalOperatorLoaded = isPortalOperatorLoaded;
	}
	public boolean isConfirmationReportLoaded() {
		return isConfirmationReportLoaded;
	}
	public void setConfirmationReportLoaded(boolean isConfirmationReportLoaded) {
		this.isConfirmationReportLoaded = isConfirmationReportLoaded;
	}
	public String getCus_BonoName() {
		return cus_BonoName;
	}
	public void setCus_BonoName(String cus_BonoName) {
		this.cus_BonoName = cus_BonoName;
	}
	public String getCus_BonoAddress() {
		return cus_BonoAddress;
	}
	public void setCus_BonoAddress(String cus_BonoAddress) {
		this.cus_BonoAddress = cus_BonoAddress;
	}
	public String getCus_TpnFax() {
		return cus_TpnFax;
	}
	public void setCus_TpnFax(String cus_TpnFax) {
		this.cus_TpnFax = cus_TpnFax;
	}
	public String getCus_bookingDate() {
		return cus_bookingDate;
	}
	public void setCus_bookingDate(String cus_bookingDate) {
		this.cus_bookingDate = cus_bookingDate;
	}
	public String getCus_ToOrderNo() {
		return cus_ToOrderNo;
	}
	public void setCus_ToOrderNo(String cus_ToOrderNo) {
		this.cus_ToOrderNo = cus_ToOrderNo;
	}
	
	public String getCus_ToName() {
		return cus_ToName;
	}
	public void setCus_ToName(String cus_ToName) {
		this.cus_ToName = cus_ToName;
	}
	public String getCus_HotelName() {
		return cus_HotelName;
	}
	public void setCus_HotelName(String cus_HotelName) {
		this.cus_HotelName = cus_HotelName;
	}
	public String getCus_HotelAddress() {
		return cus_HotelAddress;
	}
	public void setCus_HotelAddress(String cus_HotelAddress) {
		this.cus_HotelAddress = cus_HotelAddress;
	}
	public String getCus_HotelCity() {
		return cus_HotelCity;
	}
	public void setCus_HotelCity(String cus_HotelCity) {
		this.cus_HotelCity = cus_HotelCity;
	}
	public ArrayList<String> getCustomerNames() {
		return customerNames;
	}
	public void setCustomerNames(ArrayList<String> customerNames) {
		this.customerNames = customerNames;
	}
	public String getCus_CheckInDate() {
		return cus_CheckInDate;
	}
	public void setCus_CheckInDate(String cus_CheckInDate) {
		this.cus_CheckInDate = cus_CheckInDate;
	}
	public String getCus_CheckOutDate() {
		return cus_CheckOutDate;
	}
	public void setCus_CheckOutDate(String cus_CheckOutDate) {
		this.cus_CheckOutDate = cus_CheckOutDate;
	}
	public String getCus_Nights() {
		return cus_Nights;
	}
	public void setCus_Nights(String cus_Nights) {
		this.cus_Nights = cus_Nights;
	}
	public String getCus_Rooms() {
		return cus_Rooms;
	}
	public void setCus_Rooms(String cus_Rooms) {
		this.cus_Rooms = cus_Rooms;
	}
	public String getCus_Adults() {
		return cus_Adults;
	}
	public void setCus_Adults(String cus_Adults) {
		this.cus_Adults = cus_Adults;
	}
	public String getCus_Children() {
		return cus_Children;
	}
	public void setCus_Children(String cus_Children) {
		this.cus_Children = cus_Children;
	}
	public String getCus_RoomTypes() {
		return cus_RoomTypes;
	}
	public void setCus_RoomTypes(String cus_RoomTypes) {
		this.cus_RoomTypes = cus_RoomTypes;
	}
	public String getCus_OccupancyRatePlan() {
		return cus_OccupancyRatePlan;
	}
	public void setCus_OccupancyRatePlan(String cus_OccupancyRatePlan) {
		this.cus_OccupancyRatePlan = cus_OccupancyRatePlan;
	}
	public String getCus_InternalNotes() {
		return cus_InternalNotes;
	}
	public void setCus_InternalNotes(String cus_InternalNotes) {
		this.cus_InternalNotes = cus_InternalNotes;
	}
	public String getCus_Hotelnotes() {
		return cus_Hotelnotes;
	}
	public void setCus_Hotelnotes(String cus_Hotelnotes) {
		this.cus_Hotelnotes = cus_Hotelnotes;
	}
	public String getCus_CustomerNotes() {
		return cus_CustomerNotes;
	}
	public void setCus_CustomerNotes(String cus_CustomerNames) {
		this.cus_CustomerNotes = cus_CustomerNames;
	}
	public String getCus_DailyRatesWithDates() {
		return cus_DailyRatesWithDates;
	}
	public void setCus_DailyRatesWithDates(String cus_DailyRatesWithDates) {
		this.cus_DailyRatesWithDates = cus_DailyRatesWithDates;
	}
	public String getCus_ModifyFee() {
		return cus_ModifyFee;
	}
	public void setCus_ModifyFee(String cus_ModifyFee) {
		this.cus_ModifyFee = cus_ModifyFee;
	}
	public String getCus_TotalBookingCharge() {
		return cus_TotalBookingCharge;
	}
	public void setCus_TotalBookingCharge(String cus_TotalBookingCharge) {
		this.cus_TotalBookingCharge = cus_TotalBookingCharge;
	}
	public String getPO_BonoName() {
		return PO_BonoName;
	}
	public void setPO_BonoName(String pO_BonoName) {
		PO_BonoName = pO_BonoName;
	}
	public String getPO_BonoAddress() {
		return PO_BonoAddress;
	}
	public void setPO_BonoAddress(String pO_BonoAddress) {
		PO_BonoAddress = pO_BonoAddress;
	}
	public String getPO_TpnFax() {
		return PO_TpnFax;
	}
	public void setPO_TpnFax(String pO_TpnFax) {
		PO_TpnFax = pO_TpnFax;
	}
	public String getPO_bookingDate() {
		return PO_bookingDate;
	}
	public void setPO_bookingDate(String pO_bookingDate) {
		PO_bookingDate = pO_bookingDate;
	}
	public String getPO_ToOrderNo() {
		return PO_ToOrderNo;
	}
	public void setPO_ToOrderNo(String pO_ToOrderNo) {
		PO_ToOrderNo = pO_ToOrderNo;
	}
	
	
	public String getPO_BookingNo() {
		return PO_BookingNo;
	}
	public void setPO_BookingNo(String pO_BookingNo) {
		PO_BookingNo = pO_BookingNo;
	}
	public String getPO_ToName() {
		return PO_ToName;
	}
	public void setPO_ToName(String pO_ToName) {
		PO_ToName = pO_ToName;
	}
	public String getPO_HotelName() {
		return PO_HotelName;
	}
	public void setPO_HotelName(String pO_HotelName) {
		PO_HotelName = pO_HotelName;
	}
	public String getPO_HotelAddress() {
		return PO_HotelAddress;
	}
	public void setPO_HotelAddress(String pO_HotelAddress) {
		PO_HotelAddress = pO_HotelAddress;
	}
	public String getPO_HotelCity() {
		return PO_HotelCity;
	}
	public void setPO_HotelCity(String pO_HotelCity) {
		PO_HotelCity = pO_HotelCity;
	}
	public ArrayList<String> getPO_customerNames() {
		return PO_customerNames;
	}
	public void setPO_customerNames(ArrayList<String> pO_customerNames) {
		PO_customerNames = pO_customerNames;
	}
	public String getPO_CheckInDate() {
		return PO_CheckInDate;
	}
	public void setPO_CheckInDate(String pO_CheckInDate) {
		PO_CheckInDate = pO_CheckInDate;
	}
	public String getPO_CheckOutDate() {
		return PO_CheckOutDate;
	}
	public void setPO_CheckOutDate(String pO_CheckOutDate) {
		PO_CheckOutDate = pO_CheckOutDate;
	}
	public String getPO_Nights() {
		return PO_Nights;
	}
	public void setPO_Nights(String pO_Nights) {
		PO_Nights = pO_Nights;
	}
	public String getPO_Rooms() {
		return PO_Rooms;
	}
	public void setPO_Rooms(String pO_Rooms) {
		PO_Rooms = pO_Rooms;
	}
	public String getPO_Adults() {
		return PO_Adults;
	}
	public void setPO_Adults(String pO_Adults) {
		PO_Adults = pO_Adults;
	}
	public String getPO_Children() {
		return PO_Children;
	}
	public void setPO_Children(String pO_Children) {
		PO_Children = pO_Children;
	}
	public String getPO_RoomTypes() {
		return PO_RoomTypes;
	}
	public void setPO_RoomTypes(String pO_RoomTypes) {
		PO_RoomTypes = pO_RoomTypes;
	}
	public String getPO_OccupancyRatePlan() {
		return PO_OccupancyRatePlan;
	}
	public void setPO_OccupancyRatePlan(String pO_OccupancyRatePlan) {
		PO_OccupancyRatePlan = pO_OccupancyRatePlan;
	}
	public String getPO_InternalNotes() {
		return PO_InternalNotes;
	}
	public void setPO_InternalNotes(String pO_InternalNotes) {
		PO_InternalNotes = pO_InternalNotes;
	}
	public String getPO_Hotelnotes() {
		return PO_Hotelnotes;
	}
	public void setPO_Hotelnotes(String pO_Hotelnotes) {
		PO_Hotelnotes = pO_Hotelnotes;
	}
	
	public String getPO_CustomerNotes() {
		return PO_CustomerNotes;
	}
	public void setPO_CustomerNotes(String pO_CustomerNotes) {
		PO_CustomerNotes = pO_CustomerNotes;
	}
	public String getPO_DailyRatesWithDates() {
		return PO_DailyRatesWithDates;
	}
	public void setPO_DailyRatesWithDates(String pO_DailyRatesWithDates) {
		PO_DailyRatesWithDates = pO_DailyRatesWithDates;
	}
	public String getPO_ModifyFee() {
		return PO_ModifyFee;
	}
	public void setPO_ModifyFee(String pO_ModifyFee) {
		PO_ModifyFee = pO_ModifyFee;
	}
	public String getPO_TotalBookingCharge() {
		return PO_TotalBookingCharge;
	}
	public void setPO_TotalBookingCharge(String pO_TotalBookingCharge) {
		PO_TotalBookingCharge = pO_TotalBookingCharge;
	}
	
	
	

}
