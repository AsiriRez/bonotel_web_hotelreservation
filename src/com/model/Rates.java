package com.model;

public class Rates {
	
	private String scenarioId;
	private String hotelname;
	private String roomBedRate;
	private String netRate;
	private String salesTax;
	private String occupancyTax;
	private String energyTax;
	private String misTax;
	private String profitMarkup;
	
	
	public String getHotelname() {
		return hotelname;
	}
	public void setHotelname(String hotelname) {
		this.hotelname = hotelname;
	}
	public String getRoomBedRate() {
		return roomBedRate;
	}
	public void setRoomBedRate(String roomBedRate) {
		this.roomBedRate = roomBedRate;
	}
	public String getNetRate() {
		return netRate;
	}
	public void setNetRate(String netRate) {
		this.netRate = netRate;
	}
	public String getSalesTax() {
		return salesTax;
	}
	public void setSalesTax(String salesTax) {
		this.salesTax = salesTax;
	}
	public String getOccupancyTax() {
		return occupancyTax;
	}
	public void setOccupancyTax(String occupancyTax) {
		this.occupancyTax = occupancyTax;
	}
	public String getEnergyTax() {
		return energyTax;
	}
	public void setEnergyTax(String energyTax) {
		this.energyTax = energyTax;
	}
	public String getMisTax() {
		return misTax;
	}
	public void setMisTax(String misTax) {
		this.misTax = misTax;
	}
	public String getProfitMarkup() {
		return profitMarkup;
	}
	public void setProfitMarkup(String profitMarkup) {
		this.profitMarkup = profitMarkup;
	}
	public String getScenarioId() {
		return scenarioId;
	}
	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}
	
	

}
