package com.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.model.*;

public class LoadDetails {
	
	Map<Integer, String> searchInfoMap 		= null;
	Map<Integer, String> ratesMap 		= null;
	Map<Integer, String> TOMap 		= null;
	
	public LoadDetails(ArrayList<Map<Integer, String>> sheetlist){
		
		searchInfoMap      	= sheetlist.get(0);	
		TOMap               = sheetlist.get(1);	
		ratesMap      		= sheetlist.get(2);			
	}
	
	public TreeMap<String, SearchInfo> loadReservationInfo(Map<Integer, String> searchInfoDeMap){
		
		
		Iterator<Map.Entry<Integer, String>> it = searchInfoDeMap.entrySet().iterator();
		TreeMap<String, SearchInfo> hotelSearchInfoMap = new TreeMap<String, SearchInfo>();
		
		
		while(it.hasNext()){
			
			SearchInfo search = new SearchInfo();
			String[] values = it.next().getValue().split(",");
			
			search.setScenarioId(values[0]);
			search.setTourOperatorName(values[1]);
			search.setCountry(values[2]);
			search.setCity(values[3]);
			search.setStateCity(values[4]);
			search.setSelectHotel(values[5]);
			search.setDateFrom(values[6]);
			search.setDateTo(values[7]);
			search.setNights(values[8]);
			search.setRooms(values[9]);
			search.setAdults(values[10]);
			search.setChildren(values[11]);
			search.setChild_ages(values[12]);
			search.setBookingType(values[13]);
			search.setExcelHotelName(values[14]);
			search.setExcelHotelAddress(values[15]);
			search.setExcelRoomType(values[16]);
			search.setExcelOccupancyratePlan(values[17]);
			search.setBookingChannel(values[18]);
			search.setTO_type(values[19]);
			search.setHotelDescription(values[20]);
			search.setIsPromoApply(values[21]);
			
			hotelSearchInfoMap.put(values[0], search);
			
		}
		
		return hotelSearchInfoMap;
	}
	
	public TreeMap<String, SearchInfo> loadRateDetails(Map<Integer, String> ratesMapDe, TreeMap<String, SearchInfo> searchInfoDeMap){
		
		Iterator<Map.Entry<Integer, String>> it = ratesMapDe.entrySet().iterator();
		
		while(it.hasNext()){
			
			Rates rates = new Rates();
			String[] values = it.next().getValue().split(",");
			
			rates.setScenarioId(values[0]);
			rates.setHotelname(values[1]);
			rates.setRoomBedRate(values[2]);
			rates.setNetRate(values[3]);
			rates.setSalesTax(values[4]);
			rates.setOccupancyTax(values[5]);
			rates.setEnergyTax(values[6]);
			rates.setMisTax(values[7]);
			rates.setProfitMarkup(values[8]);
			
			try {				
				searchInfoDeMap.get(values[0]).addratesInfo(rates);
			} catch (Exception e) {
				e.printStackTrace();
			}
					
		}
		
		return searchInfoDeMap;
	}
	
	public TreeMap<String, SearchInfo> loadPromotionDetails(Map<Integer, String> promotionMap, TreeMap<String, SearchInfo> searchInfoDeMap){
		
		try {
			
			Iterator<Map.Entry<Integer, String>> it = promotionMap.entrySet().iterator();
			
			while(it.hasNext()){
				
				Promotion promotion = new Promotion();
				String[] values = it.next().getValue().split(",");
				
				promotion.setScenarioId(values[0]);
				promotion.setPromoType(values[1]);
				promotion.setDiscount_Type(values[2]);
				promotion.setDiscount_value(values[3]);
				promotion.setDiscount_nights(values[4]);
				
				try {
					searchInfoDeMap.get(values[0]).addPromotionInfo(promotion);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return searchInfoDeMap;	
	}
	
	
	public ArrayList<TO_Details> loadTODetails(){
		
		ArrayList<TO_Details> tOList = null;
		
		try {
			Iterator<Map.Entry<Integer, String>> it = TOMap.entrySet().iterator();
			tOList = new ArrayList<TO_Details>();
			
			while(it.hasNext()){
				
				TO_Details tO_Details = new TO_Details();
				String[] values = it.next().getValue().split(",");
				
				tO_Details.setTo_Name(values[0]);
				tO_Details.setTo_Address(values[1]);
				tO_Details.setTo_City(values[2]);
				tO_Details.setTo_Country(values[3]);
				tO_Details.setTo_Zip(values[4]);
				tO_Details.setTo_Email(values[5]);
				tO_Details.setTo_phone(values[6]);
				
				tOList.add(tO_Details);
				
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return tOList;
	}
	
	
	public static Map<String,TO_Details> tODetailsconvertIntoMap(ArrayList<TO_Details> toList){
		
		Map<String,TO_Details> TOListMap=new HashMap<String, TO_Details>();
		
		for(TO_Details to : toList){			
			TOListMap.put(to.getTo_Name(), to);
		}
		
		return TOListMap;		
	}
	
	
	
	
}
