package com.model;

import java.util.ArrayList;

public class RoomTypes {
	
	private String results_RoomType;
	private String results_Occupancy;
	private String results_RatePlan;	
	private ArrayList<String> results_DailyRates;
	private String results_AvalabilityDates;
	private String results_listTotal;
	private String results_TotalRate;
	
	public String getResults_RoomType() {
		return results_RoomType;
	}
	public void setResults_RoomType(String results_RoomType) {
		this.results_RoomType = results_RoomType;
	}
	public String getResults_Occupancy() {
		return results_Occupancy;
	}
	public void setResults_Occupancy(String results_Occupancy) {
		this.results_Occupancy = results_Occupancy;
	}
	public String getResults_RatePlan() {
		return results_RatePlan;
	}
	public void setResults_RatePlan(String results_RatePlan) {
		this.results_RatePlan = results_RatePlan;
	}
	public ArrayList<String> getResults_DailyRates() {
		return results_DailyRates;
	}
	public void setResults_DailyRates(ArrayList<String> results_DailyRates) {
		this.results_DailyRates = results_DailyRates;
	}
	public String getResults_AvalabilityDates() {
		return results_AvalabilityDates;
	}
	public void setResults_AvalabilityDates(String results_AvalabilityDates) {
		this.results_AvalabilityDates = results_AvalabilityDates;
	}
	public String getResults_listTotal() {
		return results_listTotal;
	}
	public void setResults_listTotal(String results_listTotal) {
		this.results_listTotal = results_listTotal;
	}
	public String getResults_TotalRate() {
		return results_TotalRate;
	}
	public void setResults_TotalRate(String results_TotalRate) {
		this.results_TotalRate = results_TotalRate;
	}
	
	

}
