package com.reader;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.*;


public class CancellationReportFlow {

	private CancellationDetails cancelDetails;
	private WebDriver driver = null;
	private HotelDetails hotelDetails = new HotelDetails();
	private SearchInfo sInfo = new SearchInfo();
	
	public CancellationReportFlow(HotelDetails hoteldetails, SearchInfo sInfo, WebDriver Driver){
		
		this.driver = Driver;
		this.hotelDetails = hoteldetails;	
		this.sInfo = sInfo;	
	}
	

	
	
	public CancellationDetails loadCancellationFlow(WebDriver webDriver) throws InterruptedException{
		
		WebDriverWait wait = new WebDriverWait(driver, 120);	
		cancelDetails = new CancellationDetails();
		driver.get(PG_Properties.getProperty("Baseurl")+ "/reservation/ReservationCancellationPage.do");
		Thread.sleep(1500);
		
		try {
				
			if (driver.findElements(By.id("reservationid")).size() != 0) {
				
				String reservationNo = hotelDetails.getConfirmation_ReservationNO();
				
				driver.findElement(By.id("reservationid")).clear();
				driver.findElement(By.id("reservationid")).sendKeys(reservationNo);
				driver.findElement(By.id("reservationid_lkup")).click();
				driver.switchTo().defaultContent();
				driver.switchTo().frame("lookup");
				Thread.sleep(1000);
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				Thread.sleep(1000);
				
				driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/a/img")).click();
				Thread.sleep(1000);
				
				////
				
				String hotelName = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]")).getText().split(": ")[1];
				cancelDetails.setHotelName(hotelName);
				
				String hotelAddress = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[2]")).getText();
				cancelDetails.setHotelAddress(hotelAddress);
				
				String hotelCity = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]")).getText().split(": ")[1];
				cancelDetails.setHotelCity(hotelCity);
				
				String arrivalDate = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[2]")).getText().split(": ")[1];
				cancelDetails.setArrivalDate(arrivalDate);
				
				String departureDate = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[2]")).getText().split(": ")[1];
				cancelDetails.setDepartureDate(departureDate);
				
				String adults = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td[2]")).getText().split(": ")[1];
				cancelDetails.setAdults(adults);
				
				String nights = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[4]")).getText().split(": ")[1];
				cancelDetails.setNights(nights);
				
				String rooms = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[4]")).getText().split(": ")[1];
				cancelDetails.setRooms(rooms);
				
				String children = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td[4]")).getText().split(": ")[1];
				cancelDetails.setChildren(children);
				
				String bookingDate = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td[4]")).getText().split(": ")[1];
				cancelDetails.setBookingDate(bookingDate);
				
				String TOName = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[7]/td[4]")).getText().split(": ")[1];
				cancelDetails.setTOName(TOName);
				
				String bookingNo = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td[2]")).getText().split(": ")[1];
				cancelDetails.setBookingNo(bookingNo);
				
				String bookingChannel = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[7]/td[2]")).getText().split(": ")[1];
				cancelDetails.setBookingChannel(bookingChannel);
				
				ArrayList<String> customerTitle = new ArrayList<String>();
				ArrayList<String> customerFirstName = new ArrayList<String>();
				ArrayList<String> customerLastName = new ArrayList<String>();
				int hotelRoomsCount = Integer.parseInt(sInfo.getRooms());
				
				for (int i = 1; i <= hotelRoomsCount; i++) {
					
					String cusTitle = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[2]/span")).getText().replaceAll(" ", "");
					customerTitle.add(cusTitle);	
					
					String cusLName = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[3]/span")).getText().replaceAll(" ", "");
					customerLastName.add(cusLName);
					
					String cusFName = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[4]/span")).getText().replaceAll(" ", "");
					customerFirstName.add(cusFName);
					
				}
				
				cancelDetails.setCusTitle(customerTitle);
				cancelDetails.setCusFirstName(customerFirstName);
				cancelDetails.setCusLastName(customerLastName);
				
				///
				
				ArrayList<String> roomTypeList = new ArrayList<String>();
				ArrayList<String> bedTypeList = new ArrayList<String>();
				ArrayList<String> adultsList = new ArrayList<String>();
				ArrayList<String> childrenList = new ArrayList<String>();
				ArrayList<ArrayList<String>> dailyRates = new ArrayList<ArrayList<String>>();
				ArrayList<String> totalValue = new ArrayList<String>();
				ArrayList<String> daysCount = new ArrayList<String>();
				
				for (int i = 1; i <= hotelRoomsCount; i++) {
					
					String roomType = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[1]")).getText().replaceAll(" ", "");
					roomTypeList.add(roomType);
					
					String bedType = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[2]")).getText().replaceAll(" ", "");
					bedTypeList.add(bedType);
					
					String adultsL = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[3]")).getText().replaceAll(" ", "");
					adultsList.add(adultsL);
					
					String childrenL = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[4]")).getText().replaceAll(" ", "");
					childrenList.add(childrenL);
					
					WebElement searchTableEle = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[5]/table"));				
					List<WebElement> daysCountSer = searchTableEle.findElements(By.tagName("td"));
					int sizeDays = (daysCountSer.size() / 2);
					daysCount.add(Integer.toString(sizeDays));
					
					ArrayList<String> dailyRatesLoop = new ArrayList<String>();
					WebElement searchratesEle = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[5]/table"));				
					List<WebElement> searchRate = searchratesEle.findElements(By.tagName("td"));
					
					for (int k = (sizeDays+1); k <= daysCountSer.size(); k++) {
						
						String oneDayRateSea = searchRate.get(k-1).getText().replaceAll(" ", "");
						dailyRatesLoop.add(oneDayRateSea);
						
					}
					dailyRates.add(dailyRatesLoop);
					
					String totalV = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[6]")).getText().replaceAll(" ", "");
					totalValue.add(totalV);
					
				}
				
				cancelDetails.setRoomTypeList(roomTypeList);
				cancelDetails.setBedTypeList(bedTypeList);
				cancelDetails.setAdultsList(adultsList);
				cancelDetails.setChildrenList(childrenList);
				cancelDetails.setDailyRates(dailyRates);
				cancelDetails.setTotalValue(totalValue);			
				cancelDetails.setDaysCount(daysCount);
				
				String subTotal = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(hotelRoomsCount+2)+"]/td[3]")).getText();
				cancelDetails.setSubTotalLast(subTotal);
				
				String totalPayable = driver.findElement(By.xpath(".//*[@id='ResCancelPageId']/table[2]/tbody/tr/td/table[1]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(hotelRoomsCount+3)+"]/td[3]")).getText();
				cancelDetails.setTotalPayable(totalPayable);
				
				
			}else{
				
				cancelDetails.setCancellationReportLoaded(false);
			}
			
		} catch (Exception e) {
			
			cancelDetails.setCancellationReportLoaded(false);
		}
		
	
		return cancelDetails;
		
	}
		
}
