package com.model;

import java.util.ArrayList;
import java.util.List;

public class HotelDetails {
	
	private int count_nights;
	private int count_rooms;
	private int count_adults;
	private int count_children;
	private String excelHotelName;
	private String reservationDate;
	private boolean isCancellationLinkAvailable = true;
	private boolean isRestrictionLinkAvailable = true;
	private boolean isHotelInfoLinkAvailable = true;
	private boolean isSpecialsLinkAvailable = true;
	private boolean isVoucherLoaded = true;
	private boolean isReservationReportLoaded = true;
	private long time_searchButtClick;
	private long time_resultsAvailble;
	private long time_addtoCartButtClick;
	private long time_addedToCart;
	private long time_confirmButt;
	private long time_ReserVationNoAvailable;
	
	private ArrayList<String> dailyRatesList;
	private ArrayList<String> dailyRatesCancelList;
	private ArrayList<String> dailyRatesCostList;
	private ArrayList<String> dailyNetRateList;
	private String rateAfterDiscount;
	
	private String newWindow_HotelName;
	private String newWindow_Address;
	private String isCheckOut;
	private String hoteldescription;
	private ArrayList<String> cancelPolicy;
	private ArrayList<String> paymentCancelPolicy;
	private ArrayList<String> confirmationCancelPolicy;
	public boolean isResultsAvailable = true;
	
	private String results_CheckInDate;
	private String results_CheckOutDate;
	private String results_Nights;
	private String results_Rooms;
	private String results_Adults;
	private String results_children;
	private String results_hotelAddress;
	private String results_hotelName;
	
	private String results_TotalRate;
	private String results_shoppingCartValue;
	
	private String paymentShopCart_hotelName;
	private String paymentShopCart_qty;
	private String paymentShopCart_dateFrom;
	private String paymentShopCart_dateTo;
	private String paymentShopCart_amount;
	private String paymentShopCart_CartTotal;
	
	private String paymentTravelDetails_checkIn;
	private String paymentTravelDetails_checkOut;
	private String paymentTravelDetails_Nights;
	private String paymentTravelDetails_Rooms;
	private String paymentTravelDetails_Adults;
	private String paymentTravelDetails_Children;
	private String paymentTravelDetails_HotelName;
	private String paymentTravelDetails_Address;
	private String paymentTravelDetails_city;
	private String paymentTravelDetails_state;
	private String paymentTravelDetails_country;
	
	private ArrayList<String> paymentTravelDetails_RoomAdults;
	private ArrayList<String> paymentTravelDetails_RoomChildren;
	private ArrayList<String> paymentTravelDetails_RoomType;
	private ArrayList<String> paymentTravelDetails_RoomOccupancy;
	private ArrayList<String> paymentTravelDetails_RoomRatePlan;
	private ArrayList<String> paymentTravelDetails_RoomDaysCount;
	private ArrayList<ArrayList<String>> paymentTravelDetails_RoomDailyRates;
	private ArrayList<String> paymentTravelDetails_Roomtotal;
	
	private String paymentTravelDetails_RoomBookingTotal;
	private String paymentsPage_CustomerNotes;
	
	private String paymentTravelDetails_RoomDetailsTitle;
	private String paymentTravelDetails_RoomDetailsLastName;
	private String paymentTravelDetails_RoomDetailsFirstName;
	
	private ArrayList<String> customerTitle;
	private ArrayList<String> customerLastName;
	private ArrayList<String> customerFirstName;
	private String paymentShoppingCartTotal;
	
	private String confirmationTOD_TOName;
	private String confirmationTOD_Address;
	private String confirmationTOD_City;
	private String confirmationTOD_Country;
	private String confirmationTOD_Zip;
	private String confirmationTOD_Email;
	private String confirmationTOD_phone;
	private String confirmationTS_HotelName;
	private String confirmationTS_qty;
	private String confirmationTS_checkInDate;
	private String confirmationTS_checkoutDate;
	private String confirmationTS_amount;
	private String confirmationTS_confirmedTotal;
	private String confirmationTS_Total;
	
	private String confirmationTD_checkInDate;
	private String confirmationTD_checkOutDate;
	private String confirmationTD_nights;
	private String confirmationTD_rooms;
	private String confirmationTD_adults;
	private String confirmationTD_children;
	private String confirmationTD_HotelName;
	private String confirmationTD_hotelAddress;
	private String confirmationTD_city;
	private String confirmationTD_state;
	private String confirmationTD_country;
	private String confirmationTD_BookingDate;
	private String confirmation_ReservationNO = "Not Available";
	
	private ArrayList<String> confirmationTD_FirstName;
	private ArrayList<String> confirmationTD_LastNsme;
	
	private ArrayList<String> confirmationTD_Adults;
	private ArrayList<String> confirmationTD_Children;
	private ArrayList<String> confirmationTD_RoomType;
	private ArrayList<String> confirmationTD_RoomBedType;
	private ArrayList<String> confirmationTD_RoomRatePlan;
	private ArrayList<String> confirmationTD_DaysCount;
	private ArrayList<ArrayList<String>> confirmationTD_DailyRates;
	private ArrayList<String> confirmationTD_total;
	private String confirmationTD_BookingTotal;
	
	private String voucher_bookingDate;
	private String voucher_bookingNo;
	private String voucher_ToOrderNo;
	private String voucher_ToName;
	private String voucher_ToEmail;
	private String voucher_HotelName;
	private String voucher_HotelAddress;
	private String voucher_HotelCity;
	private ArrayList<String> voucher_customerNames;
	private String voucher_CheckInDate;
	private String voucher_CheckOutDate;
	private String voucher_Nights;
	private String voucher_Rooms;
	private String voucher_Adults;
	private String voucher_Children;
	private String voucher_RoomTypes;
	private String voucher_OccupancyRatePlan;
	
	private ArrayList<RoomTypes> roomTypes = new ArrayList<RoomTypes>();

	public void addRoomTypes(RoomTypes rooms){
		roomTypes.add(rooms);
	}
	
	
	
	public boolean isReservationReportLoaded() {
		return isReservationReportLoaded;
	}



	public void setReservationReportLoaded(boolean isReservationReportLoaded) {
		this.isReservationReportLoaded = isReservationReportLoaded;
	}



	public ArrayList<String> getDailyRatesCostList() {
		return dailyRatesCostList;
	}
	
	public void setDailyRatesCostList(ArrayList<String> dailyRatesCostList) {
		this.dailyRatesCostList = dailyRatesCostList;
	}
	
	public ArrayList<String> getDailyRatesCancelList() {
		return dailyRatesCancelList;
	}
	
	public void setDailyRatesCancelList(ArrayList<String> dailyRatesCancelList) {
		this.dailyRatesCancelList = dailyRatesCancelList;
	}

	public ArrayList<String> getDailyNetRateList() {
		return dailyNetRateList;
	}

	public void setDailyNetRateList(ArrayList<String> dailyNetRateList) {
		this.dailyNetRateList = dailyNetRateList;
	}

	public String getRateAfterDiscount() {
		return rateAfterDiscount;
	}

	public void setRateAfterDiscount(String rateAfterDiscount) {
		this.rateAfterDiscount = rateAfterDiscount;
	}

	public ArrayList<String> getDailyRatesList() {
		return dailyRatesList;
	}
	public void setDailyRatesList(ArrayList<String> dailyRatesList) {
		this.dailyRatesList = dailyRatesList;
	}
	public String getPaymentsPage_CustomerNotes() {
		return paymentsPage_CustomerNotes;
	}
	public void setPaymentsPage_CustomerNotes(String paymentsPage_CustomerNotes) {
		this.paymentsPage_CustomerNotes = paymentsPage_CustomerNotes;
	}

	public String getConfirmationTOD_TOName() {
		return confirmationTOD_TOName;
	}
	
	public void setConfirmationTOD_TOName(String confirmationTOD_TOName) {
		this.confirmationTOD_TOName = confirmationTOD_TOName;
	}
	
	public boolean isResultsAvailable() {
		return isResultsAvailable;
	}

	public void setResultsAvailable(boolean isResultsAvailable) {
		this.isResultsAvailable = isResultsAvailable;
	}

	public String getResults_hotelName() {
		return results_hotelName;
	}
	public void setResults_hotelName(String results_hotelName) {
		this.results_hotelName = results_hotelName;
	}

	public String getResults_TotalRate() {
		return results_TotalRate;
	}

	public int getCount_nights() {
		return count_nights;
	}

	

	public long getTime_searchButtClick() {
		return time_searchButtClick;
	}

	

	public String getNewWindow_HotelName() {
		return newWindow_HotelName;
	}



	public void setNewWindow_HotelName(String newWindow_HotelName) {
		this.newWindow_HotelName = newWindow_HotelName;
	}



	public String getNewWindow_Address() {
		return newWindow_Address;
	}



	public void setNewWindow_Address(String newWindow_Address) {
		this.newWindow_Address = newWindow_Address;
	}



	public void setTime_searchButtClick(long time_searchButtClick) {
		this.time_searchButtClick = time_searchButtClick;
	}



	public long getTime_resultsAvailble() {
		return time_resultsAvailble;
	}



	public void setTime_resultsAvailble(long time_resultsAvailble) {
		this.time_resultsAvailble = time_resultsAvailble;
	}



	public long getTime_addtoCartButtClick() {
		return time_addtoCartButtClick;
	}



	public void setTime_addtoCartButtClick(long time_addtoCartButtClick) {
		this.time_addtoCartButtClick = time_addtoCartButtClick;
	}



	public long getTime_addedToCart() {
		return time_addedToCart;
	}



	public void setTime_addedToCart(long time_addedToCart) {
		this.time_addedToCart = time_addedToCart;
	}



	public long getTime_confirmButt() {
		return time_confirmButt;
	}



	public void setTime_confirmButt(long time_confirmButt) {
		this.time_confirmButt = time_confirmButt;
	}



	public long getTime_ReserVationNoAvailable() {
		return time_ReserVationNoAvailable;
	}



	public void setTime_ReserVationNoAvailable(long time_ReserVationNoAvailable) {
		this.time_ReserVationNoAvailable = time_ReserVationNoAvailable;
	}



	public boolean isCancellationLinkAvailable() {
		return isCancellationLinkAvailable;
	}



	public void setCancellationLinkAvailable(boolean isCancellationLinkAvailable) {
		this.isCancellationLinkAvailable = isCancellationLinkAvailable;
	}



	public boolean isRestrictionLinkAvailable() {
		return isRestrictionLinkAvailable;
	}



	public void setRestrictionLinkAvailable(boolean isRestrictionLinkAvailable) {
		this.isRestrictionLinkAvailable = isRestrictionLinkAvailable;
	}



	public boolean isHotelInfoLinkAvailable() {
		return isHotelInfoLinkAvailable;
	}



	public void setHotelInfoLinkAvailable(boolean isHotelInfoLinkAvailable) {
		this.isHotelInfoLinkAvailable = isHotelInfoLinkAvailable;
	}



	public boolean isSpecialsLinkAvailable() {
		return isSpecialsLinkAvailable;
	}



	public void setSpecialsLinkAvailable(boolean isSpecialsLinkAvailable) {
		this.isSpecialsLinkAvailable = isSpecialsLinkAvailable;
	}



	public String getConfirmationTOD_Address() {
		return confirmationTOD_Address;
	}



	public void setConfirmationTOD_Address(String confirmationTOD_Address) {
		this.confirmationTOD_Address = confirmationTOD_Address;
	}



	public String getConfirmationTOD_City() {
		return confirmationTOD_City;
	}



	public void setConfirmationTOD_City(String confirmationTOD_City) {
		this.confirmationTOD_City = confirmationTOD_City;
	}



	public String getConfirmationTOD_Country() {
		return confirmationTOD_Country;
	}



	public void setConfirmationTOD_Country(String confirmationTOD_Country) {
		this.confirmationTOD_Country = confirmationTOD_Country;
	}



	public String getConfirmationTOD_Zip() {
		return confirmationTOD_Zip;
	}



	public void setConfirmationTOD_Zip(String confirmationTOD_Zip) {
		this.confirmationTOD_Zip = confirmationTOD_Zip;
	}



	public String getConfirmationTOD_Email() {
		return confirmationTOD_Email;
	}



	public void setConfirmationTOD_Email(String confirmationTOD_Email) {
		this.confirmationTOD_Email = confirmationTOD_Email;
	}



	public String getConfirmationTOD_phone() {
		return confirmationTOD_phone;
	}



	public void setConfirmationTOD_phone(String confirmationTOD_phone) {
		this.confirmationTOD_phone = confirmationTOD_phone;
	}



	public void setCount_nights(int count_nights) {
		this.count_nights = count_nights;
	}



	public int getCount_rooms() {
		return count_rooms;
	}



	public void setCount_rooms(int count_rooms) {
		this.count_rooms = count_rooms;
	}



	public int getCount_adults() {
		return count_adults;
	}



	public void setCount_adults(int count_adults) {
		this.count_adults = count_adults;
	}



	public int getCount_children() {
		return count_children;
	}



	public void setCount_children(int count_children) {
		this.count_children = count_children;
	}



	public void setResults_TotalRate(String results_TotalRate) {
		this.results_TotalRate = results_TotalRate;
	}
	public ArrayList<RoomTypes> getRoomTypes() {
		return roomTypes;
	}

	public void setRoomTypes(ArrayList<RoomTypes> roomTypes) {
		this.roomTypes = roomTypes;
	}
	public ArrayList<String> getPaymentTravelDetails_RoomAdults() {
		return paymentTravelDetails_RoomAdults;
	}

	public void setPaymentTravelDetails_RoomAdults(
			ArrayList<String> paymentTravelDetails_RoomAdults) {
		this.paymentTravelDetails_RoomAdults = paymentTravelDetails_RoomAdults;
	}

	public ArrayList<String> getPaymentTravelDetails_RoomChildren() {
		return paymentTravelDetails_RoomChildren;
	}

	public void setPaymentTravelDetails_RoomChildren(
			ArrayList<String> paymentTravelDetails_RoomChildren) {
		this.paymentTravelDetails_RoomChildren = paymentTravelDetails_RoomChildren;
	}

	public ArrayList<String> getPaymentTravelDetails_RoomType() {
		return paymentTravelDetails_RoomType;
	}

	public void setPaymentTravelDetails_RoomType(
			ArrayList<String> paymentTravelDetails_RoomType) {
		this.paymentTravelDetails_RoomType = paymentTravelDetails_RoomType;
	}

	public ArrayList<String> getPaymentTravelDetails_RoomOccupancy() {
		return paymentTravelDetails_RoomOccupancy;
	}

	public void setPaymentTravelDetails_RoomOccupancy(
			ArrayList<String> paymentTravelDetails_RoomOccupancy) {
		this.paymentTravelDetails_RoomOccupancy = paymentTravelDetails_RoomOccupancy;
	}

	public ArrayList<String> getPaymentTravelDetails_RoomRatePlan() {
		return paymentTravelDetails_RoomRatePlan;
	}

	public void setPaymentTravelDetails_RoomRatePlan(
			ArrayList<String> paymentTravelDetails_RoomRatePlan) {
		this.paymentTravelDetails_RoomRatePlan = paymentTravelDetails_RoomRatePlan;
	}

	public ArrayList<String> getPaymentTravelDetails_RoomDaysCount() {
		return paymentTravelDetails_RoomDaysCount;
	}

	public void setPaymentTravelDetails_RoomDaysCount(
			ArrayList<String> paymentTravelDetails_RoomDaysCount) {
		this.paymentTravelDetails_RoomDaysCount = paymentTravelDetails_RoomDaysCount;
	}

	

	public ArrayList<ArrayList<String>> getPaymentTravelDetails_RoomDailyRates() {
		return paymentTravelDetails_RoomDailyRates;
	}

	public void setPaymentTravelDetails_RoomDailyRates(
			ArrayList<ArrayList<String>> paymentTravelDetails_RoomDailyRates) {
		this.paymentTravelDetails_RoomDailyRates = paymentTravelDetails_RoomDailyRates;
	}

	public ArrayList<String> getPaymentTravelDetails_Roomtotal() {
		return paymentTravelDetails_Roomtotal;
	}

	public void setPaymentTravelDetails_Roomtotal(
			ArrayList<String> paymentTravelDetails_Roomtotal) {
		this.paymentTravelDetails_Roomtotal = paymentTravelDetails_Roomtotal;
	}

	

	public String getHoteldescription() {
		return hoteldescription;
	}

	public void setHoteldescription(String hoteldescription) {
		this.hoteldescription = hoteldescription;
	}

	public ArrayList<String> getConfirmationCancelPolicy() {
		return confirmationCancelPolicy;
	}

	public void setConfirmationCancelPolicy(
			ArrayList<String> confirmationCancelPolicy) {
		this.confirmationCancelPolicy = confirmationCancelPolicy;
	}

	public String getConfirmationTS_HotelName() {
		return confirmationTS_HotelName;
	}

	public void setConfirmationTS_HotelName(String confirmationTS_HotelName) {
		this.confirmationTS_HotelName = confirmationTS_HotelName;
	}

	public String getConfirmationTS_qty() {
		return confirmationTS_qty;
	}

	public void setConfirmationTS_qty(String confirmationTS_qty) {
		this.confirmationTS_qty = confirmationTS_qty;
	}

	public String getConfirmationTS_checkInDate() {
		return confirmationTS_checkInDate;
	}

	public void setConfirmationTS_checkInDate(String confirmationTS_checkInDate) {
		this.confirmationTS_checkInDate = confirmationTS_checkInDate;
	}

	public String getConfirmationTS_checkoutDate() {
		return confirmationTS_checkoutDate;
	}

	public void setConfirmationTS_checkoutDate(String confirmationTS_checkoutDate) {
		this.confirmationTS_checkoutDate = confirmationTS_checkoutDate;
	}

	public String getConfirmationTS_amount() {
		return confirmationTS_amount;
	}

	public void setConfirmationTS_amount(String confirmationTS_amount) {
		this.confirmationTS_amount = confirmationTS_amount;
	}

	public String getConfirmationTS_confirmedTotal() {
		return confirmationTS_confirmedTotal;
	}

	public void setConfirmationTS_confirmedTotal(
			String confirmationTS_confirmedTotal) {
		this.confirmationTS_confirmedTotal = confirmationTS_confirmedTotal;
	}

	public String getConfirmationTS_Total() {
		return confirmationTS_Total;
	}

	public void setConfirmationTS_Total(String confirmationTS_Total) {
		this.confirmationTS_Total = confirmationTS_Total;
	}

	public String getConfirmationTD_checkInDate() {
		return confirmationTD_checkInDate;
	}

	public void setConfirmationTD_checkInDate(String confirmationTD_checkInDate) {
		this.confirmationTD_checkInDate = confirmationTD_checkInDate;
	}

	public String getConfirmationTD_checkOutDate() {
		return confirmationTD_checkOutDate;
	}

	public void setConfirmationTD_checkOutDate(String confirmationTD_checkOutDate) {
		this.confirmationTD_checkOutDate = confirmationTD_checkOutDate;
	}

	public String getConfirmationTD_nights() {
		return confirmationTD_nights;
	}

	public void setConfirmationTD_nights(String confirmationTD_nights) {
		this.confirmationTD_nights = confirmationTD_nights;
	}

	public String getConfirmationTD_rooms() {
		return confirmationTD_rooms;
	}

	public void setConfirmationTD_rooms(String confirmationTD_rooms) {
		this.confirmationTD_rooms = confirmationTD_rooms;
	}

	public String getConfirmationTD_adults() {
		return confirmationTD_adults;
	}

	public void setConfirmationTD_adults(String confirmationTD_adults) {
		this.confirmationTD_adults = confirmationTD_adults;
	}

	public String getConfirmationTD_children() {
		return confirmationTD_children;
	}

	public void setConfirmationTD_children(String confirmationTD_children) {
		this.confirmationTD_children = confirmationTD_children;
	}

	public String getConfirmationTD_HotelName() {
		return confirmationTD_HotelName;
	}

	public void setConfirmationTD_HotelName(String confirmationTD_HotelName) {
		this.confirmationTD_HotelName = confirmationTD_HotelName;
	}

	public String getConfirmationTD_hotelAddress() {
		return confirmationTD_hotelAddress;
	}

	public void setConfirmationTD_hotelAddress(String confirmationTD_hotelAddress) {
		this.confirmationTD_hotelAddress = confirmationTD_hotelAddress;
	}

	public String getConfirmationTD_city() {
		return confirmationTD_city;
	}

	public void setConfirmationTD_city(String confirmationTD_city) {
		this.confirmationTD_city = confirmationTD_city;
	}

	public String getConfirmationTD_state() {
		return confirmationTD_state;
	}

	public void setConfirmationTD_state(String confirmationTD_state) {
		this.confirmationTD_state = confirmationTD_state;
	}

	public String getConfirmationTD_country() {
		return confirmationTD_country;
	}

	public void setConfirmationTD_country(String confirmationTD_country) {
		this.confirmationTD_country = confirmationTD_country;
	}

	public String getConfirmationTD_BookingDate() {
		return confirmationTD_BookingDate;
	}

	public void setConfirmationTD_BookingDate(String confirmationTD_BookingDate) {
		this.confirmationTD_BookingDate = confirmationTD_BookingDate;
	}

	public String getConfirmation_ReservationNO() {
		return confirmation_ReservationNO;
	}

	public void setConfirmation_ReservationNO(String confirmation_ReservationNO) {
		this.confirmation_ReservationNO = confirmation_ReservationNO;
	}

	

	public ArrayList<String> getConfirmationTD_FirstName() {
		return confirmationTD_FirstName;
	}

	public void setConfirmationTD_FirstName(
			ArrayList<String> confirmationTD_FirstName) {
		this.confirmationTD_FirstName = confirmationTD_FirstName;
	}

	public ArrayList<String> getConfirmationTD_LastNsme() {
		return confirmationTD_LastNsme;
	}

	public void setConfirmationTD_LastNsme(ArrayList<String> confirmationTD_LastNsme) {
		this.confirmationTD_LastNsme = confirmationTD_LastNsme;
	}

	

	public ArrayList<String> getConfirmationTD_Adults() {
		return confirmationTD_Adults;
	}

	public void setConfirmationTD_Adults(ArrayList<String> confirmationTD_Adults) {
		this.confirmationTD_Adults = confirmationTD_Adults;
	}

	public ArrayList<String> getConfirmationTD_Children() {
		return confirmationTD_Children;
	}

	public void setConfirmationTD_Children(ArrayList<String> confirmationTD_Children) {
		this.confirmationTD_Children = confirmationTD_Children;
	}

	public ArrayList<String> getConfirmationTD_RoomType() {
		return confirmationTD_RoomType;
	}

	public void setConfirmationTD_RoomType(ArrayList<String> confirmationTD_RoomType) {
		this.confirmationTD_RoomType = confirmationTD_RoomType;
	}

	public ArrayList<String> getConfirmationTD_RoomBedType() {
		return confirmationTD_RoomBedType;
	}

	public void setConfirmationTD_RoomBedType(
			ArrayList<String> confirmationTD_RoomBedType) {
		this.confirmationTD_RoomBedType = confirmationTD_RoomBedType;
	}

	public ArrayList<String> getConfirmationTD_RoomRatePlan() {
		return confirmationTD_RoomRatePlan;
	}

	public void setConfirmationTD_RoomRatePlan(
			ArrayList<String> confirmationTD_RoomRatePlan) {
		this.confirmationTD_RoomRatePlan = confirmationTD_RoomRatePlan;
	}

	public ArrayList<String> getConfirmationTD_DaysCount() {
		return confirmationTD_DaysCount;
	}

	public void setConfirmationTD_DaysCount(
			ArrayList<String> confirmationTD_DaysCount) {
		this.confirmationTD_DaysCount = confirmationTD_DaysCount;
	}

	

	public ArrayList<ArrayList<String>> getConfirmationTD_DailyRates() {
		return confirmationTD_DailyRates;
	}

	public void setConfirmationTD_DailyRates(
			ArrayList<ArrayList<String>> confirmationTD_DailyRates) {
		this.confirmationTD_DailyRates = confirmationTD_DailyRates;
	}

	public ArrayList<String> getConfirmationTD_total() {
		return confirmationTD_total;
	}

	public void setConfirmationTD_total(ArrayList<String> confirmationTD_total) {
		this.confirmationTD_total = confirmationTD_total;
	}

	public String getConfirmationTD_BookingTotal() {
		return confirmationTD_BookingTotal;
	}

	public void setConfirmationTD_BookingTotal(String confirmationTD_BookingTotal) {
		this.confirmationTD_BookingTotal = confirmationTD_BookingTotal;
	}

	

	public ArrayList<String> getCustomerTitle() {
		return customerTitle;
	}

	public void setCustomerTitle(ArrayList<String> customerTitle) {
		this.customerTitle = customerTitle;
	}

	public ArrayList<String> getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(ArrayList<String> customerLastName) {
		this.customerLastName = customerLastName;
	}

	public ArrayList<String> getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(ArrayList<String> customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getPaymentShopCart_hotelName() {
		return paymentShopCart_hotelName;
	}

	public void setPaymentShopCart_hotelName(String paymentShopCart_hotelName) {
		this.paymentShopCart_hotelName = paymentShopCart_hotelName;
	}

	public String getPaymentShopCart_qty() {
		return paymentShopCart_qty;
	}

	public void setPaymentShopCart_qty(String paymentShopCart_qty) {
		this.paymentShopCart_qty = paymentShopCart_qty;
	}

	public String getPaymentShopCart_dateFrom() {
		return paymentShopCart_dateFrom;
	}

	public void setPaymentShopCart_dateFrom(String paymentShopCart_dateFrom) {
		this.paymentShopCart_dateFrom = paymentShopCart_dateFrom;
	}

	public String getPaymentShopCart_dateTo() {
		return paymentShopCart_dateTo;
	}

	public void setPaymentShopCart_dateTo(String paymentShopCart_dateTo) {
		this.paymentShopCart_dateTo = paymentShopCart_dateTo;
	}

	public String getPaymentShopCart_amount() {
		return paymentShopCart_amount;
	}

	public void setPaymentShopCart_amount(String paymentShopCart_amount) {
		this.paymentShopCart_amount = paymentShopCart_amount;
	}

	public String getPaymentShopCart_CartTotal() {
		return paymentShopCart_CartTotal;
	}

	public void setPaymentShopCart_CartTotal(String paymentShopCart_CartTotal) {
		this.paymentShopCart_CartTotal = paymentShopCart_CartTotal;
	}

	public String getPaymentTravelDetails_checkIn() {
		return paymentTravelDetails_checkIn;
	}

	public void setPaymentTravelDetails_checkIn(String paymentTravelDetails_checkIn) {
		this.paymentTravelDetails_checkIn = paymentTravelDetails_checkIn;
	}

	public String getPaymentTravelDetails_checkOut() {
		return paymentTravelDetails_checkOut;
	}

	public void setPaymentTravelDetails_checkOut(
			String paymentTravelDetails_checkOut) {
		this.paymentTravelDetails_checkOut = paymentTravelDetails_checkOut;
	}

	public String getPaymentTravelDetails_Nights() {
		return paymentTravelDetails_Nights;
	}

	public void setPaymentTravelDetails_Nights(String paymentTravelDetails_Nights) {
		this.paymentTravelDetails_Nights = paymentTravelDetails_Nights;
	}

	public String getPaymentTravelDetails_Rooms() {
		return paymentTravelDetails_Rooms;
	}

	public void setPaymentTravelDetails_Rooms(String paymentTravelDetails_Rooms) {
		this.paymentTravelDetails_Rooms = paymentTravelDetails_Rooms;
	}

	public String getPaymentTravelDetails_Adults() {
		return paymentTravelDetails_Adults;
	}

	public void setPaymentTravelDetails_Adults(String paymentTravelDetails_Adults) {
		this.paymentTravelDetails_Adults = paymentTravelDetails_Adults;
	}

	public String getPaymentTravelDetails_Children() {
		return paymentTravelDetails_Children;
	}

	public void setPaymentTravelDetails_Children(
			String paymentTravelDetails_Children) {
		this.paymentTravelDetails_Children = paymentTravelDetails_Children;
	}

	public String getPaymentTravelDetails_HotelName() {
		return paymentTravelDetails_HotelName;
	}

	public void setPaymentTravelDetails_HotelName(
			String paymentTravelDetails_HotelName) {
		this.paymentTravelDetails_HotelName = paymentTravelDetails_HotelName;
	}

	public String getPaymentTravelDetails_Address() {
		return paymentTravelDetails_Address;
	}

	public void setPaymentTravelDetails_Address(String paymentTravelDetails_Address) {
		this.paymentTravelDetails_Address = paymentTravelDetails_Address;
	}

	public String getPaymentTravelDetails_city() {
		return paymentTravelDetails_city;
	}

	public void setPaymentTravelDetails_city(String paymentTravelDetails_city) {
		this.paymentTravelDetails_city = paymentTravelDetails_city;
	}

	public String getPaymentTravelDetails_state() {
		return paymentTravelDetails_state;
	}

	public void setPaymentTravelDetails_state(String paymentTravelDetails_state) {
		this.paymentTravelDetails_state = paymentTravelDetails_state;
	}

	public String getPaymentTravelDetails_country() {
		return paymentTravelDetails_country;
	}

	public void setPaymentTravelDetails_country(String paymentTravelDetails_country) {
		this.paymentTravelDetails_country = paymentTravelDetails_country;
	}

	

	public String getPaymentTravelDetails_RoomBookingTotal() {
		return paymentTravelDetails_RoomBookingTotal;
	}

	public void setPaymentTravelDetails_RoomBookingTotal(
			String paymentTravelDetails_RoomBookingTotal) {
		this.paymentTravelDetails_RoomBookingTotal = paymentTravelDetails_RoomBookingTotal;
	}

	public String getPaymentTravelDetails_RoomDetailsTitle() {
		return paymentTravelDetails_RoomDetailsTitle;
	}

	public void setPaymentTravelDetails_RoomDetailsTitle(
			String paymentTravelDetails_RoomDetailsTitle) {
		this.paymentTravelDetails_RoomDetailsTitle = paymentTravelDetails_RoomDetailsTitle;
	}

	public String getPaymentTravelDetails_RoomDetailsLastName() {
		return paymentTravelDetails_RoomDetailsLastName;
	}

	public void setPaymentTravelDetails_RoomDetailsLastName(
			String paymentTravelDetails_RoomDetailsLastName) {
		this.paymentTravelDetails_RoomDetailsLastName = paymentTravelDetails_RoomDetailsLastName;
	}

	public String getPaymentTravelDetails_RoomDetailsFirstName() {
		return paymentTravelDetails_RoomDetailsFirstName;
	}

	public void setPaymentTravelDetails_RoomDetailsFirstName(
			String paymentTravelDetails_RoomDetailsFirstName) {
		this.paymentTravelDetails_RoomDetailsFirstName = paymentTravelDetails_RoomDetailsFirstName;
	}

	public String getPaymentShoppingCartTotal() {
		return paymentShoppingCartTotal;
	}

	public void setPaymentShoppingCartTotal(String paymentShoppingCartTotal) {
		this.paymentShoppingCartTotal = paymentShoppingCartTotal;
	}

	public ArrayList<String> getCancelPolicy() {
		return cancelPolicy;
	}

	public void setCancelPolicy(ArrayList<String> cancelPolicy) {
		this.cancelPolicy = cancelPolicy;
	}

	public ArrayList<String> getPaymentCancelPolicy() {
		return paymentCancelPolicy;
	}

	public void setPaymentCancelPolicy(ArrayList<String> paymentCancelPolicy) {
		this.paymentCancelPolicy = paymentCancelPolicy;
	}

	public String getIsCheckOut() {
		return isCheckOut;
	}

	public void setIsCheckOut(String isCheckOut) {
		this.isCheckOut = isCheckOut;
	}

	public String getResults_shoppingCartValue() {
		return results_shoppingCartValue;
	}

	public void setResults_shoppingCartValue(String results_shoppingCartValue) {
		this.results_shoppingCartValue = results_shoppingCartValue;
	}

	public String getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(String reservationDate) {
		this.reservationDate = reservationDate;
	}

	

	public String getResults_hotelAddress() {
		return results_hotelAddress;
	}

	public void setResults_hotelAddress(String results_hotelAddress) {
		this.results_hotelAddress = results_hotelAddress;
	}

	public String getResults_CheckInDate() {
		return results_CheckInDate;
	}

	public void setResults_CheckInDate(String results_CheckInDate) {
		this.results_CheckInDate = results_CheckInDate;
	}

	public String getResults_CheckOutDate() {
		return results_CheckOutDate;
	}

	public void setResults_CheckOutDate(String results_CheckOutDate) {
		this.results_CheckOutDate = results_CheckOutDate;
	}

	public String getResults_Nights() {
		return results_Nights;
	}

	public void setResults_Nights(String results_Nights) {
		this.results_Nights = results_Nights;
	}

	public String getResults_Rooms() {
		return results_Rooms;
	}

	public void setResults_Rooms(String results_Rooms) {
		this.results_Rooms = results_Rooms;
	}

	public String getResults_Adults() {
		return results_Adults;
	}

	public void setResults_Adults(String results_Adults) {
		this.results_Adults = results_Adults;
	}

	public String getResults_children() {
		return results_children;
	}

	public void setResults_children(String results_children) {
		this.results_children = results_children;
	}

	public String getExcelHotelName() {
		return excelHotelName;
	}

	public void setExcelHotelName(String excelHotelName) {
		this.excelHotelName = excelHotelName;
	}



	public String getVoucher_bookingDate() {
		return voucher_bookingDate;
	}



	public void setVoucher_bookingDate(String voucher_bookingDate) {
		this.voucher_bookingDate = voucher_bookingDate;
	}



	public String getVoucher_bookingNo() {
		return voucher_bookingNo;
	}



	public void setVoucher_bookingNo(String voucher_bookingNo) {
		this.voucher_bookingNo = voucher_bookingNo;
	}



	public String getVoucher_ToOrderNo() {
		return voucher_ToOrderNo;
	}



	public void setVoucher_ToOrderNo(String voucher_ToOrderNo) {
		this.voucher_ToOrderNo = voucher_ToOrderNo;
	}



	public String getVoucher_ToName() {
		return voucher_ToName;
	}



	public void setVoucher_ToName(String voucher_ToName) {
		this.voucher_ToName = voucher_ToName;
	}



	public String getVoucher_ToEmail() {
		return voucher_ToEmail;
	}



	public void setVoucher_ToEmail(String voucher_ToEmail) {
		this.voucher_ToEmail = voucher_ToEmail;
	}



	public String getVoucher_HotelName() {
		return voucher_HotelName;
	}



	public void setVoucher_HotelName(String voucher_HotelName) {
		this.voucher_HotelName = voucher_HotelName;
	}



	public String getVoucher_HotelAddress() {
		return voucher_HotelAddress;
	}



	public void setVoucher_HotelAddress(String voucher_HotelAddress) {
		this.voucher_HotelAddress = voucher_HotelAddress;
	}



	public String getVoucher_HotelCity() {
		return voucher_HotelCity;
	}



	public void setVoucher_HotelCity(String voucher_HotelCity) {
		this.voucher_HotelCity = voucher_HotelCity;
	}



	public ArrayList<String> getVoucher_customerNames() {
		return voucher_customerNames;
	}



	public void setVoucher_customerNames(ArrayList<String> voucher_customerNames) {
		this.voucher_customerNames = voucher_customerNames;
	}



	public String getVoucher_CheckInDate() {
		return voucher_CheckInDate;
	}



	public void setVoucher_CheckInDate(String voucher_CheckInDate) {
		this.voucher_CheckInDate = voucher_CheckInDate;
	}



	public String getVoucher_CheckOutDate() {
		return voucher_CheckOutDate;
	}



	public void setVoucher_CheckOutDate(String voucher_CheckOutDate) {
		this.voucher_CheckOutDate = voucher_CheckOutDate;
	}



	public String getVoucher_Nights() {
		return voucher_Nights;
	}



	public void setVoucher_Nights(String voucher_Nights) {
		this.voucher_Nights = voucher_Nights;
	}



	public String getVoucher_Rooms() {
		return voucher_Rooms;
	}



	public void setVoucher_Rooms(String voucher_Rooms) {
		this.voucher_Rooms = voucher_Rooms;
	}



	public String getVoucher_Adults() {
		return voucher_Adults;
	}



	public void setVoucher_Adults(String voucher_Adults) {
		this.voucher_Adults = voucher_Adults;
	}



	public String getVoucher_Children() {
		return voucher_Children;
	}



	public void setVoucher_Children(String voucher_Children) {
		this.voucher_Children = voucher_Children;
	}



	public String getVoucher_RoomTypes() {
		return voucher_RoomTypes;
	}



	public void setVoucher_RoomTypes(String voucher_RoomTypes) {
		this.voucher_RoomTypes = voucher_RoomTypes;
	}



	public String getVoucher_OccupancyRatePlan() {
		return voucher_OccupancyRatePlan;
	}



	public void setVoucher_OccupancyRatePlan(String voucher_OccupancyRatePlan) {
		this.voucher_OccupancyRatePlan = voucher_OccupancyRatePlan;
	}



	public boolean isVoucherLoaded() {
		return isVoucherLoaded;
	}



	public void setVoucherLoaded(boolean isVoucherLoaded) {
		this.isVoucherLoaded = isVoucherLoaded;
	}
	
	
	

}
