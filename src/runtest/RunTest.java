package runtest;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.*;
import com.model.*;
import com.reader.*;

public class RunTest {
	
	private WebDriver driver         = null;
	private FirefoxProfile profile;
	private LoadDetails loadDetails;
	private StringBuffer  PrintWriter;
	private int TestCaseCount;
	private TreeMap<String, SearchInfo>   SearchList;
	private int i=0;
	
	ArrayList<Map<Integer, String>> sheetlist = new ArrayList<Map<Integer, String>>();
	ArrayList<TO_Details>  toList   			= new ArrayList<TO_Details>();
	
	Map<Integer, String> hotelSearchInfoMap 		= null;
	Map<Integer, String> hotelRatesMap 				= null;
	Map<Integer, String> hotelPromotionMap 			= null;
	
	@Before
	public void setUp(){

		profile = new FirefoxProfile(new File(PG_Properties.getProperty("Firefox.Profile")));
		driver = new FirefoxDriver(profile);
		PrintWriter = new StringBuffer();
		
		ExcelReader reader = new ExcelReader();		
		sheetlist 	= reader.init(PG_Properties.getProperty("Excel.Path.HotelReservation"));
		
		loadDetails = new LoadDetails(sheetlist);
		
		toList         	  =   loadDetails.loadTODetails();	
		
		hotelSearchInfoMap 	= sheetlist.get(0);
		hotelRatesMap		= sheetlist.get(2);
		hotelPromotionMap   = sheetlist.get(3);
		
		
		try {
			SearchList = loadDetails.loadReservationInfo(hotelSearchInfoMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			SearchList = loadDetails.loadRateDetails(hotelRatesMap, SearchList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			SearchList = loadDetails.loadPromotionDetails(hotelPromotionMap, SearchList);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void hotelReservatiionFlow() throws InterruptedException, IOException{
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		String currentDateforMatch = dateFormatcurrentDate.format(cal.getTime());
		WebReservationFlow webreservationFlow = new WebReservationFlow(driver);
		InternalLogics	internLogic =  new InternalLogics();
		
		Map<String,TO_Details> tODMap 	= LoadDetails.tODetailsconvertIntoMap(toList);
		
		
		PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Hotel Reservation Report - Date ["+currentDateforMatch+"]</p></div>");
		PrintWriter.append("<body>");		
		PrintWriter.append("<br><br>");
		
		TestCaseCount = 0;
		PrintWriter.append("<br><br><table ><tr><th>Test Case No</th><th>Test Description</th><th>Expected Results</th><th>Actual Results</th> <th>Test Status</th></tr>");
		
		Iterator<Map.Entry<String, SearchInfo>> it = (Iterator<Entry<String, SearchInfo>>) SearchList.entrySet().iterator();
		
		//Search scenarios
		while(it.hasNext()){
			
			HotelDetails hotelDetails 			= new HotelDetails();
			Reservation reservationDetails  	= new Reservation();
			ConfirmationReport conDetails 		= new ConfirmationReport();
			CancellationDetails cancelDetails	= new CancellationDetails();
			Modification modDetails  			= new Modification();
			TO_Details toDetails = null;
			
			SearchInfo searchInfo  = it.next().getValue();
			webreservationFlow.addHotel(searchInfo);
			internLogic.addHotel(searchInfo);
			i++;
									
			try {
				
				String toName = searchInfo.getTourOperatorName();
							
				if (tODMap.containsKey(toName)) {
					
					toDetails = tODMap.get(toName);
					System.out.println("TO Name : "+toDetails.getTo_Name()+"  , search scenario count: "+toName);		
									
				} else {
					System.out.println("====>>>TO not found for the search scenario count "+toName+"<<<========");							
				}
				
				
				HotelDetails hotelDet = internLogic.dailyRateCalculation();			
				driver = webreservationFlow.searchEngine(driver);
				hotelDetails = webreservationFlow.availableResults(hotelDet, driver);
				
				TestCaseCount++;
				PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Login Status</td>");
				PrintWriter.append("<td>Should login successfully</td>");
				
				if (login(driver) == true){
					
					PrintWriter.append("<td>Success..!!!</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
				
					TestCaseCount++;
					PrintWriter.append("<tr><td>"+TestCaseCount+"</td> <td>Hotel Reservations</td>");
					PrintWriter.append("<td>Reservation should be made</td>");
					
					if (hotelDetails.isResultsAvailable() == true) {
						
						ReservationReportFlow reservatinFlow = new ReservationReportFlow(hotelDetails, searchInfo, driver);						
						reservationDetails = reservatinFlow.loadReservationFlow(driver);
						
						ConfirmationReportFlow conFlow = new ConfirmationReportFlow(hotelDetails, searchInfo, driver);
						conDetails = conFlow.loadConfirmationReportFlow(driver);
						
						CancellationReportFlow cancelFlow = new CancellationReportFlow(hotelDetails, searchInfo, driver);
						cancelDetails = cancelFlow.loadCancellationFlow(driver);
						
						ModificationFlow modFlow = new ModificationFlow(hotelDetails, searchInfo, driver);
						modDetails = modFlow.loadModificationFlow(driver);
										
						webreservationFlow.cancellationFlow(driver);
						
						WebReportsReader webReport = new WebReportsReader(hotelDetails, reservationDetails, conDetails, cancelDetails, modDetails, searchInfo, toDetails, driver);
						webReport.getHotelReservationReport(driver);
						webReport.getCancellationReport(driver);
						
						PrintWriter.append("<td>HOTEL >> "+searchInfo.getExcelHotelName()+" -> Reservation No - ["+hotelDetails.getConfirmation_ReservationNO()+"]</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><tr>");	
						
					}else{
						
						WebReportsReader webReport = new WebReportsReader(hotelDetails, reservationDetails, conDetails, cancelDetails, modDetails, searchInfo, toDetails, driver);
						webReport.getHotelReservationReport(driver);
						webReport.getCancellationReport(driver);
						
						PrintWriter.append("<td>Results not Available</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
											
					}
			
				}else{
					
					PrintWriter.append("<td>Login Errorrr..!!!!</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><tr>");
					
				}
									
									
			} catch (Exception e) {
				e.printStackTrace();
				
				WebReportsReader webReport = new WebReportsReader(hotelDetails, reservationDetails, conDetails, cancelDetails, modDetails, searchInfo, toDetails, driver);
				webReport.getHotelReservationReport(driver);
				webReport.getCancellationReport(driver);
				
		        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(scrFile, new File("ScreenShots/FailedImage_"+i+".png"));
		        
				PrintWriter.append("<td>Error :- "+e.toString()+"</td>");
				PrintWriter.append("<td class='Failed'>FAIL</td><tr>");	
								
			}	
		}
		
		PrintWriter.append("</table>");
	}
	
	
	
	@After
	public void tearDown() throws IOException{
		
		PrintWriter.append("</body></html>");
		BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(PG_Properties.getProperty("HotelReservationReport"))));
		bwr.write(PrintWriter.toString());
		bwr.flush();
		bwr.close();
	    driver.quit();
		
	}
	
	public boolean login(WebDriver Driver){

		this.driver = Driver;
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.get(PG_Properties.getProperty("Baseurl") + "/common/LoginPage.do");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("userId")));
		driver.findElement(By.id("userId")).clear();
		driver.findElement(By.id("userId")).sendKeys(PG_Properties.getProperty("UserName"));
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(PG_Properties.getProperty("Password"));
		driver.findElement(By.xpath(".//*[@id='loginbutton']/img")).click();
		
		try {			
			driver.findElement(By.id("mainMenuItems"));
			return true;
		} catch (Exception e) {
			System.out.println("Login Fail...");
			return false;
		}		
	}

}
