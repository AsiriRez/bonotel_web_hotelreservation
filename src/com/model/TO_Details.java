package com.model;

public class TO_Details {
	
	private String to_Name;
	private String to_Address;
	private String to_City;
	private String to_Country;
	private String to_Zip;
	private String to_Email;
	private String to_phone;
	
	
	public String getTo_Name() {
		return to_Name;
	}
	public void setTo_Name(String to_Name) {
		this.to_Name = to_Name;
	}
	public String getTo_Address() {
		return to_Address;
	}
	public void setTo_Address(String to_Address) {
		this.to_Address = to_Address;
	}
	public String getTo_City() {
		return to_City;
	}
	public void setTo_City(String to_City) {
		this.to_City = to_City;
	}
	public String getTo_Country() {
		return to_Country;
	}
	public void setTo_Country(String to_Country) {
		this.to_Country = to_Country;
	}
	public String getTo_Zip() {
		return to_Zip;
	}
	public void setTo_Zip(String to_Zip) {
		this.to_Zip = to_Zip;
	}
	public String getTo_Email() {
		return to_Email;
	}
	public void setTo_Email(String to_Email) {
		this.to_Email = to_Email;
	}
	public String getTo_phone() {
		return to_phone;
	}
	public void setTo_phone(String to_phone) {
		this.to_phone = to_phone;
	}
	
	

}
