package com.reader;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.*;

public class ModificationFlow {
	
	private WebDriver driver = null;
	private HotelDetails hotelDetails = new HotelDetails();
	private SearchInfo sInfo = new SearchInfo();
	private Modification modifyDetails;
	
	public ModificationFlow(HotelDetails hoteldetails, SearchInfo sInfo, WebDriver Driver){
		
		this.driver = Driver;
		this.hotelDetails = hoteldetails;	
		this.sInfo = sInfo;	
	}
	
	
	public Modification loadModificationFlow(WebDriver webDriver) throws InterruptedException{
		
		modifyDetails = new Modification();		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.get(PG_Properties.getProperty("Baseurl")+ "/reservation/ReservationModificationSummaryPage.do");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("ManagerOverride_lkinid")));
		Thread.sleep(500);
		
		try {
					
			if (driver.findElements(By.id("ManagerOverride_lkinid")).size() != 0) {
				
				String reservationNo = hotelDetails.getConfirmation_ReservationNO();
				
				driver.findElement(By.id("reservationid")).clear();
				Thread.sleep(1500);
				driver.findElement(By.id("reservationid")).sendKeys(reservationNo);
				Thread.sleep(1500);
				driver.findElement(By.id("reservationid_lkup")).click();
				Thread.sleep(1500);
				driver.switchTo().defaultContent();
				driver.switchTo().frame("lookup");
				Thread.sleep(2500);
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				Thread.sleep(1500);
				
				//Summary
				
				String summary_hotelName = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]")).getText().split(": ")[1];
				modifyDetails.setSummary_hotelName(summary_hotelName);
				
				String summary_hotelAddress = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[2]")).getText();
				modifyDetails.setSummary_hotelAddress(summary_hotelAddress);
				
				String summary_cityStateCountry = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]")).getText().split(": ")[1];
				modifyDetails.setSummary_cityStateCountry(summary_cityStateCountry);
				
				String summary_nights = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[4]")).getText().split(": ")[1];
				modifyDetails.setSummary_nights(summary_nights);
				
				String summary_rooms = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[4]")).getText().split(": ")[1];
				modifyDetails.setSummary_rooms(summary_rooms);
				
				String summary_children = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td[4]")).getText().split(": ")[1];
				modifyDetails.setSummary_children(summary_children);
				
				String summary_bookingDate = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td[4]")).getText().split(": ")[1];
				modifyDetails.setSummary_bookingDate(summary_bookingDate);
				
				String summary_TOName = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[7]/td[4]")).getText().split(": ")[1];
				modifyDetails.setSummary_TOName(summary_TOName);
				
				String summary_chechInDate = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[2]")).getText().split(": ")[1];
				modifyDetails.setSummary_chechInDate(summary_chechInDate);
				
				String summary_checkOutDate = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[2]")).getText().split(": ")[1];
				modifyDetails.setSummary_checkOutDate(summary_checkOutDate);
				
				String summary_adults = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td[2]")).getText().split(": ")[1];
				modifyDetails.setSummary_adults(summary_adults);
				
				String summary_BookingNo = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td[2]")).getText().split(": ")[1];
				modifyDetails.setSummary_BookingNo(summary_BookingNo);
				
				String summary_BookingSource = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[7]/td[2]")).getText().split(": ")[1];
				modifyDetails.setSummary_BookingSource(summary_BookingSource);
				
				ArrayList<String> customerTitle = new ArrayList<String>();
				ArrayList<String> customerFirstName = new ArrayList<String>();
				ArrayList<String> customerLastName = new ArrayList<String>();
				int hotelRoomsCount = Integer.parseInt(sInfo.getRooms());
				
				for (int i = 1; i <= hotelRoomsCount; i++) {
					
					String cusTitle = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[2]/span")).getText().replaceAll(" ", "");
					customerTitle.add(cusTitle);	
					
					String cusLName = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[3]/span")).getText().replaceAll(" ", "");
					customerLastName.add(cusLName);
					
					String cusFName = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[4]/span")).getText().replaceAll(" ", "");
					customerFirstName.add(cusFName);
					
				}
				
				modifyDetails.setSummary_cusTitle(customerTitle);
				modifyDetails.setSummary_cusLname(customerLastName);
				modifyDetails.setSummary_cusFName(customerFirstName);
				
				/////
				
				ArrayList<String> roomSum_TypeList = new ArrayList<String>();
				ArrayList<String> bedSum_TypeList = new ArrayList<String>();
				ArrayList<String> Sum_adultsList = new ArrayList<String>();
				ArrayList<String> Sum_childrenList = new ArrayList<String>();
				ArrayList<String> Sum_guestName = new ArrayList<String>();
				ArrayList<ArrayList<String>> dailySum_Rates = new ArrayList<ArrayList<String>>();
				ArrayList<String> totalSum_Value = new ArrayList<String>();
				ArrayList<String> daysCountSum = new ArrayList<String>();
				
				for (int i = 1; i <= hotelRoomsCount; i++) {
					
					String roomType = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[9]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]")).getText();
					roomSum_TypeList.add(roomType);
					
					String bedType = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[9]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[2]")).getText();
					bedSum_TypeList.add(bedType);
					
					String adultsL = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[9]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[3]")).getText();
					Sum_adultsList.add(adultsL);
					
					String childrenL = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[9]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[4]")).getText();
					Sum_childrenList.add(childrenL);
					
					String guestName = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[9]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[5]")).getText();
					Sum_guestName.add(guestName);
					
					WebElement searchTableEle = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[9]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[6]/table"));				
					List<WebElement> daysCountSer = searchTableEle.findElements(By.tagName("td"));
					int sizeDays = (daysCountSer.size() / 2);
					daysCountSum.add(Integer.toString(sizeDays));
					
					ArrayList<String> dailyRatesLoop = new ArrayList<String>();
					WebElement searchratesEle = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[9]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[6]/table"));				
					List<WebElement> searchRate = searchratesEle.findElements(By.tagName("td"));
					
					for (int k = (sizeDays+1); k <= daysCountSer.size(); k++) {
						
						String oneDayRateSea = searchRate.get(k-1).getText().replaceAll(" ", "");
						dailyRatesLoop.add(oneDayRateSea);
						
					}
					dailySum_Rates.add(dailyRatesLoop);
					
					String totalV = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[9]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[7]")).getText();
					totalSum_Value.add(totalV);
					
				}
				
				modifyDetails.setSummary_roomTypeList(roomSum_TypeList);
				modifyDetails.setSummary_OccupancyTypeList(bedSum_TypeList);
				modifyDetails.setSummary_AdultsList(Sum_adultsList);
				modifyDetails.setSummary_childrenList(Sum_childrenList);
				modifyDetails.setSummary_GuestName(Sum_guestName);
				modifyDetails.setSummary_DailyRates(dailySum_Rates);
				modifyDetails.setSummary_DaysCount(daysCountSum);
				modifyDetails.setSummary_Total(totalSum_Value);
				
				String subTotal = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[9]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(hotelRoomsCount+2)+"]/td[3]")).getText();
				modifyDetails.setSummary_TotalBookingValue(subTotal);
				
				String totalPayable = driver.findElement(By.xpath(".//*[@id='ResModifySumId']/table[2]/tbody/tr[2]/td/table/tbody/tr[9]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(hotelRoomsCount+3)+"]/td[3]")).getText();
				modifyDetails.setSummary_TotalCost(totalPayable);
				
				
				
				
				///guest details
				
				driver.findElement(By.xpath(".//*[@id='GuestDetails_lkinid']")).click();
				Thread.sleep(1500);
				
				////////
				
				ArrayList<String> GD_customerTitle = new ArrayList<String>();
				ArrayList<String> GD_customerFirstName = new ArrayList<String>();
				ArrayList<String> GD_customerLastName = new ArrayList<String>();
				
				for (int i = 1; i <= hotelRoomsCount; i++) {
					
					String cusTitle = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[2]")).getText().replaceAll(" ", "");
					GD_customerTitle.add(cusTitle);	
					
					String cusLName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[3]")).getText().replaceAll(" ", "");
					GD_customerLastName.add(cusLName);
					
					String cusFName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[2]/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr["+(i+1)+"]/td[4]")).getText().replaceAll(" ", "");
					GD_customerFirstName.add(cusFName);
					
				}
				
				modifyDetails.setGD_cusTitle(GD_customerTitle);
				modifyDetails.setGD_cusFName(GD_customerFirstName);
				modifyDetails.setGD_cusLname(GD_customerLastName);
				
				
				
				
				///Stay Period
				
				driver.findElement(By.xpath(".//*[@id='StayPeriod_lkinid']")).click();
				Thread.sleep(1500);
				
				////////
				
				String SP_HotelName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]")).getText().split(": ")[1];
				modifyDetails.setSP_HotelName(SP_HotelName);
				
				String SP_checkInDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[2]")).getText().split(": ")[1];
				modifyDetails.setSP_checkInDate(SP_checkInDate);
				
				String SP_checkOutDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[2]")).getText().split(": ")[1];
				modifyDetails.setSP_checkOutDate(SP_checkOutDate);
				
				String SP_Rooms = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[4]")).getText().split(": ")[1];
				modifyDetails.setSP_Rooms(SP_Rooms);
				
				String SP_Adults = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]")).getText().split(": ")[1];
				modifyDetails.setSP_Adults(SP_Adults);
				
				String SP_Child = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[4]")).getText().split(": ")[1];
				modifyDetails.setSP_Child(SP_Child);
				
				String SP_Nights = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[2]")).getText().split(": ")[1];
				modifyDetails.setSP_Nights(SP_Nights);
				
				
				ArrayList<String> SP_roomTypeList = new ArrayList<String>();
				ArrayList<String> SP_bedSum_TypeList = new ArrayList<String>();
				ArrayList<String> SP_Sum_adultsList = new ArrayList<String>();
				ArrayList<String> SP_Sum_childrenList = new ArrayList<String>();
				ArrayList<ArrayList<String>> SP_dailySum_Rates = new ArrayList<ArrayList<String>>();
				ArrayList<String> SP_totalSum_Value = new ArrayList<String>();
				ArrayList<String> SP_daysCountSum = new ArrayList<String>();
				ArrayList<String> SP_guestName = new ArrayList<String>();
				
				
				for (int i = 1; i <= hotelRoomsCount; i++) {
					
					String roomType = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[1]")).getText().replaceAll(" ", "");
					SP_roomTypeList.add(roomType);
					
					String bedType = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[2]")).getText().replaceAll(" ", "");
					SP_bedSum_TypeList.add(bedType);
					
					String adultsL = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[3]")).getText().replaceAll(" ", "");
					SP_Sum_adultsList.add(adultsL);
					
					String childrenL = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[4]")).getText().replaceAll(" ", "");
					SP_Sum_childrenList.add(childrenL);
					
					WebElement searchTableEle = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[6]/table"));				
					List<WebElement> daysCountSer = searchTableEle.findElements(By.tagName("td"));
					int sizeDays = (daysCountSer.size() / 2);
					SP_daysCountSum.add(Integer.toString(sizeDays));
					
					ArrayList<String> dailyRatesLoop = new ArrayList<String>();
					WebElement searchratesEle = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[6]/table"));				
					List<WebElement> searchRate = searchratesEle.findElements(By.tagName("td"));
					
					for (int k = (sizeDays+1); k <= daysCountSer.size(); k++) {
						
						String oneDayRateSea = searchRate.get(k-1).getText().replaceAll(" ", "");
						dailyRatesLoop.add(oneDayRateSea);
						
					}
					SP_dailySum_Rates.add(dailyRatesLoop);
					
					String guestNameL = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[5]")).getText().replaceAll(" ", "");
					SP_guestName.add(guestNameL);
					
					String totalV = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[7]")).getText().replaceAll(" ", "");
					SP_totalSum_Value.add(totalV);
					
				}
				
				modifyDetails.setSP_roomTypeList(SP_roomTypeList);
				modifyDetails.setSP_OccupancyTypeList(SP_bedSum_TypeList);
				modifyDetails.setSP_AdultsList(SP_Sum_adultsList);
				modifyDetails.setSP_childrenList(SP_Sum_childrenList);
				modifyDetails.setSP_DaysCount(SP_daysCountSum);
				modifyDetails.setSP_DailyRates(SP_dailySum_Rates);
				modifyDetails.setSP_GuestName(SP_guestName);
				modifyDetails.setSP_Total(SP_totalSum_Value);
				
	
				String Sp_subTotal = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(hotelRoomsCount+2)+"]/td[3]")).getText();
				modifyDetails.setSP_TotalBookingValue(Sp_subTotal);
				
				String Sp_totalPayable = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[3]/tbody/tr/td/table[3]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(hotelRoomsCount+3)+"]/td[3]")).getText();
				modifyDetails.setSP_TotalCost(Sp_totalPayable);
				
				
				
				///change Add Rooms
				
				driver.findElement(By.xpath(".//*[@id='Change/AddRooms_lkinid']")).click();
				Thread.sleep(1500);
				
				////////
				
				String CAR_HotelName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]")).getText().split(": ")[1];
				modifyDetails.setCAR_HotelName(CAR_HotelName);
				
				String CAR_checkInDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[2]")).getText().split(": ")[1];
				modifyDetails.setCAR_checkInDate(CAR_checkInDate);
				
				String CAR_checkOutDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[2]")).getText().split(": ")[1];
				modifyDetails.setCAR_checkOutDate(CAR_checkOutDate);
				
				String CAR_Rooms = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[4]")).getText().split(": ")[1];
				modifyDetails.setCAR_Rooms(CAR_Rooms);
				
				String CAR_Adults = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]")).getText().split(": ")[1];
				modifyDetails.setCAR_Adults(CAR_Adults);
				
				String CAR_Child = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[4]")).getText().split(": ")[1];
				modifyDetails.setCAR_Child(CAR_Child);
				
				String CAR_Nights = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[2]")).getText().split(": ")[1];
				modifyDetails.setCAR_Nights(CAR_Nights);
				
				ArrayList<String> CAR_roomTypeList = new ArrayList<String>();
				ArrayList<String> CAR_bedSum_TypeList = new ArrayList<String>();
				ArrayList<String> CAR_Sum_adultsList = new ArrayList<String>();
				ArrayList<String> CAR_Sum_childrenList = new ArrayList<String>();
				ArrayList<ArrayList<String>> CAR_dailySum_Rates = new ArrayList<ArrayList<String>>();
				ArrayList<String> CAR_totalSum_Value = new ArrayList<String>();
				ArrayList<String> CAR_daysCountSum = new ArrayList<String>();
				ArrayList<String> CAR_guestName = new ArrayList<String>();
				
				for (int i = 1; i <= hotelRoomsCount; i++) {
					
					String roomType = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[1]")).getText().replaceAll(" ", "");
					CAR_roomTypeList.add(roomType);
					
					String bedType = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[2]")).getText().replaceAll(" ", "");
					CAR_bedSum_TypeList.add(bedType);
					
					String adultsL = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[3]")).getText().replaceAll(" ", "");
					CAR_Sum_adultsList.add(adultsL);
					
					String childrenL = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[4]")).getText().replaceAll(" ", "");
					CAR_Sum_childrenList.add(childrenL);
					
					WebElement searchTableEle = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[6]/table"));				
					List<WebElement> daysCountSer = searchTableEle.findElements(By.tagName("td"));
					int sizeDays = (daysCountSer.size() / 2);
					CAR_daysCountSum.add(Integer.toString(sizeDays));
					
					ArrayList<String> dailyRatesLoop = new ArrayList<String>();
					WebElement searchratesEle = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[6]/table"));				
					List<WebElement> searchRate = searchratesEle.findElements(By.tagName("td"));
					
					for (int k = (sizeDays+1); k <= daysCountSer.size(); k++) {
						
						String oneDayRateSea = searchRate.get(k-1).getText().replaceAll(" ", "");
						dailyRatesLoop.add(oneDayRateSea);
						
					}
					CAR_dailySum_Rates.add(dailyRatesLoop);
					
					String guestNameL = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[5]")).getText().replaceAll(" ", "");
					CAR_guestName.add(guestNameL);
					
					String totalV = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[7]")).getText().replaceAll(" ", "");
					CAR_totalSum_Value.add(totalV);
					
				}
				
				modifyDetails.setCAR_roomTypeList(CAR_roomTypeList);
				modifyDetails.setCAR_OccupancyTypeList(CAR_bedSum_TypeList);
				modifyDetails.setCAR_AdultsList(CAR_Sum_adultsList);
				modifyDetails.setCAR_childrenList(CAR_Sum_childrenList);
				modifyDetails.setCAR_DaysCount(CAR_daysCountSum);
				modifyDetails.setCAR_DailyRates(CAR_dailySum_Rates);
				modifyDetails.setCAR_GuestName(CAR_guestName);
				modifyDetails.setCAR_Total(CAR_totalSum_Value);
				
				
				String car_subTotal = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(hotelRoomsCount+2)+"]/td[3]")).getText();
				modifyDetails.setCAR_TotalBookingValue(car_subTotal);
				
				String car_totalPayable = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(hotelRoomsCount+3)+"]/td[3]")).getText();
				modifyDetails.setCAR_TotalCost(car_totalPayable);
				
				
				//Remove Rooms
				
				driver.findElement(By.xpath(".//*[@id='RemoveRooms_lkinid']")).click();
				Thread.sleep(1500);
				
				////////
				
				String RR_HotelName = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[2]")).getText().split(": ")[1];
				modifyDetails.setRR_HotelName(RR_HotelName);
				
				String RR_checkInDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[2]")).getText().split(": ")[1];
				modifyDetails.setRR_checkInDate(RR_checkInDate);
				
				String RR_checkOutDate = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[2]")).getText().split(": ")[1];
				modifyDetails.setRR_checkOutDate(RR_checkOutDate);
				
				String RR_Rooms = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[4]")).getText().split(": ")[1];
				modifyDetails.setRR_Rooms(RR_Rooms);
				
				String RR_Adults = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[4]")).getText().split(": ")[1];
				modifyDetails.setRR_Adults(RR_Adults);
				
				String RR_Child = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[3]/td[4]")).getText().split(": ")[1];
				modifyDetails.setRR_Child(RR_Child);
				
				String RR_Nights = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[4]/td[2]")).getText().split(": ")[1];
				modifyDetails.setRR_Nights(RR_Nights);
				
				ArrayList<String> RR_roomTypeList = new ArrayList<String>();
				ArrayList<String> RR_bedSum_TypeList = new ArrayList<String>();
				ArrayList<String> RR_Sum_adultsList = new ArrayList<String>();
				ArrayList<String> RR_Sum_childrenList = new ArrayList<String>();
				ArrayList<ArrayList<String>> RR_dailySum_Rates = new ArrayList<ArrayList<String>>();
				ArrayList<String> RR_totalSum_Value = new ArrayList<String>();
				ArrayList<String> RR_daysCountSum = new ArrayList<String>();
				ArrayList<String> RR_guestName = new ArrayList<String>();
				
				for (int i = 1; i <= hotelRoomsCount; i++) {
					
					String roomType = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[1]")).getText().replaceAll(" ", "");
					RR_roomTypeList.add(roomType);
					
					String bedType = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[2]")).getText().replaceAll(" ", "");
					RR_bedSum_TypeList.add(bedType);
					
					String adultsL = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[3]")).getText().replaceAll(" ", "");
					RR_Sum_adultsList.add(adultsL);
					
					String childrenL = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[4]")).getText().replaceAll(" ", "");
					RR_Sum_childrenList.add(childrenL);
					
					WebElement searchTableEle = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[6]/table"));				
					List<WebElement> daysCountSer = searchTableEle.findElements(By.tagName("td"));
					int sizeDays = (daysCountSer.size() / 2);
					RR_daysCountSum.add(Integer.toString(sizeDays));
					
					ArrayList<String> dailyRatesLoop = new ArrayList<String>();
					WebElement searchratesEle = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[6]/table"));				
					List<WebElement> searchRate = searchratesEle.findElements(By.tagName("td"));
					
					for (int k = (sizeDays+1); k <= daysCountSer.size(); k++) {
						
						String oneDayRateSea = searchRate.get(k-1).getText().replaceAll(" ", "");
						dailyRatesLoop.add(oneDayRateSea);
						
					}
					RR_dailySum_Rates.add(dailyRatesLoop);
					
					String guestNameL = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[5]")).getText().replaceAll(" ", "");
					RR_guestName.add(guestNameL);
					
					String totalV = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(i+1)+"]/td[7]")).getText().replaceAll(" ", "");
					RR_totalSum_Value.add(totalV);
					
				}
	
	
				modifyDetails.setRR_roomTypeList(RR_roomTypeList);
				modifyDetails.setRR_OccupancyTypeList(RR_bedSum_TypeList);
				modifyDetails.setRR_AdultsList(RR_Sum_adultsList);
				modifyDetails.setRR_childrenList(RR_Sum_childrenList);
				modifyDetails.setRR_DaysCount(RR_daysCountSum);
				modifyDetails.setRR_DailyRates(RR_dailySum_Rates);
				modifyDetails.setRR_GuestName(RR_guestName);
				modifyDetails.setRR_Total(RR_totalSum_Value);
	
				
				String rrr_subTotal = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(hotelRoomsCount+2)+"]/td[3]")).getText();
				modifyDetails.setRR_TotalBookingValue(rrr_subTotal);
				
				String rrr_totalPayable = driver.findElement(By.xpath(".//*[@id='scrollerAllID']/form/table[2]/tbody/tr/td/table[4]/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td/table/tbody/tr["+(hotelRoomsCount+3)+"]/td[3]")).getText();
				modifyDetails.setRR_TotalCost(rrr_totalPayable);
				
				
			}else{
			
				modifyDetails.setModificationReportLoaded(false);
			}
				
		} catch (Exception e) {
			modifyDetails.setModificationReportLoaded(false);
		}
		
		return modifyDetails;
	}

}
