package com.reader;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.model.HotelDetails;
import com.model.Promotion;
import com.model.Rates;
import com.model.SearchInfo;

public class InternalLogics {
	
	private SearchInfo searchInfo;
	private HotelDetails hotelDetails;
	private double netRate; 
	private double sellRate;
	private double costRate;
	
	public void addHotel(SearchInfo searchInfo){
		
		this.searchInfo = searchInfo;
	}
	
	public HotelDetails dailyRateCalculation(){
			
		hotelDetails = new HotelDetails();
		ArrayList<String> ratesroom = new ArrayList<String>();
		ArrayList<String> costRoom = new ArrayList<String>();
		ArrayList<String> netRateList = new ArrayList<String>();
		ArrayList<String> ratesroomCancel = new ArrayList<String>();
		
		ArrayList<Rates> ratesInfoList = searchInfo.getRatesInfo();
		
		for (Rates rates : ratesInfoList) {
			
			netRate = Double.parseDouble(rates.getNetRate());
			double pm = Double.parseDouble(rates.getProfitMarkup());
			
			if (searchInfo.getIsPromoApply().equalsIgnoreCase("Yes")) {
				
				String dateFromToCal = searchInfo.getDateFrom();
				String dateToCal = searchInfo.getDateTo();
				SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
				
				Date d1 = null;
				Date d2 = null;
				int diffDays = 0;
				
				try {	
					
					d1 = format.parse(dateFromToCal);
					d2 = format.parse(dateToCal);
		 
					//in milliseconds
					long diffD = d2.getTime() - d1.getTime();
					diffDays = (int)(diffD / (24 * 60 * 60 * 1000));
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
				ArrayList<Promotion> promotionList = searchInfo.getPromoInfo();
				
				for (Promotion promotion : promotionList) {
					
					int nightsRequired = Integer.parseInt(promotion.getDiscount_nights());
					double discountValue = Double.parseDouble(promotion.getDiscount_value());
					
					if (diffDays >= nightsRequired) {
						
						if (promotion.getDiscount_Type().equalsIgnoreCase("percentage")) {
											
							netRate = netRate * ((100 - discountValue)/100); 	
						}
					
						if (promotion.getDiscount_value().equalsIgnoreCase("value")) {
							
							netRate = netRate - discountValue;			
						}
					}
				}	
			}
			
			
			if(rates.getSalesTax().equalsIgnoreCase("N/A")){
				sellRate = netRate;
			}else{
				double salesTax = Double.parseDouble(rates.getSalesTax());
				sellRate = (netRate* ((100 + salesTax) / 100));	
			}
			
			double energyTax = Double.parseDouble(rates.getEnergyTax());
			double occupancyTax =	Double.parseDouble(rates.getOccupancyTax());
			double misFees = Double.parseDouble(rates.getMisTax());
			
			sellRate = sellRate + energyTax + occupancyTax + misFees;
			costRate = sellRate;
			
			sellRate = (sellRate* ((100 + pm) / 100));	
			DecimalFormat df = new DecimalFormat("#.0#"); 
			DecimalFormat df1 = new DecimalFormat("#.00"); 
			
			String ratesString = df.format(Math.ceil(sellRate));
			String costrate = df.format(costRate);
			String net = df.format(netRate);
			String ratesroomCancels = df1.format(Math.ceil(sellRate));
		
			netRateList.add(net);	
			costRoom.add(costrate);
			ratesroom.add(ratesString);
			ratesroomCancel.add(ratesroomCancels);
			
		}
		
		hotelDetails.setDailyRatesList(ratesroom);
		hotelDetails.setDailyRatesCostList(costRoom);
		hotelDetails.setDailyNetRateList(netRateList);
		hotelDetails.setDailyRatesCancelList(ratesroomCancel);
		
		
		return hotelDetails;
	}
	
	

}
