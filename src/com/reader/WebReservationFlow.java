package com.reader;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;
import com.controller.PG_Properties;
import com.model.*;
import java.util.TimeZone;

public class WebReservationFlow {

	private WebDriver driver = null;
	private int adultCountFromexcel;
	private int childCountFromexcel;
	private List<String> excelRT;
	private List<String> excelOccuRate;
	private int excelRoomCount;
	private HotelDetails hotelDetails;
	private SearchInfo searchInfo;
	private RoomTypes roomTypes;
	private List<String> adultList;	
	private List<String> childList;	
	private List<String> childAgeList;	
	private String excelHotelName;
	private String currentDate; 
	private String toType, checkInDate;
	private int excelNights;
	private int count_nights;
	private int count_rooms;
	private int count_adults;
	private int count_children;
	private long searchButtClick;
	private long time_resultsAvailble;
	private double sellRate;
	private double costRate;
	
	public WebReservationFlow(WebDriver Driver){
		
		this.driver = Driver;
	}
	
	public void addHotel(SearchInfo searchInfo){
		
		this.searchInfo = searchInfo;
	}
	
	
	public WebDriver searchEngine(WebDriver webdriver) throws InterruptedException, ParseException{
		
		WebDriverWait wait = new WebDriverWait(driver, 120);
		
		driver.get(PG_Properties.getProperty("SearchLink"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cmbCtry")));
		
		driver.findElement(By.xpath(".//*[@id='ResSearchForm']/div/div[1]/table/tbody/tr[2]/td[2]/input")).clear();
		driver.findElement(By.xpath(".//*[@id='ResSearchForm']/div/div[1]/table/tbody/tr[2]/td[2]/input")).sendKeys(PG_Properties.getProperty("Web.UserName"));
		driver.findElement(By.xpath(".//*[@id='ResSearchForm']/div/div[1]/table/tbody/tr[3]/td[2]/input")).clear();
		driver.findElement(By.xpath(".//*[@id='ResSearchForm']/div/div[1]/table/tbody/tr[3]/td[2]/input")).sendKeys(PG_Properties.getProperty("Web.Password"));
		
		new Select(driver.findElement(By.name("cmbCtry"))).selectByVisibleText(searchInfo.getCountry());
		new Select(driver.findElement(By.name("cmbCty"))).selectByVisibleText(searchInfo.getStateCity());

		String searchFDate = searchInfo.getDateFrom();
		Date dateObj = new SimpleDateFormat("dd-MMM-yyyy").parse(searchFDate);
		String newstring = new SimpleDateFormat("d-MMM-yyyy").format(dateObj);	
		String[] partsCF = newstring.split("-");
		String ConFDay = partsCF[0]; 
		String ConFMonth = partsCF[1]; 
		String ConFYear = partsCF[2];
		
		new Select(driver.findElement(By.name("cmbInMn"))).selectByVisibleText(ConFMonth);
		new Select(driver.findElement(By.name("cmbInDt"))).selectByVisibleText(ConFDay);
		new Select(driver.findElement(By.name("cmbInYr"))).selectByVisibleText(ConFYear);
		
		adultList = new ArrayList<String>();
		childList = new ArrayList<String>();
		childAgeList = new ArrayList<String>();
		
		int roomCount = Integer.parseInt(searchInfo.getRooms());
		new Select(driver.findElement(By.name("cmbNgt"))).selectByVisibleText(searchInfo.getNights());
		new Select(driver.findElement(By.name("cmbNoRm"))).selectByVisibleText(searchInfo.getRooms());
		
		
		if (searchInfo.getAdults().contains(";")) {
			String adu = searchInfo.getAdults();
			String[] partsAdults = adu.split(";");
			for (String valueAdu : partsAdults) {
				adultList.add(valueAdu);
			}
		} else {
			String adu = searchInfo.getAdults();
			adultList.add(adu);
		}
		
		if (searchInfo.getChildren().contains(";")) {
			String chi = searchInfo.getChildren();
			String[] partschilds = chi.split(";");
			for (String valuechi : partschilds) {
				childList.add(valuechi);
			}
		} else {
			String chi = searchInfo.getChildren();
			childList.add(chi);
		}
		
		int b = 1;
		int c = 1;
		
		for (int j = 0; j < adultList.size(); j++) {
			
			if(adultList.get(j) != "0"){
				
				new Select(driver.findElement(By.name("cmbAdlt_R"+b+""))).selectByVisibleText(adultList.get(j));
				b++;
			}
		}
			
		for (int a = 0; a < childList.size(); a++) {
			
			if (childList.get(a) != "0") {
				
				new Select(driver.findElement(By.name("cmbChi_R"+c+""))).selectByVisibleText(childList.get(a));	
				c++;
			}	
		}
			
	/*
		if (searchInfo.getChild_ages().contains(";")) {
			
			String chiAgeSemi = searchInfo.getChild_ages();
			String[] partschildSemi = chiAgeSemi.split(";");
		
			for(String starlist : partschildSemi){				
				if (starlist.contains("#")) {
					String[] partsstartList = starlist.split("#");

						for(String starP : partsstartList){		
							childAgeList.add(starP);
						}					
										
				} else {
					childAgeList.add(starlist);
				}
			}					
		}
		
		else if (searchInfo.getChild_ages().contains("#")) {
			
			String chiAgeSemi = searchInfo.getChild_ages();
			String[] partschildSemi = chiAgeSemi.split("#");
			for(String starP : partschildSemi){
				childAgeList.add(starP);
			}	
			
		}
		else{
			String childAge = searchInfo.getChild_ages();
			childAgeList.add(childAge);
		
		}
		System.out.println(childAgeList);
		System.out.println(childAgeList);
		System.out.println(childAgeList);
		
		
		for (int i = 1; i < Integer.parseInt(searchInfo.getRooms()); i++) {
			
			int NOC = Integer.parseInt(childList.get(i-1));
            //String[] CAges = CurrentSearch.getChildAges().get(i);
            
            if(NOC != 0){
            
              if(NOC == childAgeList.size()){
            	  	for (int k = 0; k < childAgeList.size(); k++) {
          				new Select(driver.findElement(By.id("ChiAgeR"+i+""+k+"_id"))).selectByVisibleText(childAgeList.get(k));
  					}
              }else
            	 System.out.println("Problem with child ages room "+i);
        	   }
			
		}
		
		*/
		
		
		WebElement roomCEle = driver.findElement(By.name("cmbNoRm"));
		List<WebElement> roomCEleList = roomCEle.findElements(By.tagName("option"));
		count_rooms = roomCEleList.size();
		
		WebElement nightsCEle = driver.findElement(By.name("cmbNgt"));
		List<WebElement> nightsCEleList = nightsCEle.findElements(By.tagName("option"));
		count_nights = nightsCEleList.size();
		
		WebElement adultsCEle = driver.findElement(By.name("cmbAdlt_R1"));
		List<WebElement> adultsCEleList = adultsCEle.findElements(By.tagName("option"));
		count_adults = adultsCEleList.size();
		
		WebElement childCEle = driver.findElement(By.name("cmbChi_R1"));
		List<WebElement> childCEleList = childCEle.findElements(By.tagName("option"));
		count_children = childCEleList.size()-1;
		
		excelHotelName = searchInfo.getExcelHotelName();
		excelRoomCount = Integer.parseInt(searchInfo.getRooms());
		excelNights = Integer.parseInt(searchInfo.getNights());
		toType = searchInfo.getTO_type();
		checkInDate = searchInfo.getDateFrom();
		
		excelRT = new ArrayList<String>();
		String excelRoomType = searchInfo.getExcelRoomType();
		
		if (excelRoomType.contains("#")) {
			String[] partsEA = excelRoomType.split("#");
			for (String values : partsEA) {
				excelRT.add(values);
			}
		} else {
			excelRT.add(excelRoomType);
		}
		
		excelOccuRate = new ArrayList<String>();
		String excelOccuRateTypes = searchInfo.getExcelOccupancyratePlan();
		
		if (excelOccuRateTypes.contains("#")) {
			String[] partsEART = excelOccuRateTypes.split("#");
			for (String valueT : partsEART) {
				excelOccuRate.add(valueT);
			}
		} else {
			excelOccuRate.add(excelOccuRateTypes);
		}
		
		
		
		driver.findElement(By.xpath(".//*[@id='ResSearchForm']/div/div[6]/input")).click();
		searchButtClick = (System.currentTimeMillis() / 1000);
		
		
		
		return driver;
	}

	public HotelDetails availableResults(HotelDetails hDetail, WebDriver webdriver) throws InterruptedException{ 
				
		WebDriverWait wait = new WebDriverWait(driver, 120);
		this.hotelDetails = hDetail;
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("availOnly")));
		time_resultsAvailble = (System.currentTimeMillis() / 1000);
		hotelDetails.setTime_searchButtClick(searchButtClick);			
		hotelDetails.setTime_resultsAvailble(time_resultsAvailble);
		
		Calendar cal1 = Calendar.getInstance(TimeZone.getTimeZone("US/Mountain"), Locale.US);
		System.out.println(cal1.getTime());
		SimpleDateFormat formatToDate = new SimpleDateFormat("dd-MMM-yyyy");
		currentDate = formatToDate.format(cal1.getTime());
		
		hotelDetails.setExcelHotelName(excelHotelName);
		hotelDetails.setReservationDate(currentDate);		
		hotelDetails.setCount_adults(count_adults);
		hotelDetails.setCount_children(count_children);
		hotelDetails.setCount_nights(count_nights);
		hotelDetails.setCount_rooms(count_rooms);
		
		Thread.sleep(500);
		int hotelCount = Integer.parseInt(driver.findElement(By.xpath(".//*[@id='mainBody']/form/table[1]/tbody/tr[2]/td[2]/div[1]/span[1]")).getText());
		
		int wholepages = (hotelCount/10);
		
		if(((hotelCount)%10) == 0){				
			System.out.println("Page count - " + wholepages);
		}else{
			wholepages ++;
			System.out.println("Page count - " + wholepages);
		}
		
		String results_CheckInDate = driver.findElement(By.xpath(".//*[@id='mainBody']/form/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td")).getText();
		hotelDetails.setResults_CheckInDate(results_CheckInDate);
		
		String results_CheckOutDate = driver.findElement(By.xpath(".//*[@id='mainBody']/form/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[2]/td")).getText();
		hotelDetails.setResults_CheckOutDate(results_CheckOutDate);
		
		String results_Nights = driver.findElement(By.xpath(".//*[@id='mainBody']/form/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[3]/td")).getText();
		hotelDetails.setResults_Nights(results_Nights);
		
		String results_Rooms = driver.findElement(By.xpath(".//*[@id='mainBody']/form/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[4]/td")).getText();
		hotelDetails.setResults_Rooms(results_Rooms);
		
		String results_Adults = driver.findElement(By.xpath(".//*[@id='mainBody']/form/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[5]/td")).getText();
		hotelDetails.setResults_Adults(results_Adults);
		
		String results_children  = driver.findElement(By.xpath(".//*[@id='mainBody']/form/table[1]/tbody/tr[2]/td[2]/table/tbody/tr[6]/td")).getText();
		hotelDetails.setResults_children(results_children);
		
		
		if (driver.findElements(By.id("table_0")).size() != 0) {
			
			//Hotel results Loop
			final short HOTEL_PER_PAGE=10;
			outerloop : for (int hotelPage = 1; hotelPage <= wholepages ; hotelPage++) {
				for (int i = 0 ; i < HOTEL_PER_PAGE; i++) {
			
					if ( ((hotelPage-1)*HOTEL_PER_PAGE) + (i) > hotelCount) {
						
						break outerloop;
						
					} else {
						
						String webHotelName = driver.findElement(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[1]/td[2]")).getText().replaceAll(" ", "");
						hotelDetails.setResults_hotelName(webHotelName);
												
						if(excelHotelName.replaceAll(" ", "").equalsIgnoreCase(webHotelName)){
							
							WebElement hotNameEle = driver.findElement(By.id("table_"+i+""));
							String hotelDescription = hotNameEle.findElement(By.className("itemDesc")).getText();
							hotelDetails.setHoteldescription(hotelDescription);
							
							if (driver.findElements(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/div[2]/ul/li[1]/a")).size() == 0) {
								hotelDetails.setCancellationLinkAvailable(false);
							}
							if (driver.findElements(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/div[2]/ul/li[2]/a")).size() == 0) {
								hotelDetails.setRestrictionLinkAvailable(false);
							}
							if (driver.findElements(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/div[2]/ul/li[3]/a")).size() == 0) {
								hotelDetails.setHotelInfoLinkAvailable(false);
							}
							if (driver.findElements(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/div[2]/ul/li[4]/a")).size() == 0) {
								hotelDetails.setSpecialsLinkAvailable(false);
							}
							
							//Open a new window
							String parentHandle = driver.getWindowHandle();
						    driver.findElement(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/div[2]/ul/li[3]/a")).click();
						    
						    for (String winHandle : driver.getWindowHandles()) {
						        driver.switchTo().window(winHandle); 
						    }
						    
						    Thread.sleep(2000);
						    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/table[3]/tbody/tr/td/table/tbody/tr/td[2]/table[2]/tbody/tr/td[1]")));
						    String newWindow_HotelName = driver.findElement(By.xpath("html/body/table[3]/tbody/tr/td/table/tbody/tr/td[2]/table[2]/tbody/tr/td[1]")).getText();
						    hotelDetails.setNewWindow_HotelName(newWindow_HotelName);
						    
							String newWindow_Address = driver.findElement(By.xpath("html/body/table[3]/tbody/tr/td/table/tbody/tr/td[2]/table[3]/tbody/tr/td[1]/table/tbody/tr[1]/td")).getText();
						    
							if (newWindow_Address.contains("\n")) {
								hotelDetails.setNewWindow_Address(newWindow_Address.replaceAll("\n", ""));
							} else {
								hotelDetails.setNewWindow_Address(newWindow_Address);
							}
								
							Thread.sleep(1000);
						    driver.close(); // close newly opened window when done with it
						    driver.switchTo().window(parentHandle); // switch back to the original window
							
						    Thread.sleep(1000);
						    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/div[1]/strong")));
						    
							String hotelAddress = driver.findElement(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/div[1]/strong")).getText().replaceAll(" ", "");
							hotelDetails.setResults_hotelAddress(hotelAddress);
							
							for (int r = 1;  r <= excelRoomCount ; r++) {
								
								if(driver.findElements(By.id("showMoreRooms_"+i+"_"+r+"") ).size() != 0){
									driver.findElement(By.xpath(".//*[@id='showMoreRooms_"+i+"_"+r+"']/td/img")).click();							
								}
								
								List<WebElement> roomEle = hotNameEle.findElements(By.name("HT"+(i+1)+"_RT"+r+""));
								
								for (int j = 1; j <= roomEle.size(); j++) {
									
									if(4>j){
										
										WebElement webRTEle = driver.findElement(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/table["+(r+1)+"]/tbody/tr["+(j*2)+"]/td[1]"));
										String webRT = webRTEle.findElement(By.tagName("label")).getText().replaceAll(" ", "");
										
										String webOccuRatePlan = driver.findElement(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/table["+(r+1)+"]/tbody/tr["+(j*2)+"]/td[2]")).getText().replaceAll(" ", "");
									
										if(excelRT.get(r-1).replaceAll(" ", "").equalsIgnoreCase(webRT) && excelOccuRate.get(r-1).replaceAll(" ", "").equalsIgnoreCase(webOccuRatePlan)){
											
											roomTypes = new RoomTypes();
											roomTypes.setResults_RoomType(webRT);
											roomTypes.setResults_Occupancy(webOccuRatePlan.split("/")[0]);
											roomTypes.setResults_RatePlan(webOccuRatePlan.split("/")[1]);
											
											WebElement searchTableEle = driver.findElement(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/table["+(r+1)+"]/tbody/tr["+(j*2)+"]/td[3]/table"));				
											List<WebElement> daysCountSer = searchTableEle.findElements(By.tagName("th"));
											roomTypes.setResults_AvalabilityDates(Integer.toString(daysCountSer.size()));
											
											WebElement searchratesEle = driver.findElement(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/table["+(r+1)+"]/tbody/tr["+(j*2)+"]/td[3]/table"));				
											List<WebElement> searchRate = searchratesEle.findElements(By.tagName("td"));
											ArrayList<String> dailyRates = new ArrayList<String>();
											
											for (int k = 0; k < excelNights; k++) {
												
												String oneDayRateSea = searchRate.get(k).getText();
												dailyRates.add(oneDayRateSea);
												
											}
											roomTypes.setResults_DailyRates(dailyRates);
											
											String results_listTotal = driver.findElement(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/table["+(r+1)+"]/tbody/tr["+(j*2)+"]/td[4]")).getText();
											roomTypes.setResults_listTotal(results_listTotal);
											
											hotelDetails.addRoomTypes(roomTypes);
											
											driver.findElement(By.xpath(".//*[@id='rd_rm"+i+"_"+r+"_"+(j-1)+"']")).click();		
											
										}
										
									}
		
									if(4<=j){
										
										WebElement hidHotEle = driver.findElement(By.xpath(".//*[@id='hotellmore_rm"+i+"_"+r+"_"+(j-1)+"']/td[1]"));
										String hidRT = hidHotEle.findElement(By.tagName("label")).getText();
										
										String hidOccuratePlan = driver.findElement(By.xpath(".//*[@id='hotellmore_rm"+i+"_"+r+"_"+(j-1)+"']/td[2]")).getText().replaceAll(" ", "");
										
										if(excelRT.get(r-1).replaceAll(" ", "").equalsIgnoreCase(hidRT) && excelOccuRate.get(r-1).replaceAll(" ", "").equalsIgnoreCase(hidOccuratePlan)){
											
											roomTypes = new RoomTypes();
											roomTypes.setResults_RoomType(hidRT);
											roomTypes.setResults_Occupancy(hidOccuratePlan.split("/")[0]);
											roomTypes.setResults_RatePlan(hidOccuratePlan.split("/")[1]);
											
											WebElement hidSearchTableEle = driver.findElement(By.xpath(".//*[@id='hotellmore_rm"+i+"_"+r+"_"+(j-1)+"']/td[3]/table"));				
											List<WebElement> hiddaysCountSer = hidSearchTableEle.findElements(By.tagName("th"));
											roomTypes.setResults_AvalabilityDates(Integer.toString(hiddaysCountSer.size()));
											
											WebElement hidSearchratesEle = driver.findElement(By.xpath(".//*[@id='hotellmore_rm"+i+"_"+r+"_"+(j-1)+"']/td[3]/table"));				
											List<WebElement> hidSearchRate = hidSearchratesEle.findElements(By.tagName("td"));
											ArrayList<String> dailyRates = new ArrayList<String>();
											
											for (int k = 0; k < excelNights; k++) {
												
												String oneDayRateSea = hidSearchRate.get(k).getText();
												dailyRates.add(oneDayRateSea);
												
											}
											roomTypes.setResults_DailyRates(dailyRates);
											
											String hidResults_listTotal = driver.findElement(By.xpath(".//*[@id='hotellmore_rm"+i+"_"+r+"_"+(j-1)+"']/td[4]")).getText();
											roomTypes.setResults_listTotal(hidResults_listTotal);
											
											hotelDetails.addRoomTypes(roomTypes);
											
											driver.findElement(By.xpath(".//*[@id='rd_hotellmore_rm"+i+"_"+r+"_"+(j-1)+"']")).click();		
											
										}
									}							
								}	
							}
							
							String results_TotalRate = driver.findElement(By.id("HT_"+i+"")).getText();
							hotelDetails.setResults_TotalRate(results_TotalRate);	
							
							Random ran = new Random();
							int x = ran.nextInt(100) + 5;
							
							if(x % 2 == 0 ){
								hotelDetails.setIsCheckOut("true");
							}else{
								hotelDetails.setIsCheckOut("true");
							}
							
							if (hotelDetails.getIsCheckOut().equals("true")) {
								
								driver.findElement(By.xpath(".//*[@id='table_"+i+"']/tbody/tr[2]/td[2]/table["+(excelRoomCount+2)+"]/tbody/tr[2]/td[2]/div/a[2]/img")).click();
								long time_addtoCartButtClick = (System.currentTimeMillis() / 1000);	
								hotelDetails.setTime_addtoCartButtClick(time_addtoCartButtClick);
								
								wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cartDetails")));	
								long time_addedToCart  = (System.currentTimeMillis() / 1000);	
								hotelDetails.setTime_addedToCart(time_addedToCart);
								
								break outerloop;
																
							} else {
								
								
								
							}		
						}	
					}
				}
				
				driver.findElement(By.xpath(".//*[@id='pagingStyle']/table/tbody/tr/td[5]/img")).click();
				Thread.sleep(2000);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("table_0")));	
				
			}
			
			//Payments page
			
			String paymentShopCart_hotelName = driver.findElement(By.xpath(".//*[@id='cartItemTable']/tbody/tr[2]/td[2]/strong")).getText();
			hotelDetails.setPaymentShopCart_hotelName(paymentShopCart_hotelName);		
			String paymentShopCart_qty = driver.findElement(By.xpath(".//*[@id='cartItemTable']/tbody/tr[2]/td[4]/strong")).getText();
			hotelDetails.setPaymentShopCart_qty(paymentShopCart_qty);		
			String paymentShopCart_dateFrom = driver.findElement(By.xpath(".//*[@id='cartItemTable']/tbody/tr[2]/td[5]/strong")).getText();
			hotelDetails.setPaymentShopCart_dateFrom(paymentShopCart_dateFrom);		
			String paymentShopCart_dateTo = driver.findElement(By.xpath(".//*[@id='cartItemTable']/tbody/tr[2]/td[6]/strong")).getText();
			hotelDetails.setPaymentShopCart_dateTo(paymentShopCart_dateTo);		
			String paymentShopCart_amount = driver.findElement(By.xpath(".//*[@id='cartItemTable']/tbody/tr[2]/td[7]/strong")).getText();
			hotelDetails.setPaymentShopCart_amount(paymentShopCart_amount);	
			
			String paymentShopCart_CartTotal = driver.findElement(By.xpath(".//*[@id='shoppingCartSummery']/tbody/tr[2]/td[2]/table[2]/tbody/tr[2]/td[2]/div")).getText();
			hotelDetails.setPaymentShopCart_CartTotal(paymentShopCart_CartTotal);
			
			
			String paymentTravelDetails_checkIn = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[1]/tbody/tr[1]/td")).getText();
			hotelDetails.setPaymentTravelDetails_checkIn(paymentTravelDetails_checkIn);		
			String paymentTravelDetails_checkOut = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td")).getText();
			hotelDetails.setPaymentTravelDetails_checkOut(paymentTravelDetails_checkOut);		
			String paymentTravelDetails_Nights = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[1]/tbody/tr[3]/td")).getText();
			hotelDetails.setPaymentTravelDetails_Nights(paymentTravelDetails_Nights);		
			String paymentTravelDetails_Rooms = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[1]/tbody/tr[4]/td")).getText();
			hotelDetails.setPaymentTravelDetails_Rooms(paymentTravelDetails_Rooms);		
			String paymentTravelDetails_Adults = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[1]/tbody/tr[5]/td")).getText();
			hotelDetails.setPaymentTravelDetails_Adults(paymentTravelDetails_Adults);		
			String paymentTravelDetails_Children = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[1]/tbody/tr[6]/td")).getText();
			hotelDetails.setPaymentTravelDetails_Children(paymentTravelDetails_Children);		
			String paymentTravelDetails_HotelName = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[2]/tbody/tr[1]/td")).getText();
			hotelDetails.setPaymentTravelDetails_HotelName(paymentTravelDetails_HotelName);	
			String paymentTravelDetails_Address = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[2]/tbody/tr[2]/td")).getText().replaceAll(" ", "");
			hotelDetails.setPaymentTravelDetails_Address(paymentTravelDetails_Address);	
			String paymentTravelDetails_city = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[2]/tbody/tr[3]/td")).getText();
			hotelDetails.setPaymentTravelDetails_city(paymentTravelDetails_city);
			String paymentTravelDetails_state = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[2]/tbody/tr[4]/td")).getText();
			hotelDetails.setPaymentTravelDetails_state(paymentTravelDetails_state);
			String paymentTravelDetails_country = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[2]/tbody/tr[5]/td")).getText();
			hotelDetails.setPaymentTravelDetails_country(paymentTravelDetails_country);
			
			ArrayList<String> paymentTravelDetails_RoomAdultsA = new ArrayList<String>(); 
			ArrayList<String> paymentTravelDetails_RoomChildrenA = new ArrayList<String>();
			ArrayList<String> paymentTravelDetails_RoomTypeA = new ArrayList<String>();
			ArrayList<String> paymentTravelDetails_RoomOccupancyA = new ArrayList<String>();
			ArrayList<String> paymentTravelDetails_RoomRatePlanA = new ArrayList<String>();
			ArrayList<String> paymentTravelDetails_RoomDaysCountA = new ArrayList<String>();		
			ArrayList<String> paymentTravelDetails_RoomtotalA = new ArrayList<String>();
			ArrayList<ArrayList<String>> paymentTravelDetails_RoomDailyRatesA = new ArrayList<ArrayList<String>>();
			
			for (int i = 1; i <= excelRoomCount; i++) {
				
				String paymentTravelDetails_RoomAdults = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[4]/tbody/tr["+(i+1)+"]/td[1]")).getText().split("\n")[1].split(": ")[1];
				paymentTravelDetails_RoomAdultsA.add(paymentTravelDetails_RoomAdults);
				
				String paymentTravelDetails_RoomChildren = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[4]/tbody/tr["+(i+1)+"]/td[1]")).getText().split("\n")[2].split(": ")[1];
				paymentTravelDetails_RoomChildrenA.add(paymentTravelDetails_RoomChildren);
				
				String paymentTravelDetails_RoomType = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[4]/tbody/tr["+(i+1)+"]/td[2]")).getText().split("\n")[0];
				paymentTravelDetails_RoomTypeA.add(paymentTravelDetails_RoomType);
				
				String paymentTravelDetails_RoomOccupancy = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[4]/tbody/tr["+(i+1)+"]/td[3]")).getText().split("/")[0];
				paymentTravelDetails_RoomOccupancyA.add(paymentTravelDetails_RoomOccupancy);
				
				String paymentTravelDetails_RoomRatePlan = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[4]/tbody/tr["+(i+1)+"]/td[3]")).getText().split("/")[1];
				paymentTravelDetails_RoomRatePlanA.add(paymentTravelDetails_RoomRatePlan);
								
				WebElement roomTablePaymentsEle = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[4]/tbody/tr["+(i+1)+"]/td[4]/table"));				
				List<WebElement> daysCountElement = roomTablePaymentsEle.findElements(By.tagName("th"));
				paymentTravelDetails_RoomDaysCountA.add(Integer.toString(daysCountElement.size()));
			
				WebElement roomratesPaymentsEle = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[4]/tbody/tr["+(i+1)+"]/td[4]/table"));				
				List<WebElement> ratesElement = roomratesPaymentsEle.findElements(By.tagName("td"));
				
				ArrayList<String> dailyRatesPayment = new ArrayList<String>();
				for (int k = 0; k < excelNights; k++) {
					
					String oneDayRateSeaP = ratesElement.get(k).getText();
					dailyRatesPayment.add(oneDayRateSeaP);
					
				}
				
				paymentTravelDetails_RoomDailyRatesA.add(dailyRatesPayment);
				
				String paymentTravelDetails_Roomtotal = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[4]/tbody/tr["+(i+1)+"]/td[5]")).getText();
				paymentTravelDetails_RoomtotalA.add(paymentTravelDetails_Roomtotal);
				
			}
			
			hotelDetails.setPaymentTravelDetails_RoomAdults(paymentTravelDetails_RoomAdultsA);
			hotelDetails.setPaymentTravelDetails_RoomChildren(paymentTravelDetails_RoomChildrenA);
			hotelDetails.setPaymentTravelDetails_RoomType(paymentTravelDetails_RoomTypeA);
			hotelDetails.setPaymentTravelDetails_RoomOccupancy(paymentTravelDetails_RoomOccupancyA);
			hotelDetails.setPaymentTravelDetails_RoomRatePlan(paymentTravelDetails_RoomRatePlanA);
			hotelDetails.setPaymentTravelDetails_RoomDaysCount(paymentTravelDetails_RoomDaysCountA);
			hotelDetails.setPaymentTravelDetails_RoomDailyRates(paymentTravelDetails_RoomDailyRatesA);
			hotelDetails.setPaymentTravelDetails_Roomtotal(paymentTravelDetails_RoomtotalA);
			
			String paymentTravelDetails_RoomBookingTotal = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[5]/tbody/tr[2]/td[2]")).getText().split(": ")[1];
			hotelDetails.setPaymentTravelDetails_RoomBookingTotal(paymentTravelDetails_RoomBookingTotal);
			
			
			
			/////
			Random rand = new Random(); 
			int randNumber = rand.nextInt(100000) + 5;
			
			WebElement tdEle = driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[6]/tbody/tr[2]/td/table"));	
			String guestId = tdEle.findElement(By.tagName("tbody")).getAttribute("id");
			
			StringBuffer sb = new StringBuffer("guest");
			String[] gh = guestId.split("_");
			sb.append("_");
			sb.append(gh[1]);
			sb.append("_");
			sb.append(gh[2]);
			sb.append("_");
			
			for (int i = 0; i < excelRoomCount; i++) {
				
				String gId = sb.toString()+ i;
				new Select(driver.findElement(By.xpath(".//*[@id='"+gId+"']/tr/td/table/tbody/tr/td[2]/select"))).selectByVisibleText("Mr.");
				driver.findElement(By.xpath(".//*[@id='"+gId+"']/tr/td/table/tbody/tr/td[3]/input")).sendKeys("TestLast"+randNumber+"Room"+(i+1)+""); 
				driver.findElement(By.xpath(".//*[@id='"+gId+"']/tr/td/table/tbody/tr/td[4]/input")).sendKeys("TestFirst"+randNumber+"Room"+(i+1)+""); 	
			}
			
			Thread.sleep(1000);
			ArrayList<String> customerTitle = new ArrayList<String>();
			ArrayList<String> customerLastName = new ArrayList<String>();
			ArrayList<String> customerFirstName = new ArrayList<String>();
			
			for (int i = 0; i < excelRoomCount; i++) {
				
				String gId = sb.toString() + i;
				Select titleCustomer = new org.openqa.selenium.support.ui.Select(driver.findElement(By.xpath(".//*[@id='"+gId+"']/tr/td/table/tbody/tr/td[2]/select")));
				WebElement elePaymentPage_title = titleCustomer.getFirstSelectedOption();
				customerTitle.add(elePaymentPage_title.getText().split("\\.")[0]);
				
				String paymentPage_LName =	driver.findElement(By.xpath(".//*[@id='"+gId+"']/tr/td/table/tbody/tr/td[3]/input")).getAttribute("value");
				customerLastName.add(paymentPage_LName);
				String paymentPage_FName = driver.findElement(By.xpath(".//*[@id='"+gId+"']/tr/td/table/tbody/tr/td[4]/input")).getAttribute("value");
				customerFirstName.add(paymentPage_FName);
			}
			
			hotelDetails.setCustomerTitle(customerTitle);
			hotelDetails.setCustomerLastName(customerLastName);
			hotelDetails.setCustomerFirstName(customerFirstName);
		
			///////
			
			String cusNotes = "Our product line includes a number of unique properties with very limited room capacity which result in a variety of cancellation policies.";
			driver.findElement(By.xpath(".//*[@id='cartDetails']/table/tbody/tr[2]/td[2]/table[3]/tbody/tr[2]/td/textarea")).sendKeys(cusNotes);
			hotelDetails.setPaymentsPage_CustomerNotes(cusNotes);
			Thread.sleep(1500);
			
			
			//Payments gateway (Credit Cards)
			if(toType.equalsIgnoreCase("PayNBook")){
				
				new Select(driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[1]/td[2]/table/tbody/tr/td[1]/select"))).selectByVisibleText("Visa");
				driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[1]/input")).sendKeys("Milroy Perera");
				driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[1]/input")).sendKeys("370268038793555");
				
				new Select(driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[4]/td[2]/select[1]"))).selectByValue("Jan");
				new Select(driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[4]/td[2]/select[2]"))).selectByValue("2020");
				
				driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[5]/td[2]/table/tbody/tr/td[1]/table/tbody/tr/td[1]/input")).sendKeys("123");
					
			}
			
			if(toType.equalsIgnoreCase("Prepay")){
				
				Date d1 = null;
				Date d2 = null;
		 
				try {
					d1 = formatToDate.parse(currentDate);
					d2 = formatToDate.parse(checkInDate);
		 
					//in milliseconds
					long diff = d2.getTime() - d1.getTime();
					long diffDays = diff / (24 * 60 * 60 * 1000);
		 
					System.out.print(diffDays);
					
					if(diffDays <= 21){
						
						new Select(driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[1]/td[2]/table/tbody/tr/td[1]/select"))).selectByVisibleText("Visa");
						driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[2]/td[2]/table/tbody/tr/td[1]/input")).sendKeys("Milroy Perera");
						driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[1]/input")).sendKeys("370268038793555");
						
						new Select(driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[4]/td[2]/select[1]"))).selectByValue("Jan");
						new Select(driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[4]/td[2]/select[2]"))).selectByValue("2020");
						
						driver.findElement(By.xpath(".//*[@id='cartDetails']/table[2]/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[5]/td[2]/table/tbody/tr/td[1]/table/tbody/tr/td[1]/input")).sendKeys("123");
								
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}	
			}
			
			//////
			Thread.sleep(1500);
			String paymentShoppingCartTotal = driver.findElement(By.xpath(".//*[@id='detailTable']/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/div")).getText();
			hotelDetails.setPaymentShoppingCartTotal(paymentShoppingCartTotal);
			Thread.sleep(1000);
			
			driver.findElement(By.xpath(".//*[@id='detailTable']/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/div/a/img")).click();
			long time_confirmButt  = (System.currentTimeMillis() / 1000);
			hotelDetails.setTime_confirmButt(time_confirmButt);
			
			wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = driver.switchTo().alert();
	        alert.accept();		
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cartItemTable")));
			long time_ReserVationNoAvailable  = (System.currentTimeMillis() / 1000);
			hotelDetails.setTime_ReserVationNoAvailable(time_ReserVationNoAvailable);
			
			
			//Confirmation Page
			
			String confirmationTOD_TOName = driver.findElement(By.xpath(".//*[@id='mainBody']/table[1]/tbody/tr[2]/td[2]/table[1]/tbody/tr[1]/td")).getText();
			hotelDetails.setConfirmationTOD_TOName(confirmationTOD_TOName);				
			String confirmationTOD_Address = driver.findElement(By.xpath(".//*[@id='mainBody']/table[1]/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td")).getText();
			hotelDetails.setConfirmationTOD_Address(confirmationTOD_Address);
			String confirmationTOD_City = driver.findElement(By.xpath(".//*[@id='mainBody']/table[1]/tbody/tr[2]/td[2]/table[1]/tbody/tr[3]/td")).getText();
			hotelDetails.setConfirmationTOD_City(confirmationTOD_City);
			String confirmationTOD_Country = driver.findElement(By.xpath(".//*[@id='mainBody']/table[1]/tbody/tr[2]/td[2]/table[1]/tbody/tr[4]/td")).getText();
			hotelDetails.setConfirmationTOD_Country(confirmationTOD_Country);
			String confirmationTOD_Zip = driver.findElement(By.xpath(".//*[@id='mainBody']/table[1]/tbody/tr[2]/td[2]/table[1]/tbody/tr[5]/td")).getText();
			hotelDetails.setConfirmationTOD_Zip(confirmationTOD_Zip);
			String confirmationTOD_Email = driver.findElement(By.xpath(".//*[@id='mainBody']/table[1]/tbody/tr[2]/td[2]/table[2]/tbody/tr[1]/td")).getText();
			hotelDetails.setConfirmationTOD_Email(confirmationTOD_Email);
			String confirmationTOD_phone = driver.findElement(By.xpath(".//*[@id='mainBody']/table[1]/tbody/tr[2]/td[2]/table[2]/tbody/tr[2]/td")).getText();
			hotelDetails.setConfirmationTOD_phone(confirmationTOD_phone);
						
			String confirmationTS_HotelName = driver.findElement(By.xpath(".//*[@id='cartItemTable']/tbody/tr[3]/td[1]")).getText();
			hotelDetails.setConfirmationTS_HotelName(confirmationTS_HotelName);		
			String confirmationTS_qty = driver.findElement(By.xpath(".//*[@id='cartItemTable']/tbody/tr[3]/td[3]")).getText();
			hotelDetails.setConfirmationTS_qty(confirmationTS_qty);	
			String confirmationTS_checkInDate = driver.findElement(By.xpath(".//*[@id='cartItemTable']/tbody/tr[3]/td[4]")).getText();
			hotelDetails.setConfirmationTS_checkInDate(confirmationTS_checkInDate);	
			String confirmationTS_checkoutDate = driver.findElement(By.xpath(".//*[@id='cartItemTable']/tbody/tr[3]/td[5]")).getText();
			hotelDetails.setConfirmationTS_checkoutDate(confirmationTS_checkoutDate);	
			String confirmationTS_amount = driver.findElement(By.xpath(".//*[@id='cartItemTable']/tbody/tr[3]/td[6]")).getText();
			hotelDetails.setConfirmationTS_amount(confirmationTS_amount);	
			String confirmationTS_confirmedTotal = driver.findElement(By.xpath(".//*[@id='cartItemTable']/tbody/tr[4]/td[2]")).getText();
			hotelDetails.setConfirmationTS_confirmedTotal(confirmationTS_confirmedTotal);
			
			String confirmationTS_Total = driver.findElement(By.xpath(".//*[@id='mainBody']/table[2]/tbody/tr[2]/td[2]/table[2]/tbody/tr[2]/td[2]/div")).getText();
			
			if (confirmationTS_Total.contains(",")) {
				hotelDetails.setConfirmationTS_Total(confirmationTS_Total.replaceAll(",", ""));
			} else {
				hotelDetails.setConfirmationTS_Total(confirmationTS_Total);
			}
			
		
			String confirmationTD_checkInDate = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[1]/tbody/tr[1]/td")).getText();
			hotelDetails.setConfirmationTD_checkInDate(confirmationTD_checkInDate);
			String confirmationTD_checkOutDate = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[1]/tbody/tr[2]/td")).getText();
			hotelDetails.setConfirmationTD_checkOutDate(confirmationTD_checkOutDate);
			String confirmationTD_nights = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[1]/tbody/tr[3]/td")).getText();
			hotelDetails.setConfirmationTD_nights(confirmationTD_nights);
			String confirmationTD_rooms = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[1]/tbody/tr[4]/td")).getText();
			hotelDetails.setConfirmationTD_rooms(confirmationTD_rooms);
			String confirmationTD_adults = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[1]/tbody/tr[5]/td")).getText();
			hotelDetails.setConfirmationTD_adults(confirmationTD_adults);
			String confirmationTD_children = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[1]/tbody/tr[6]/td")).getText();
			hotelDetails.setConfirmationTD_children(confirmationTD_children);
			String confirmationTD_HotelName = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[2]/tbody/tr[1]/td")).getText();
			hotelDetails.setConfirmationTD_HotelName(confirmationTD_HotelName);
			String confirmationTD_hotelAddress = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[2]/tbody/tr[2]/td")).getText().replaceAll(" ", "");
			hotelDetails.setConfirmationTD_hotelAddress(confirmationTD_hotelAddress);
			String confirmationTD_city = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[2]/tbody/tr[3]/td")).getText();
			hotelDetails.setConfirmationTD_city(confirmationTD_city);
			String confirmationTD_state = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[2]/tbody/tr[4]/td")).getText();
			hotelDetails.setConfirmationTD_state(confirmationTD_state);
			String confirmationTD_country = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[2]/tbody/tr[5]/td")).getText();
			hotelDetails.setConfirmationTD_country(confirmationTD_country);	
			String confirmationTD_BookingDate = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[3]/tbody/tr[1]/td")).getText();
			hotelDetails.setConfirmationTD_BookingDate(confirmationTD_BookingDate);		
			String confirmation_ReservationNO = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[3]/tbody/tr[2]/td")).getText();
			hotelDetails.setConfirmation_ReservationNO(confirmation_ReservationNO);
			
			ArrayList<String> confirmationTD_FirstNameA = new ArrayList<String>();
			ArrayList<String> confirmationTD_LastNsmeA = new ArrayList<String>();
			
			for (int i = 1; i <= excelRoomCount; i++) {
				
				String confirmationTD_FirstName = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[4]/tbody/tr["+(i+1)+"]/td[2]")).getText();
				confirmationTD_FirstNameA.add(confirmationTD_FirstName);
				String confirmationTD_LastNsme = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[4]/tbody/tr["+(i+1)+"]/td[3]")).getText();
				confirmationTD_LastNsmeA.add(confirmationTD_LastNsme);
				
				
			}
			
			hotelDetails.setConfirmationTD_FirstName(confirmationTD_FirstNameA);
			hotelDetails.setConfirmationTD_LastNsme(confirmationTD_LastNsmeA);
			
			/////////
			
			WebElement idForVoucherElement = driver.findElement(By.xpath("/html/body/div/table[3]/tbody/tr[2]/td[2]/table[5]/tbody/tr[2]/td[2]"));
			String idVoucher = idForVoucherElement.findElement(By.tagName("a")).getAttribute("href").substring(21, 25);
			
			//Voucher Details
			
			//Open a new window
			String parentHandle = driver.getWindowHandle();
		    driver.findElement(By.xpath(".//*[@id='printVoucher_"+idVoucher+"_0']/a/img")).click();
		    
		    for (String winHandle : driver.getWindowHandles()) {
		        driver.switchTo().window(winHandle); 
		    }
		    
		    if (driver.findElements(By.xpath("html/body/table[1]/tbody/tr[2]/td/font/b")).size() != 0) {
				
		    	String voucher_bookingDate = driver.findElement(By.xpath("html/body/table[1]/tbody/tr[4]/td/table/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_bookingDate(voucher_bookingDate);
				
				String voucher_bookingNo = driver.findElement(By.xpath("html/body/table[1]/tbody/tr[4]/td/table/tbody/tr[2]/td[2]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_bookingNo(voucher_bookingNo);
				
				String voucher_ToOrderNo = driver.findElement(By.xpath("html/body/table[1]/tbody/tr[4]/td/table/tbody/tr[1]/td[4]")).getText();
				hotelDetails.setVoucher_ToOrderNo(voucher_ToOrderNo);
				
				String voucher_ToName = driver.findElement(By.xpath("html/body/table[2]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_ToName(voucher_ToName);
				
				String voucher_HotelName = driver.findElement(By.xpath("html/body/table[3]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_HotelName(voucher_HotelName);
				
				String voucher_HotelAddress = driver.findElement(By.xpath("html/body/table[3]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/font")).getText();
				hotelDetails.setVoucher_HotelAddress(voucher_HotelAddress);
				
				String voucher_HotelCity = driver.findElement(By.xpath("html/body/table[3]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_HotelCity(voucher_HotelCity);
				
				ArrayList<String> customerNamess = new ArrayList<String>();
				
				for (int i = 1; i <= excelRoomCount; i++) {
					
					String name = driver.findElement(By.xpath("html/body/table[4]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
					customerNamess.add(name);
				}
				
				hotelDetails.setVoucher_customerNames(customerNamess);
				
				String voucher_CheckInDate = driver.findElement(By.xpath("html/body/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_CheckInDate(voucher_CheckInDate);
				
				String voucher_CheckOutDate = driver.findElement(By.xpath("html/body/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_CheckOutDate(voucher_CheckOutDate);
				
				String voucher_Nights = driver.findElement(By.xpath("html/body/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[4]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_Nights(voucher_Nights);
				
				String voucher_Rooms = driver.findElement(By.xpath("html/body/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[4]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_Rooms(voucher_Rooms);
				
				String voucher_Adults = driver.findElement(By.xpath("html/body/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[4]/td[2]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_Adults(voucher_Adults);
				
				String voucher_Children = driver.findElement(By.xpath("html/body/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[4]/td[4]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_Children(voucher_Children);
				
				String voucher_RoomTypes = driver.findElement(By.xpath("html/body/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_RoomTypes(voucher_RoomTypes);
				
				String voucher_OccupancyRatePlan = driver.findElement(By.xpath("html/body/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[5]/td[2]/font")).getText().split(": ")[1];
				hotelDetails.setVoucher_OccupancyRatePlan(voucher_OccupancyRatePlan);
				
				
		    }else{
		    	
		    	hotelDetails.setVoucherLoaded(false);	    	
		    }
		   						    
		    driver.close(); // close newly opened window when done with it
		    driver.switchTo().window(parentHandle); // switch back to the original window
			
			////////
			
			
			ArrayList<String> confirmationTD_AdultsA  = new ArrayList<String>();
			ArrayList<String> confirmationTD_ChildrenA = new ArrayList<String>();
			ArrayList<String> confirmationTD_RoomTypeA = new ArrayList<String>();
			ArrayList<String> confirmationTD_RoomBedTypeA = new ArrayList<String>();
			ArrayList<String> confirmationTD_RoomRatePlanA = new ArrayList<String>();
			ArrayList<String> confirmationTD_DaysCountA = new ArrayList<String>();
			ArrayList<String> confirmationTD_totalA = new ArrayList<String>();
			ArrayList<ArrayList<String>> confirmationTD_DailyRatesA = new ArrayList<ArrayList<String>>();
			
			
			for (int i = 1; i <= excelRoomCount; i++) {
				
				String confirmationTD_Adults = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[5]/tbody/tr["+(i+1)+"]/td[1]")).getText().split("\n")[1].split(": ")[1];
				confirmationTD_AdultsA.add(confirmationTD_Adults);
				
				String confirmationTD_Children = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[5]/tbody/tr["+(i+1)+"]/td[1]")).getText().split("\n")[2].split(": ")[1];
				confirmationTD_ChildrenA.add(confirmationTD_Children);
				
				String confirmationTD_RoomType = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[5]/tbody/tr["+(i+1)+"]/td[2]")).getText().split("\n")[0];
				confirmationTD_RoomTypeA.add(confirmationTD_RoomType);
				
				String confirmationTD_RoomBedType = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[5]/tbody/tr["+(i+1)+"]/td[3]")).getText().split("/")[0];
				confirmationTD_RoomBedTypeA.add(confirmationTD_RoomBedType);
				
				String confirmationTD_RoomRatePlan = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[5]/tbody/tr["+(i+1)+"]/td[3]")).getText().split("/")[1];
				confirmationTD_RoomRatePlanA.add(confirmationTD_RoomRatePlan);
				
				WebElement roomTableConEle = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[5]/tbody/tr["+(i+1)+"]/td[4]/table"));				
				List<WebElement> daysCountConElement = roomTableConEle.findElements(By.tagName("th"));
				confirmationTD_DaysCountA.add(Integer.toString(daysCountConElement.size()));
				
				WebElement roomratesConEle = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[5]/tbody/tr["+(i+1)+"]/td[4]/table"));				
				List<WebElement> ratesConElement = roomratesConEle.findElements(By.tagName("td"));
				
				ArrayList<String> dailyRatesconfirm = new ArrayList<String>();
				for (int k = 0; k < excelNights; k++) {
					
					String oneDayRateSeaP = ratesConElement.get(k).getText();
					dailyRatesconfirm.add(oneDayRateSeaP);
					
				}
				
				confirmationTD_DailyRatesA.add(dailyRatesconfirm);
				
				String confirmationTD_total = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[5]/tbody/tr["+(i+1)+"]/td[5]")).getText();
				confirmationTD_totalA.add(confirmationTD_total);
				
			}
			
			hotelDetails.setConfirmationTD_Adults(confirmationTD_AdultsA);		
			hotelDetails.setConfirmationTD_Children(confirmationTD_ChildrenA);	
			hotelDetails.setConfirmationTD_RoomType(confirmationTD_RoomTypeA);	
			hotelDetails.setConfirmationTD_RoomBedType(confirmationTD_RoomBedTypeA);		
			hotelDetails.setConfirmationTD_RoomRatePlan(confirmationTD_RoomRatePlanA);		
			hotelDetails.setConfirmationTD_DaysCount(confirmationTD_DaysCountA);	
			hotelDetails.setConfirmationTD_DailyRates(confirmationTD_DailyRatesA);			
			hotelDetails.setConfirmationTD_total(confirmationTD_totalA);
			
		
			String confirmationTD_BookingTotal = driver.findElement(By.xpath(".//*[@id='mainBody']/table[3]/tbody/tr[2]/td[2]/table[6]/tbody/tr[2]/td[2]")).getText().split(": ")[1];
			hotelDetails.setConfirmationTD_BookingTotal(confirmationTD_BookingTotal);
		
			
			
		} else {
			hotelDetails.setResultsAvailable(false);
		}
		
		
		return hotelDetails;

	}

	public void cancellationFlow(WebDriver webdriver){
		
		WebDriverWait wait = new WebDriverWait(driver, 120);	
		
		try {
			
			driver.get(PG_Properties.getProperty("Baseurl") + "/reservation/ReservationCancellationPage.do");
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationid")));
			Thread.sleep(500);	
			
			String reservationId = hotelDetails.getConfirmation_ReservationNO();
			
			driver.findElement(By.id("reservationid")).clear();
			driver.findElement(By.id("reservationid")).sendKeys(reservationId);
			driver.findElement(By.id("reservationid_lkup")).click();
			driver.switchTo().frame("lookup");
			Thread.sleep(1000);
			driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
			Thread.sleep(1000);
			driver.switchTo().defaultContent();
			
			driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/a/img")).click();
			Thread.sleep(1000);
			
			driver.findElement(By.xpath(".//*[@id='saveButId']")).click();
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")));
			driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
			Thread.sleep(1000);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
	
	
	
	
	
	
}
