package com.model;

import java.util.ArrayList;

public class SearchInfo {
	
	private String scenarioId;
	private String tourOperatorName;
	private String country;
	private String city;
	private String StateCity;
	private String selectHotel;
	private String dateFrom;
	private String dateTo;
	private String nights;
	private String rooms;
	private String adults;
	private String children;
	private String child_ages;
	private String bookingType;
	
	private String excelHotelName;
	private String excelHotelAddress;
	private String excelRoomType;
	private String excelOccupancyratePlan;
	private String bookingChannel;
	private String TO_type;
	private String hotelDescription;
	private String isPromoApply;
	private ArrayList<String> roomBedTypeRatePlan;
	
	private ArrayList<Rates> ratesInfo			= new ArrayList<Rates>();
	private ArrayList<Promotion> promoInfo			= new ArrayList<Promotion>();
	
	
	public void addratesInfo(Rates rates)
	{
		ratesInfo.add(rates);
	}
	
	public void addPromotionInfo(Promotion promotion)
	{
		promoInfo.add(promotion);
	}
	
	
	public ArrayList<Promotion> getPromoInfo() {
		return promoInfo;
	}

	public void setPromoInfo(ArrayList<Promotion> promoInfo) {
		this.promoInfo = promoInfo;
	}

	public ArrayList<Rates> getRatesInfo() {
		return ratesInfo;
	}

	public void setRatesInfo(ArrayList<Rates> ratesInfo) {
		this.ratesInfo = ratesInfo;
	}

	public String getExcelHotelAddress() {
		return excelHotelAddress;
	}
	public void setExcelHotelAddress(String excelHotelAddress) {
		this.excelHotelAddress = excelHotelAddress;
	}
	public String getTO_type() {
		return TO_type;
	}
	public void setTO_type(String tO_type) {
		TO_type = tO_type;
	}
	public String getScenarioId() {
		return scenarioId;
	}
	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}
	public String getBookingChannel() {
		return bookingChannel;
	}
	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}
	public String getExcelRoomType() {
		return excelRoomType;
	}
	public void setExcelRoomType(String excelRoomType) {
		this.excelRoomType = excelRoomType;
	}
	public String getExcelOccupancyratePlan() {
		return excelOccupancyratePlan;
	}
	public void setExcelOccupancyratePlan(String excelOccupancyratePlan) {
		this.excelOccupancyratePlan = excelOccupancyratePlan;
	}
	public String getExcelHotelName() {
		return excelHotelName;
	}
	public void setExcelHotelName(String excelHotelName) {
		this.excelHotelName = excelHotelName;
	}
	public String getTourOperatorName() {
		return tourOperatorName;
	}
	public void setTourOperatorName(String tourOperatorName) {
		this.tourOperatorName = tourOperatorName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getSelectHotel() {
		return selectHotel;
	}
	public void setSelectHotel(String selectHotel) {
		this.selectHotel = selectHotel;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public String getNights() {
		return nights;
	}
	public void setNights(String nights) {
		this.nights = nights;
	}
	public String getRooms() {
		return rooms;
	}
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getChildren() {
		return children;
	}
	public void setChildren(String children) {
		this.children = children;
	}
	public String getChild_ages() {
		return child_ages;
	}
	public void setChild_ages(String child_ages) {
		this.child_ages = child_ages;
	}
	public String getBookingType() {
		return bookingType;
	}
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}
	public String getHotelDescription() {
		return hotelDescription;
	}
	public void setHotelDescription(String hotelDescription) {
		this.hotelDescription = hotelDescription;
	}
	public ArrayList<String> getRoomBedTypeRatePlan() {
		return roomBedTypeRatePlan;
	}
	public void setRoomBedTypeRatePlan(ArrayList<String> roomBedTypeRatePlan) {
		this.roomBedTypeRatePlan = roomBedTypeRatePlan;
	}

	public String getIsPromoApply() {
		return isPromoApply;
	}

	public void setIsPromoApply(String isPromoApply) {
		this.isPromoApply = isPromoApply;
	}

	public String getStateCity() {
		return StateCity;
	}

	public void setStateCity(String stateCity) {
		StateCity = stateCity;
	}
	
	
	
	
	
	
	

}
