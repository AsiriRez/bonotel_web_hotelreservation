package com.model;

import java.util.ArrayList;

public class Modification {
	
	private boolean isModificationReportLoaded  = true;
	private String summary_hotelName;
	private String summary_hotelAddress;
	private String summary_cityStateCountry;
	private String summary_nights;
	private String summary_rooms;
	private String summary_children;
	private String summary_bookingDate;
	private String summary_TOName;
	private String summary_chechInDate;
	private String summary_checkOutDate;
	private String summary_adults;
	private String summary_BookingNo;
	private String summary_BookingSource;
	private ArrayList<String> summary_cusTitle;
	private ArrayList<String> summary_cusFName;
	private ArrayList<String> summary_cusLname;
	
	private ArrayList<String> summary_roomTypeList;
	private ArrayList<String> summary_OccupancyTypeList;
	private ArrayList<String> summary_AdultsList;
	private ArrayList<String> summary_childrenList;
	private ArrayList<String> summary_GuestName;
	private ArrayList<ArrayList<String>> summary_DailyRates;
	private ArrayList<String> summary_DaysCount;
	private ArrayList<String> summary_Total;
	
	private String summary_Currency1;
	private String summary_Currency2;
	private String summary_TotalBookingValue;
	private String summary_TotalCost;
	private String summary_TONotes;
	private String summary_HotelNotes;
	private String summary_InternalNotes;
	
	///////
	
	private ArrayList<String> GD_cusTitle;
	private ArrayList<String> GD_cusFName;
	private ArrayList<String> GD_cusLname;
	
	
	////
	
	private String SP_HotelName;
	private String SP_checkInDate;
	private String SP_checkOutDate;
	private String SP_Rooms;
	private String SP_Adults;
	private String SP_Child;
	private String SP_Nights;
	
	private ArrayList<String> SP_roomTypeList;
	private ArrayList<String> SP_OccupancyTypeList;
	private ArrayList<String> SP_AdultsList;
	private ArrayList<String> SP_childrenList;
	private ArrayList<String> SP_GuestName;
	private ArrayList<ArrayList<String>> SP_DailyRates;
	private ArrayList<String> SP_DaysCount;
	private ArrayList<String> SP_Total;
	
	private String SP_TotalBookingValue;
	private String SP_TotalCost;
	
	////
	
	private String CAR_HotelName;
	private String CAR_checkInDate;
	private String CAR_checkOutDate;
	private String CAR_Rooms;
	private String CAR_Adults;
	private String CAR_Child;
	private String CAR_Nights;
	
	private ArrayList<String> CAR_roomTypeList;
	private ArrayList<String> CAR_OccupancyTypeList;
	private ArrayList<String> CAR_AdultsList;
	private ArrayList<String> CAR_childrenList;
	private ArrayList<String> CAR_GuestName;
	private ArrayList<ArrayList<String>> CAR_DailyRates;
	private ArrayList<String> CAR_DaysCount;
	private ArrayList<String> CAR_Total;
	
	private String CAR_TotalBookingValue;
	private String CAR_TotalCost;
	
	////////
	
	private String RR_HotelName;
	private String RR_checkInDate;
	private String RR_checkOutDate;
	private String RR_Rooms;
	private String RR_Adults;
	private String RR_Child;
	private String RR_Nights;
	
	private ArrayList<String> RR_roomTypeList;
	private ArrayList<String> RR_OccupancyTypeList;
	private ArrayList<String> RR_AdultsList;
	private ArrayList<String> RR_childrenList;
	private ArrayList<String> RR_GuestName;
	private ArrayList<ArrayList<String>> RR_DailyRates;
	private ArrayList<String> RR_DaysCount;
	private ArrayList<String> RR_Total;
	
	private String RR_TotalBookingValue;
	private String RR_TotalCost;
	
	
	
	public ArrayList<String> getGD_cusTitle() {
		return GD_cusTitle;
	}
	public void setGD_cusTitle(ArrayList<String> gD_cusTitle) {
		GD_cusTitle = gD_cusTitle;
	}
	public ArrayList<String> getGD_cusFName() {
		return GD_cusFName;
	}
	public void setGD_cusFName(ArrayList<String> gD_cusFName) {
		GD_cusFName = gD_cusFName;
	}
	public ArrayList<String> getGD_cusLname() {
		return GD_cusLname;
	}
	public void setGD_cusLname(ArrayList<String> gD_cusLname) {
		GD_cusLname = gD_cusLname;
	}
	public String getSP_HotelName() {
		return SP_HotelName;
	}
	public void setSP_HotelName(String sP_HotelName) {
		SP_HotelName = sP_HotelName;
	}
	public String getSP_checkInDate() {
		return SP_checkInDate;
	}
	public void setSP_checkInDate(String sP_checkInDate) {
		SP_checkInDate = sP_checkInDate;
	}
	public String getSP_checkOutDate() {
		return SP_checkOutDate;
	}
	public void setSP_checkOutDate(String sP_checkOutDate) {
		SP_checkOutDate = sP_checkOutDate;
	}
	public String getSP_Rooms() {
		return SP_Rooms;
	}
	public void setSP_Rooms(String sP_Rooms) {
		SP_Rooms = sP_Rooms;
	}
	public String getSP_Adults() {
		return SP_Adults;
	}
	public void setSP_Adults(String sP_Adults) {
		SP_Adults = sP_Adults;
	}
	public String getSP_Child() {
		return SP_Child;
	}
	public void setSP_Child(String sP_Child) {
		SP_Child = sP_Child;
	}
	public String getSP_Nights() {
		return SP_Nights;
	}
	public void setSP_Nights(String sP_Nights) {
		SP_Nights = sP_Nights;
	}
	public ArrayList<String> getSP_roomTypeList() {
		return SP_roomTypeList;
	}
	public void setSP_roomTypeList(ArrayList<String> sP_roomTypeList) {
		SP_roomTypeList = sP_roomTypeList;
	}
	public ArrayList<String> getSP_OccupancyTypeList() {
		return SP_OccupancyTypeList;
	}
	public void setSP_OccupancyTypeList(ArrayList<String> sP_OccupancyTypeList) {
		SP_OccupancyTypeList = sP_OccupancyTypeList;
	}
	public ArrayList<String> getSP_AdultsList() {
		return SP_AdultsList;
	}
	public void setSP_AdultsList(ArrayList<String> sP_AdultsList) {
		SP_AdultsList = sP_AdultsList;
	}
	public ArrayList<String> getSP_childrenList() {
		return SP_childrenList;
	}
	public void setSP_childrenList(ArrayList<String> sP_childrenList) {
		SP_childrenList = sP_childrenList;
	}
	public ArrayList<String> getSP_GuestName() {
		return SP_GuestName;
	}
	public void setSP_GuestName(ArrayList<String> sP_GuestName) {
		SP_GuestName = sP_GuestName;
	}
	public ArrayList<ArrayList<String>> getSP_DailyRates() {
		return SP_DailyRates;
	}
	public void setSP_DailyRates(ArrayList<ArrayList<String>> sP_DailyRates) {
		SP_DailyRates = sP_DailyRates;
	}
	public ArrayList<String> getSP_DaysCount() {
		return SP_DaysCount;
	}
	public void setSP_DaysCount(ArrayList<String> sP_DaysCount) {
		SP_DaysCount = sP_DaysCount;
	}
	public ArrayList<String> getSP_Total() {
		return SP_Total;
	}
	public void setSP_Total(ArrayList<String> sP_Total) {
		SP_Total = sP_Total;
	}
	public String getSP_TotalBookingValue() {
		return SP_TotalBookingValue;
	}
	public void setSP_TotalBookingValue(String sP_TotalBookingValue) {
		SP_TotalBookingValue = sP_TotalBookingValue;
	}
	public String getSP_TotalCost() {
		return SP_TotalCost;
	}
	public void setSP_TotalCost(String sP_TotalCost) {
		SP_TotalCost = sP_TotalCost;
	}
	public String getCAR_HotelName() {
		return CAR_HotelName;
	}
	public void setCAR_HotelName(String cAR_HotelName) {
		CAR_HotelName = cAR_HotelName;
	}
	public String getCAR_checkInDate() {
		return CAR_checkInDate;
	}
	public void setCAR_checkInDate(String cAR_checkInDate) {
		CAR_checkInDate = cAR_checkInDate;
	}
	public String getCAR_checkOutDate() {
		return CAR_checkOutDate;
	}
	public void setCAR_checkOutDate(String cAR_checkOutDate) {
		CAR_checkOutDate = cAR_checkOutDate;
	}
	public String getCAR_Rooms() {
		return CAR_Rooms;
	}
	public void setCAR_Rooms(String cAR_Rooms) {
		CAR_Rooms = cAR_Rooms;
	}
	public String getCAR_Adults() {
		return CAR_Adults;
	}
	public void setCAR_Adults(String cAR_Adults) {
		CAR_Adults = cAR_Adults;
	}
	public String getCAR_Child() {
		return CAR_Child;
	}
	public void setCAR_Child(String cAR_Child) {
		CAR_Child = cAR_Child;
	}
	public String getCAR_Nights() {
		return CAR_Nights;
	}
	public void setCAR_Nights(String cAR_Nights) {
		CAR_Nights = cAR_Nights;
	}
	public ArrayList<String> getCAR_roomTypeList() {
		return CAR_roomTypeList;
	}
	public void setCAR_roomTypeList(ArrayList<String> cAR_roomTypeList) {
		CAR_roomTypeList = cAR_roomTypeList;
	}
	public ArrayList<String> getCAR_OccupancyTypeList() {
		return CAR_OccupancyTypeList;
	}
	public void setCAR_OccupancyTypeList(ArrayList<String> cAR_OccupancyTypeList) {
		CAR_OccupancyTypeList = cAR_OccupancyTypeList;
	}
	public ArrayList<String> getCAR_AdultsList() {
		return CAR_AdultsList;
	}
	public void setCAR_AdultsList(ArrayList<String> cAR_AdultsList) {
		CAR_AdultsList = cAR_AdultsList;
	}
	public ArrayList<String> getCAR_childrenList() {
		return CAR_childrenList;
	}
	public void setCAR_childrenList(ArrayList<String> cAR_childrenList) {
		CAR_childrenList = cAR_childrenList;
	}
	public ArrayList<String> getCAR_GuestName() {
		return CAR_GuestName;
	}
	public void setCAR_GuestName(ArrayList<String> cAR_GuestName) {
		CAR_GuestName = cAR_GuestName;
	}
	public ArrayList<ArrayList<String>> getCAR_DailyRates() {
		return CAR_DailyRates;
	}
	public void setCAR_DailyRates(ArrayList<ArrayList<String>> cAR_DailyRates) {
		CAR_DailyRates = cAR_DailyRates;
	}
	public ArrayList<String> getCAR_DaysCount() {
		return CAR_DaysCount;
	}
	public void setCAR_DaysCount(ArrayList<String> cAR_DaysCount) {
		CAR_DaysCount = cAR_DaysCount;
	}
	public ArrayList<String> getCAR_Total() {
		return CAR_Total;
	}
	public void setCAR_Total(ArrayList<String> cAR_Total) {
		CAR_Total = cAR_Total;
	}
	public String getCAR_TotalBookingValue() {
		return CAR_TotalBookingValue;
	}
	public void setCAR_TotalBookingValue(String cAR_TotalBookingValue) {
		CAR_TotalBookingValue = cAR_TotalBookingValue;
	}
	public String getCAR_TotalCost() {
		return CAR_TotalCost;
	}
	public void setCAR_TotalCost(String cAR_TotalCost) {
		CAR_TotalCost = cAR_TotalCost;
	}
	public String getRR_HotelName() {
		return RR_HotelName;
	}
	public void setRR_HotelName(String rR_HotelName) {
		RR_HotelName = rR_HotelName;
	}
	public String getRR_checkInDate() {
		return RR_checkInDate;
	}
	public void setRR_checkInDate(String rR_checkInDate) {
		RR_checkInDate = rR_checkInDate;
	}
	public String getRR_checkOutDate() {
		return RR_checkOutDate;
	}
	public void setRR_checkOutDate(String rR_checkOutDate) {
		RR_checkOutDate = rR_checkOutDate;
	}
	public String getRR_Rooms() {
		return RR_Rooms;
	}
	public void setRR_Rooms(String rR_Rooms) {
		RR_Rooms = rR_Rooms;
	}
	public String getRR_Adults() {
		return RR_Adults;
	}
	public void setRR_Adults(String rR_Adults) {
		RR_Adults = rR_Adults;
	}
	public String getRR_Child() {
		return RR_Child;
	}
	public void setRR_Child(String rR_Child) {
		RR_Child = rR_Child;
	}
	public String getRR_Nights() {
		return RR_Nights;
	}
	public void setRR_Nights(String rR_Nights) {
		RR_Nights = rR_Nights;
	}
	public ArrayList<String> getRR_roomTypeList() {
		return RR_roomTypeList;
	}
	public void setRR_roomTypeList(ArrayList<String> rR_roomTypeList) {
		RR_roomTypeList = rR_roomTypeList;
	}
	public ArrayList<String> getRR_OccupancyTypeList() {
		return RR_OccupancyTypeList;
	}
	public void setRR_OccupancyTypeList(ArrayList<String> rR_OccupancyTypeList) {
		RR_OccupancyTypeList = rR_OccupancyTypeList;
	}
	public ArrayList<String> getRR_AdultsList() {
		return RR_AdultsList;
	}
	public void setRR_AdultsList(ArrayList<String> rR_AdultsList) {
		RR_AdultsList = rR_AdultsList;
	}
	public ArrayList<String> getRR_childrenList() {
		return RR_childrenList;
	}
	public void setRR_childrenList(ArrayList<String> rR_childrenList) {
		RR_childrenList = rR_childrenList;
	}
	public ArrayList<String> getRR_GuestName() {
		return RR_GuestName;
	}
	public void setRR_GuestName(ArrayList<String> rR_GuestName) {
		RR_GuestName = rR_GuestName;
	}
	public ArrayList<ArrayList<String>> getRR_DailyRates() {
		return RR_DailyRates;
	}
	public void setRR_DailyRates(ArrayList<ArrayList<String>> rR_DailyRates) {
		RR_DailyRates = rR_DailyRates;
	}
	public ArrayList<String> getRR_DaysCount() {
		return RR_DaysCount;
	}
	public void setRR_DaysCount(ArrayList<String> rR_DaysCount) {
		RR_DaysCount = rR_DaysCount;
	}
	public ArrayList<String> getRR_Total() {
		return RR_Total;
	}
	public void setRR_Total(ArrayList<String> rR_Total) {
		RR_Total = rR_Total;
	}
	public String getRR_TotalBookingValue() {
		return RR_TotalBookingValue;
	}
	public void setRR_TotalBookingValue(String rR_TotalBookingValue) {
		RR_TotalBookingValue = rR_TotalBookingValue;
	}
	public String getRR_TotalCost() {
		return RR_TotalCost;
	}
	public void setRR_TotalCost(String rR_TotalCost) {
		RR_TotalCost = rR_TotalCost;
	}
	public boolean isModificationReportLoaded() {
		return isModificationReportLoaded;
	}
	public void setModificationReportLoaded(boolean isModificationReportLoaded) {
		this.isModificationReportLoaded = isModificationReportLoaded;
	}
	public String getSummary_hotelName() {
		return summary_hotelName;
	}
	public void setSummary_hotelName(String summary_hotelName) {
		this.summary_hotelName = summary_hotelName;
	}
	public String getSummary_hotelAddress() {
		return summary_hotelAddress;
	}
	public void setSummary_hotelAddress(String summary_hotelAddress) {
		this.summary_hotelAddress = summary_hotelAddress;
	}
	public String getSummary_cityStateCountry() {
		return summary_cityStateCountry;
	}
	public void setSummary_cityStateCountry(String summary_cityStateCountry) {
		this.summary_cityStateCountry = summary_cityStateCountry;
	}
	public String getSummary_nights() {
		return summary_nights;
	}
	public void setSummary_nights(String summary_nights) {
		this.summary_nights = summary_nights;
	}
	public String getSummary_rooms() {
		return summary_rooms;
	}
	public void setSummary_rooms(String summary_rooms) {
		this.summary_rooms = summary_rooms;
	}
	public String getSummary_children() {
		return summary_children;
	}
	public void setSummary_children(String summary_children) {
		this.summary_children = summary_children;
	}
	public String getSummary_bookingDate() {
		return summary_bookingDate;
	}
	public void setSummary_bookingDate(String summary_bookingDate) {
		this.summary_bookingDate = summary_bookingDate;
	}
	public String getSummary_TOName() {
		return summary_TOName;
	}
	public void setSummary_TOName(String summary_TOName) {
		this.summary_TOName = summary_TOName;
	}
	public String getSummary_chechInDate() {
		return summary_chechInDate;
	}
	public void setSummary_chechInDate(String summary_chechInDate) {
		this.summary_chechInDate = summary_chechInDate;
	}
	public String getSummary_checkOutDate() {
		return summary_checkOutDate;
	}
	public void setSummary_checkOutDate(String summary_checkOutDate) {
		this.summary_checkOutDate = summary_checkOutDate;
	}
	public String getSummary_adults() {
		return summary_adults;
	}
	public void setSummary_adults(String summary_adults) {
		this.summary_adults = summary_adults;
	}
	public String getSummary_BookingNo() {
		return summary_BookingNo;
	}
	public void setSummary_BookingNo(String summary_BookingNo) {
		this.summary_BookingNo = summary_BookingNo;
	}
	public String getSummary_BookingSource() {
		return summary_BookingSource;
	}
	public void setSummary_BookingSource(String summary_BookingSource) {
		this.summary_BookingSource = summary_BookingSource;
	}
	public ArrayList<String> getSummary_cusTitle() {
		return summary_cusTitle;
	}
	public void setSummary_cusTitle(ArrayList<String> summary_cusTitle) {
		this.summary_cusTitle = summary_cusTitle;
	}
	public ArrayList<String> getSummary_cusFName() {
		return summary_cusFName;
	}
	public void setSummary_cusFName(ArrayList<String> summary_cusFName) {
		this.summary_cusFName = summary_cusFName;
	}
	public ArrayList<String> getSummary_cusLname() {
		return summary_cusLname;
	}
	public void setSummary_cusLname(ArrayList<String> summary_cusLname) {
		this.summary_cusLname = summary_cusLname;
	}
	public ArrayList<String> getSummary_roomTypeList() {
		return summary_roomTypeList;
	}
	public void setSummary_roomTypeList(ArrayList<String> summary_roomTypeList) {
		this.summary_roomTypeList = summary_roomTypeList;
	}
	public ArrayList<String> getSummary_OccupancyTypeList() {
		return summary_OccupancyTypeList;
	}
	public void setSummary_OccupancyTypeList(
			ArrayList<String> summary_OccupancyTypeList) {
		this.summary_OccupancyTypeList = summary_OccupancyTypeList;
	}
	public ArrayList<String> getSummary_AdultsList() {
		return summary_AdultsList;
	}
	public void setSummary_AdultsList(ArrayList<String> summary_AdultsList) {
		this.summary_AdultsList = summary_AdultsList;
	}
	public ArrayList<String> getSummary_childrenList() {
		return summary_childrenList;
	}
	public void setSummary_childrenList(ArrayList<String> summary_childrenList) {
		this.summary_childrenList = summary_childrenList;
	}
	public ArrayList<String> getSummary_GuestName() {
		return summary_GuestName;
	}
	public void setSummary_GuestName(ArrayList<String> summary_GuestName) {
		this.summary_GuestName = summary_GuestName;
	}
	public ArrayList<ArrayList<String>> getSummary_DailyRates() {
		return summary_DailyRates;
	}
	public void setSummary_DailyRates(
			ArrayList<ArrayList<String>> summary_DailyRates) {
		this.summary_DailyRates = summary_DailyRates;
	}
	public ArrayList<String> getSummary_DaysCount() {
		return summary_DaysCount;
	}
	public void setSummary_DaysCount(ArrayList<String> summary_DaysCount) {
		this.summary_DaysCount = summary_DaysCount;
	}
	public ArrayList<String> getSummary_Total() {
		return summary_Total;
	}
	public void setSummary_Total(ArrayList<String> summary_Total) {
		this.summary_Total = summary_Total;
	}
	public String getSummary_Currency1() {
		return summary_Currency1;
	}
	public void setSummary_Currency1(String summary_Currency1) {
		this.summary_Currency1 = summary_Currency1;
	}
	public String getSummary_Currency2() {
		return summary_Currency2;
	}
	public void setSummary_Currency2(String summary_Currency2) {
		this.summary_Currency2 = summary_Currency2;
	}
	public String getSummary_TotalBookingValue() {
		return summary_TotalBookingValue;
	}
	public void setSummary_TotalBookingValue(String summary_TotalBookingValue) {
		this.summary_TotalBookingValue = summary_TotalBookingValue;
	}
	public String getSummary_TotalCost() {
		return summary_TotalCost;
	}
	public void setSummary_TotalCost(String summary_TotalCost) {
		this.summary_TotalCost = summary_TotalCost;
	}
	public String getSummary_TONotes() {
		return summary_TONotes;
	}
	public void setSummary_TONotes(String summary_TONotes) {
		this.summary_TONotes = summary_TONotes;
	}
	public String getSummary_HotelNotes() {
		return summary_HotelNotes;
	}
	public void setSummary_HotelNotes(String summary_HotelNotes) {
		this.summary_HotelNotes = summary_HotelNotes;
	}
	public String getSummary_InternalNotes() {
		return summary_InternalNotes;
	}
	public void setSummary_InternalNotes(String summary_InternalNotes) {
		this.summary_InternalNotes = summary_InternalNotes;
	}
	
	
	
}
