package com.reader;

import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.controller.PG_Properties;
import com.model.ConfirmationReport;
import com.model.HotelDetails;
import com.model.SearchInfo;

public class ConfirmationReportFlow {

	private ConfirmationReport conDetails = null;
	private WebDriver driver = null;
	private HotelDetails hotelDetails = new HotelDetails();
	private SearchInfo sInfo = new SearchInfo();
	
	public ConfirmationReportFlow(HotelDetails hoteldetails, SearchInfo sInfo, WebDriver Driver){
		
		this.driver = Driver;
		this.hotelDetails = hoteldetails;	
		this.sInfo = sInfo;	
	}
	
	public ConfirmationReport loadConfirmationReportFlow(WebDriver webDriver) throws InterruptedException{
		
		conDetails = new ConfirmationReport();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		driver.get(PG_Properties.getProperty("Baseurl")+ "/reports/OpsBookingConfirmationReport.do");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("hotelname")));
		Thread.sleep(2000);
		
		driver.findElement(By.xpath(".//*[@id='producttype_A']")).click();	
		driver.findElement(By.xpath(".//*[@id='producttype_H']")).click();	
		
		driver.findElement(By.xpath(".//*[@id='searchby_reservationno']")).click();		
		Thread.sleep(2000);

		String reservationNo = hotelDetails.getConfirmation_ReservationNO();
		driver.findElement(By.id("reservationno")).clear();
		driver.findElement(By.id("reservationno")).sendKeys(reservationNo);
		driver.findElement(By.id("reservationno_lkup")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("lookup");
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		
		driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[3]/a/img")).click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reportData")));
		
		//Customer Mail
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_customer']")).click();
		driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[1]/a")).click();
		Thread.sleep(1500);
		
		driver.switchTo().frame("Myemailformat");
		
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[1]/td/font[1]/b")).size() != 0) {
			
			String cus_BonoName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[1]/td/font[1]/b")).getText();
			conDetails.setCus_BonoName(cus_BonoName);
			
			String cus_BonoAddress = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[1]/td/font[2]/b")).getText();
			conDetails.setCus_BonoAddress(cus_BonoAddress);
			
			String cus_TpnFax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[1]/td/font[3]")).getText();
			conDetails.setCus_TpnFax(cus_TpnFax);
			
			
			String cus_bookingDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[4]/td/table/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_bookingDate(cus_bookingDate);
			
//			String cus_ToOrderNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[4]/td/table/tbody/tr[1]/td[4]/font")).getText().split(": ")[1];
//			conDetails.setCus_ToOrderNo(cus_ToOrderNo);
			
			String cus_BookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[4]/td/table/tbody/tr[2]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_bookingNo(cus_BookingNo);
			
			String cus_ToName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_ToName(cus_ToName);
			
			String cus_ToEmail = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_ToEmail(cus_ToEmail);
			
			String cus_HotelName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[3]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_HotelName(cus_HotelName);
			
			String cus_HotelAddress = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[3]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/font")).getText();
			conDetails.setCus_HotelAddress(cus_HotelAddress);
			
			String cus_HotelCity = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[3]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_HotelCity(cus_HotelCity);
			
			ArrayList<String> customerNamess = new ArrayList<String>();
			
			for (int i = 1; i <= Integer.parseInt(sInfo.getRooms()); i++) {
				
				String name = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[4]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
				customerNamess.add(name);
			}
			
			conDetails.setCustomerNames(customerNamess);
			
			String cus_CheckInDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_CheckInDate(cus_CheckInDate);
			
			String cus_CheckOutDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_CheckOutDate(cus_CheckOutDate);
			
			String cus_Nights = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[4]/font")).getText().split(": ")[1];
			conDetails.setCus_Nights(cus_Nights);
			
			String cus_Rooms = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[4]/font")).getText().split(": ")[1];
			conDetails.setCus_Rooms(cus_Rooms);
			
			String cus_Adults = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[4]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_Adults(cus_Adults);
			
			String cus_Children = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[4]/td[4]/font")).getText().split(": ")[1];
			conDetails.setCus_Children(cus_Children);
			
			String cus_RoomTypes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_RoomTypes(cus_RoomTypes);
			
			String cus_OccupancyRatePlan = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[5]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_OccupancyRatePlan(cus_OccupancyRatePlan);
			
			String cus_CustomerNotes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[8]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_CustomerNotes(cus_CustomerNotes);
			
			String cus_DailyRatesWithDates = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/font")).getText();
			conDetails.setCus_DailyRatesWithDates(cus_DailyRatesWithDates);
			
			String cus_ModifyFee = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/font")).getText().split(": ")[1];
			conDetails.setCus_ModifyFee(cus_ModifyFee);
			
			String cus_TotalBookingCharge = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/font")).getText().split(": ")[1];
			
			if (cus_TotalBookingCharge.contains(",")) {
				conDetails.setCus_TotalBookingCharge(cus_TotalBookingCharge.replaceAll(",", ""));
			} else {
				conDetails.setCus_TotalBookingCharge(cus_TotalBookingCharge);
			}
			
			
			
			
			
		
		} else {
			
			conDetails.setCustomerMailLoaded(false);
		}
		
		//Hotel mail
		
		driver.switchTo().defaultContent();
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_hotel']")).click();
		driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[1]/a")).click();
		Thread.sleep(1500);
		
		driver.switchTo().frame("Myemailformat");
		
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[1]/td/font/b")).size() != 0) {
			
			String hotel_BonoName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[1]/td/font/b")).getText();
			conDetails.setHotel_BonoName(hotel_BonoName);
			
			String addA = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[2]/td/font/b")).getText() + driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[3]/td/font/b")).getText();
			conDetails.setHotel_BonoAddress(addA);
			
			String hotel_TpnFax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[4]/td/font")).getText();
			conDetails.setHotel_TpnFax(hotel_TpnFax);
			
			String hotel_HotelName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
			conDetails.setHotel_HotelName(hotel_HotelName);
			
			String hotel_BookingDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr[2]/td[2]/font")).getText().split(": ")[1];
			conDetails.setHotel_BookingDate(hotel_BookingDate);
			
			String hotel_city = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr[2]/td[4]/font")).getText().split(": ")[1];
			conDetails.setHotel_city(hotel_city);
			
			String hotel_bonoNameFrom = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr[4]/td[2]/font")).getText().split(": ")[1];
			conDetails.setHotel_bonoNameFrom(hotel_bonoNameFrom);
			
			ArrayList<String> hotel_CustomerName = new ArrayList<String>();
			ArrayList<String> hotel_BookingNo = new ArrayList<String>();
			ArrayList<String> hotel_RoomType = new ArrayList<String>();
			ArrayList<String> hotel_OccupancyRateType = new ArrayList<String>();
			ArrayList<String> hotel_ArrivalDate = new ArrayList<String>();
			ArrayList<String> hotel_Nights = new ArrayList<String>();
			ArrayList<String> hotel_Adults = new ArrayList<String>();
			ArrayList<String> hotel_Rooms = new ArrayList<String>();
			ArrayList<String> hotel_Children = new ArrayList<String>();
			ArrayList<String> hotel_DailyRates = new ArrayList<String>();
			ArrayList<String> hotel_TotalRoomCost = new ArrayList<String>();
			ArrayList<String> hotel_ModifyFee = new ArrayList<String>();
			
			final short LOOP_VALUES = 300;
			for (int i = 10; i < LOOP_VALUES; i+=0) {
				
				if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+(i+5)+"]/td[2]/font")).size() != 0) {
					
					String cName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
					hotel_CustomerName.add(cName);
					i++;
					i++;
					
					String cBookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
					hotel_BookingNo.add(cBookingNo);
					i++;
					i++;
					
					String cRoomType = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
					hotel_RoomType.add(cRoomType);
					i++;
					
					String cOccuRate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
					hotel_OccupancyRateType.add(cOccuRate);
					i++;
					
					String cArriDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
					hotel_ArrivalDate.add(cArriDate);
					i++;
					
					String cNights = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
					hotel_Nights.add(cNights);
					
					String cRooms = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td[4]/font")).getText().split(": ")[1];
					hotel_Rooms.add(cRooms);
					i++;
					
					String cAdults = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
					hotel_Adults.add(cAdults);
					
					String cChildren = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td[4]/font")).getText().split(": ")[1].split(" Children")[0];
					hotel_Children.add(cChildren);
					i+=4;
					
					String cDailyrates = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td/font")).getText();
					hotel_DailyRates.add(cDailyrates);
					i++;
					
					String cRoomCost = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
					
					if (cRoomCost.contains(",")) {
						hotel_TotalRoomCost.add(cRoomCost.replaceAll(",", ""));
					} else {
						hotel_TotalRoomCost.add(cRoomCost);
					}
					
					i++;
					
					String cModFee = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
					hotel_ModifyFee.add(cModFee);
					i+=3;
					
					
				} else {
					break;
				}				
			}
			
			conDetails.setHotel_CustomerName(hotel_CustomerName);
			conDetails.setHotel_BookingNo(hotel_BookingNo);
			conDetails.setHotel_RoomType(hotel_RoomType);
			conDetails.setHotel_OccupancyRateType(hotel_OccupancyRateType);
			conDetails.setHotel_ArrivalDate(hotel_ArrivalDate);
			conDetails.setHotel_Nights(hotel_Nights);
			conDetails.setHotel_Adults(hotel_Adults);
			conDetails.setHotel_Rooms(hotel_Rooms);
			conDetails.setHotel_Children(hotel_Children);
			conDetails.setHotel_DailyRates(hotel_DailyRates);
			conDetails.setHotel_TotalRoomCost(hotel_TotalRoomCost);
			conDetails.setHotel_ModifyFee(hotel_ModifyFee);
			
			
					
			
		} else {
			
			conDetails.setHotelMailLoaded(false);
		}
		
		//Portal Operator
		
		driver.switchTo().defaultContent();
		
		driver.findElement(By.xpath(".//*[@id='emaillayoutbasedon_portal']")).click();
		driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[1]/a")).click();
		Thread.sleep(1500);
		
		driver.switchTo().frame("Myemailformat");
		
		if (driver.findElements(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[1]/td/font[1]/b")).size() != 0) {
			
			String PO_BonoName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[1]/td/font[1]/b")).getText();
			conDetails.setPO_BonoName(PO_BonoName);
			
			String PO_BonoAddress = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[1]/td/font[2]/b")).getText();
			conDetails.setPO_BonoAddress(PO_BonoAddress);
			
			String PO_TpnFax = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[1]/td/font[3]")).getText();
			conDetails.setPO_TpnFax(PO_TpnFax);
			
			String PO_bookingDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[4]/td/table/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_bookingDate(PO_bookingDate);
			
//			String PO_ToOrderNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[4]/td/table/tbody/tr[1]/td[4]/font")).getText().split(": ")[1];
//			conDetails.setPO_ToOrderNo(PO_ToOrderNo);
			
			String PO_BookingNo = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[1]/tbody/tr[4]/td/table/tbody/tr[2]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_BookingNo(PO_BookingNo);
			
			String PO_ToName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_ToName(PO_ToName);
			
			String PO_ToEmail = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[2]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_ToEmail(PO_ToEmail);
			
			String PO_HotelName = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[3]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_HotelName(PO_HotelName);
			
			String PO_HotelAddress = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[3]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/font")).getText();
			conDetails.setPO_HotelAddress(PO_HotelAddress);
			
			String PO_HotelCity = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[3]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_HotelCity(PO_HotelCity);
			
			ArrayList<String> customerNamess = new ArrayList<String>();
			
			for (int i = 1; i <= Integer.parseInt(sInfo.getRooms()); i++) {
				
				String name = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[4]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr["+i+"]/td[2]/font")).getText().split(": ")[1];
				customerNamess.add(name);
			}
			
			conDetails.setPO_customerNames(customerNamess);
			
			String PO_CheckInDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_CheckInDate(PO_CheckInDate);
			
			String PO_CheckOutDate = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_CheckOutDate(PO_CheckOutDate);
			
			String PO_Nights = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[4]/font")).getText().split(": ")[1];
			conDetails.setPO_Nights(PO_Nights);
			
			String PO_Rooms = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[4]/font")).getText().split(": ")[1];
			conDetails.setPO_Rooms(PO_Rooms);
			
			String PO_Adults = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[4]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_Adults(PO_Adults);
			
			String PO_Children = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[4]/td[4]/font")).getText().split(": ")[1];
			conDetails.setPO_Children(PO_Children);
			
			String PO_RoomTypes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_RoomTypes(PO_RoomTypes);
			
			String PO_OccupancyRatePlan = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[5]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_OccupancyRatePlan(PO_OccupancyRatePlan);
			
			String PO_InternalNotes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[8]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_InternalNotes(PO_InternalNotes);
			
			String PO_Hotelnotes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[9]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_Hotelnotes(PO_Hotelnotes);
			
			String PO_CustomerNotes = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[5]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[10]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_CustomerNotes(PO_CustomerNotes);
			
			String PO_DailyRatesWithDates = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/font")).getText();
			conDetails.setPO_DailyRatesWithDates(PO_DailyRatesWithDates);
			
			String PO_ModifyFee = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/font")).getText().split(": ")[1];
			conDetails.setPO_ModifyFee(PO_ModifyFee);
			
			String PO_TotalBookingCharge = driver.findElement(By.xpath("html/body/form/table/tbody/tr/td/table[6]/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr[3]/td[2]/font")).getText().split(": ")[1];
			
			if (PO_TotalBookingCharge.contains(",")) {
				conDetails.setPO_TotalBookingCharge(PO_TotalBookingCharge.replaceAll(",", ""));
			} else {
				conDetails.setPO_TotalBookingCharge(PO_TotalBookingCharge);
			}
			
			
			
		} else {
			
			conDetails.setPortalOperatorLoaded(false);
		}
		
		return conDetails;
	}
	
}


