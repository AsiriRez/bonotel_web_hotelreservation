package com.reader;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.controller.PG_Properties;
import com.model.*;

public class ReservationReportFlow {
	
	private Reservation reservationDetails;
	private WebDriver driver = null;
	private HotelDetails hotelDetails = new HotelDetails();
	private SearchInfo sInfo = new SearchInfo();
	
	public ReservationReportFlow(HotelDetails hoteldetails, SearchInfo sInfo, WebDriver Driver){
		
		this.driver = Driver;
		this.hotelDetails = hoteldetails;	
		this.sInfo = sInfo;	
	}
	
	
	public Reservation loadReservationFlow(WebDriver webdriver) throws InterruptedException{
		
		WebDriverWait wait = new WebDriverWait(driver, 120);	
		reservationDetails = new Reservation();
		driver.get(PG_Properties.getProperty("Baseurl")+ "/reports/OpsReservationReport.do");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("producttype_label_text")));
		Thread.sleep(500);
		
		driver.findElement(By.xpath(".//*[@id='searchby_reservationno']")).click();		
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='searchby_reservationno']")).click();		
		Thread.sleep(2000);
		
		try {
			
			if (driver.findElements(By.id("reservationno")).size() != 0) {
				
				String reservationNo = hotelDetails.getConfirmation_ReservationNO();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reservationno")));
				driver.findElement(By.id("reservationno")).clear();
				driver.findElement(By.id("reservationno")).sendKeys(reservationNo);
				driver.findElement(By.id("reservationno_lkup")).click();
				driver.switchTo().defaultContent();
				driver.switchTo().frame("lookup");
				driver.findElement(By.xpath(".//*[@id='row-0']/td")).click();
				Thread.sleep(1000);
				driver.switchTo().defaultContent();
				
				driver.findElement(By.xpath(".//*[@id='topControlBar']/tbody/tr/td[2]/table/tbody/tr/td[2]/table/tbody/tr[2]/td[7]/a/img")).click();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("reportData")));
				
				String bookingNo = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[3]")).getText();
				reservationDetails.setBookingNo(bookingNo);
				
				String bookingDate = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[1]")).getText();
				reservationDetails.setBookingDate(bookingDate);
				
				String toName = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[1]")).getText();
				reservationDetails.setToName(toName);
				
				String toOrderNo = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[1]")).getText();
				reservationDetails.setToOrderNo(toOrderNo);
				
				String notes = driver.findElement(By.xpath(".//*[@id='NotesId_1']")).getText();
				reservationDetails.setNotes(notes);
				
				String hotelname = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[4]")).getText();
				reservationDetails.setHotelname(hotelname);
				
				String bookingType = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[2]")).getText().split(" / ")[0];
				reservationDetails.setBookingType(bookingType);
				
				String channel = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[2]")).getText().split(" / ")[1];
				reservationDetails.setChannel(channel);
				
				int roomCount = Integer.parseInt(sInfo.getRooms());
				ArrayList<String> firstNameList = new ArrayList<String>();
				
				for (int i = (roomCount-1); i >= 0; i--) {
					
					String fullName = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[2]")).getText().split("\n")[i];
					firstNameList.add(fullName);
					
				}
					
				reservationDetails.setFirstName(firstNameList);
					
				String modifiedBy = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[2]")).getText();
				reservationDetails.setModifiedBy(modifiedBy);
				
				String arrivalDate = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[5]")).getText().split(" / ")[0];
				reservationDetails.setArrivalDate(arrivalDate);
				
				String departureDate = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[5]")).getText().split(" / ")[1];
				reservationDetails.setDepartureDate(departureDate);
				
				ArrayList<String> roomTypeList = new ArrayList<String>();
				ArrayList<String> bedRateList = new ArrayList<String>();
				
				String roomTypes = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[3]")).getText();
				
				if (roomTypes.contains("\n")) {
					String roomTypesB = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[3]")).getText();			
					String[] partsRTypes = roomTypesB.split("\n");
					for (String valueRT : partsRTypes) {
						roomTypeList.add(valueRT);
					}				
				} else {
					String roomTypesA = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[3]")).getText();
					roomTypeList.add(roomTypesA);
				}
				
				String bedRate = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[3]")).getText();
				
				if (bedRate.contains("\n")) {
					String bedRateA = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[3]")).getText();			
					String[] partsBedTypes = bedRateA.split("\n");
					for (String valueB : partsBedTypes) {
						bedRateList.add(valueB);
					}				
				} else {
					String bedRated = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[3]")).getText();
					bedRateList.add(bedRated);
				}
				
				
				reservationDetails.setBedType(bedRateList);
				reservationDetails.setRoomType(roomTypeList);
				
				String rooms = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[8]")).getText().split(" / ")[0];
				reservationDetails.setRooms(rooms);
				
				String nights = driver.findElement(By.xpath(".//*[@id='RowNo_1_1']/td[8]")).getText().split(" / ")[1];
				reservationDetails.setNights(nights);
				
				String totalCost = driver.findElement(By.xpath(".//*[@id='RowNo_1_2']/td[6]")).getText();
				
				if (totalCost.contains(",")) {
					reservationDetails.setTotalCost(totalCost.replaceAll(",", ""));
				} else {
					reservationDetails.setTotalCost(totalCost);
				}
				
				
				String bookingValue = driver.findElement(By.xpath(".//*[@id='RowNo_1_3']/td[6]")).getText();
				
				if (bookingValue.contains(",")) {
					reservationDetails.setBookingValue(bookingValue.replaceAll(",", ""));
				} else {
					reservationDetails.setBookingValue(bookingValue);
				}
				
				
				String bonoModifyFee = driver.findElement(By.xpath(".//*[@id='RowNo_1_4']/td[4]")).getText();
				reservationDetails.setBonoModifyFee(bonoModifyFee);
				
				String hotelModifyFee = driver.findElement(By.xpath(".//*[@id='RowNo_1_5']/td[2]")).getText();
				reservationDetails.setHotelModifyFee(hotelModifyFee);
				
				
				
				String pageTotal = driver.findElement(By.xpath(".//*[@id='reportData']/tbody/tr[14]/td[5]")).getText().replaceAll(" ", "");
				
				if (pageTotal.contains(",")) {
					reservationDetails.setPageTotal(pageTotal.replaceAll(",", ""));
				} else {
					reservationDetails.setPageTotal(pageTotal);
				}
				
				
				String grandTotal = driver.findElement(By.xpath(".//*[@id='reportData']/tbody/tr[16]/td[5]")).getText().replaceAll(" ", "");
				
				if (grandTotal.contains(",")) {
					reservationDetails.setGrandTotal(grandTotal.replaceAll(",", ""));
				} else {
					reservationDetails.setGrandTotal(grandTotal);
				}
				
				
				
			} else {
				reservationDetails.setReservationReportLoaded(false);
			}
			
			
		} catch (Exception e) {
			reservationDetails.setReservationReportLoaded(false);
		}

		return reservationDetails;
	}
	

}
