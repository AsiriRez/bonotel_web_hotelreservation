package com.model;

import java.util.ArrayList;

public class CancellationDetails {
	
	private boolean isCancellationReportLoaded  = true;
	private String hotelName;
	private String hotelAddress;
	private String hotelCity;
	private String arrivalDate;
	private String departureDate;
	private String adults;
	private String nights;
	private String rooms;
	private String children;
	private String bookingDate;
	private String TOName;
	private String bookingNo;
	private String bookingChannel;
	private ArrayList<String> cusTitle; 
	private ArrayList<String> cusLastName; 
	private ArrayList<String> cusFirstName;
	
	private ArrayList<String> roomTypeList;
	private ArrayList<String> bedTypeList;
	private ArrayList<String> adultsList;
	private ArrayList<String> childrenList;
	private ArrayList<ArrayList<String>> dailyRates;
	private ArrayList<String> totalValue;	
	private ArrayList<String> daysCount;
	
	private String subTotalLast;
	private String totalPayable;
	
	
	
	public ArrayList<String> getDaysCount() {
		return daysCount;
	}
	public void setDaysCount(ArrayList<String> daysCount) {
		this.daysCount = daysCount;
	}
	public boolean isCancellationReportLoaded() {
		return isCancellationReportLoaded;
	}
	public void setCancellationReportLoaded(boolean isCancellationReportLoaded) {
		this.isCancellationReportLoaded = isCancellationReportLoaded;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getHotelAddress() {
		return hotelAddress;
	}
	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress;
	}
	public String getHotelCity() {
		return hotelCity;
	}
	public void setHotelCity(String hotelCity) {
		this.hotelCity = hotelCity;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getAdults() {
		return adults;
	}
	public void setAdults(String adults) {
		this.adults = adults;
	}
	public String getNights() {
		return nights;
	}
	public void setNights(String nights) {
		this.nights = nights;
	}
	public String getRooms() {
		return rooms;
	}
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	public String getChildren() {
		return children;
	}
	public void setChildren(String children) {
		this.children = children;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getTOName() {
		return TOName;
	}
	public void setTOName(String tOName) {
		TOName = tOName;
	}
	public String getBookingNo() {
		return bookingNo;
	}
	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}
	public String getBookingChannel() {
		return bookingChannel;
	}
	public void setBookingChannel(String bookingChannel) {
		this.bookingChannel = bookingChannel;
	}
	public ArrayList<String> getCusTitle() {
		return cusTitle;
	}
	public void setCusTitle(ArrayList<String> cusTitle) {
		this.cusTitle = cusTitle;
	}
	public ArrayList<String> getCusLastName() {
		return cusLastName;
	}
	public void setCusLastName(ArrayList<String> cusLastName) {
		this.cusLastName = cusLastName;
	}
	public ArrayList<String> getCusFirstName() {
		return cusFirstName;
	}
	public void setCusFirstName(ArrayList<String> cusFirstName) {
		this.cusFirstName = cusFirstName;
	}
	public ArrayList<String> getRoomTypeList() {
		return roomTypeList;
	}
	public void setRoomTypeList(ArrayList<String> roomTypeList) {
		this.roomTypeList = roomTypeList;
	}
	public ArrayList<String> getBedTypeList() {
		return bedTypeList;
	}
	public void setBedTypeList(ArrayList<String> bedTypeList) {
		this.bedTypeList = bedTypeList;
	}
	public ArrayList<String> getAdultsList() {
		return adultsList;
	}
	public void setAdultsList(ArrayList<String> adultsList) {
		this.adultsList = adultsList;
	}
	public ArrayList<String> getChildrenList() {
		return childrenList;
	}
	public void setChildrenList(ArrayList<String> childrenList) {
		this.childrenList = childrenList;
	}
	public ArrayList<ArrayList<String>> getDailyRates() {
		return dailyRates;
	}
	public void setDailyRates(ArrayList<ArrayList<String>> dailyRates) {
		this.dailyRates = dailyRates;
	}
	public ArrayList<String> getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(ArrayList<String> totalValue) {
		this.totalValue = totalValue;
	}
	public String getSubTotalLast() {
		return subTotalLast;
	}
	public void setSubTotalLast(String subTotalLast) {
		this.subTotalLast = subTotalLast;
	}
	public String getTotalPayable() {
		return totalPayable;
	}
	public void setTotalPayable(String totalPayable) {
		this.totalPayable = totalPayable;
	}
	
	
	
	

}
